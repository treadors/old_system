import pandas as pd
import numpy as np
import matplotlib
from sklearn.preprocessing import StandardScaler
matplotlib.use('Qt5Agg')
import tsfresh as tsf
import ta as ta
import matplotlib.pyplot as plt

from preferences import globalDataLen, dataPath
from hello.utils import loadData, saveData
from hello.models import Exc_rate, toDictionary
import hello.analysis as anl

# Calculate func by column named "key", if append=True, name it as "name" column
# and add it to a Table
def calculate_and_append(self, keys, func, name, *args):
    if isinstance(keys, (tuple or list or dict or set)):
        columns = [self[key].values for key in keys]
    elif isinstance(keys, str):
        columns = [self[keys].values,]
    assert name != None, "Name for data column must be not None!"
    if not args:
        self[name] = func(*columns)
    else:
        self[name] = func(*columns, *args)

def calculate_(self, keys, func, *args, reshaped=False):
    if isinstance(keys, (tuple or list or dict or set)):
        if not reshaped:
            columns = [self[key].values for key in keys]
        else:
            columns = [self[key].values.reshape(-1, 1) for key in keys]
    elif isinstance(keys, str):
        if not reshaped:
            columns = [self[keys].values,]
        else:
            columns = [self[keys].values.reshape(-1, 1), ]
    if not args:
        if not reshaped:
            return pd.Series(func(*columns), index=self.index)
        else:
            return pd.Series(func(*columns).flatten(), index=self.index)
        #return func(*columns)
    else:
        if not reshaped:
            return pd.Series(func(*columns, *args), index=self.index)
        else:
            return pd.Series(func(*columns, *args).flatten(), index=self.index)

def calculate_last(self, keys, func, *args):
    if isinstance(keys, (tuple or list or dict or set)):
        columns = [self[key].values for key in keys]
    elif isinstance(keys, str):
        columns = [self[keys].values,]
    if not args:
        return func(*columns)[-1]
    else:
        return func(*columns, *args)[-1]

class Data():
    def __init__(self):
        self.tables = {}
        self.tableNames = []
        self.scalers = {}
        self.preferences = None
        self.extended = False # Вычислялись ли параметры индикаторы для слоев?
    def __getitem__(self, item):
        return self.tables[item]
    def __setitem__(self, key, value):
        self.tables[key] = value
        self.tables[key].name = key
        if not key in self.tableNames:
            self.tableNames.append(key)
    def __delitem__(self, key):
        del self.tables[key]

    # Prepare general indicators for system
    def prepare_general_indicators(self, symbol, load_scalers=True):
        if load_scalers:
            self.scalers = loadData(name='scalers')
            if not symbol in self.scalers.keys():
                self.scalers[symbol] = StandardScaler()
                self.scalers[symbol].fit(self[symbol]['Close'].values.reshape(-1, 1))
            self[symbol]['scaled_price'] = self.scalers[symbol].transform(self[symbol]['Close'].values.reshape(-1, 1))
        else:
            self.scalers[symbol] = StandardScaler()
            self.scalers[symbol].fit(self[symbol]['Close'].values.reshape(-1, 1))
            self[symbol]['scaled_price'] = self.scalers[symbol].transform(self[symbol]['Close'].values.reshape(-1, 1))
        calculate_and_append(self[symbol], 'Close', anl.toPriceMoves, name='priceMoves')
        calculate_and_append(self[symbol], 'baseVolume', anl.toPriceMoves, name='volumeMoves')
        calculate_and_append(self[symbol], ('bid10per', 'ask10per'), anl.to_DepthRatio, name='depth_ratio_10per')
        calculate_and_append(self[symbol], ('bid25per', 'ask25per'), anl.to_DepthRatio, name='depth_ratio_25per')
        calculate_and_append(self[symbol], 'timedate', anl.to_Hours, name='hours')
        print(self[symbol][['Close', 'scaled_price']].tail(5))

    def glb_prepare_general_indicators(self, load_scalers=True):
        for symbol in self.tables:
            self.prepare_general_indicators(symbol, load_scalers)
        if not load_scalers:
            saveData(self.scalers, 'scalers')

    # Prepare data/indicators for layer evaluations
    def prepare_layer_indicators(self, symbol, prefs, silent=False):
        table = self[symbol]
        calculate_and_append(table, 'Close', anl.toStandartSignals, 'states_s', prefs['smoothy'])
        calculate_and_append(table, 'Close', anl.toStandartSignals, 'states_m', prefs['mSmoothy'])
        calculate_and_append(table, 'Close', anl.toStandartSignals, 'states_e', prefs['eSmoothy'])

        calculate_and_append(table, 'Close', anl.MACDRatio, 'macd_s',
                             prefs['smoothy'], prefs['mSmoothy'], prefs['smoothy'] // 5)
        calculate_and_append(table, 'Close', anl.MACDRatio, 'macd_m',
                             prefs['mSmoothy'], prefs['eSmoothy'], prefs['eSmoothy'] // 5)
        calculate_and_append(table, 'Close', anl.MACDRatio, 'macd_e',
                             prefs['eSmoothy'] * 1.61, prefs['eSmoothy'] * 2.61, prefs['eSmoothy'] // 5)
        calculate_and_append(table, 'Close', anl.Stochastic, 'stoh_s',
                             prefs['smoothy'], prefs['smoothy'] // 6, True)
        calculate_and_append(table, 'Close', anl.Stochastic, 'stoh_m',
                             prefs['mSmoothy'], prefs['mSmoothy'] // 6, True)
        calculate_and_append(table, 'Close', anl.Stochastic, 'stoh_e',
                             prefs['eSmoothy'], prefs['eSmoothy'] // 6, True)
        calculate_and_append(table, 'Close', anl.getForward_OLS, 'tang_s', prefs['smallTG'], True)
        calculate_and_append(table, 'Close', anl.getForward_OLS, 'tang_m', prefs['middleTG'], True)
        calculate_and_append(table, 'Close', anl.getForward_OLS, 'tang_e', prefs['largeTG'], True)

        calculate_and_append(table, 'Close', anl.FibonacciSlippery, 'fibo_s', prefs['smallHistory'], 20)
        calculate_and_append(table, 'Close', anl.FibonacciSlippery, 'fibo_m', prefs['history'], 40)
        calculate_and_append(table, 'Close', anl.FibonacciSlippery, 'fibo_e', prefs['extendedHistory'], 100)
        calculate_and_append(table, 'Close', anl.FibonacciSlippery, 'fibo_fuzzy_s',
                             prefs['smallHistory'], 20, True)
        calculate_and_append(table, 'Close', anl.FibonacciSlippery, 'fibo_fuzzy_m',
                             prefs['history'], 40, True)
        calculate_and_append(table, 'Close', anl.FibonacciSlippery, 'fibo_fuzzy_e',
                             prefs['extendedHistory'], 100, True)
        table['atr_e'] = ta.average_true_range(table['Low'], table['High'], table['Close'], n=prefs['eSmoothy']).apply(lambda x: x/100)
        table['rsi_e'] = ta.rsi(table['Close'], n=prefs['eSmoothy']).apply(lambda x: x/100)
        filtered = [col for col in list(table) if col not in ['tang_s', 'tang_m', 'tang_e']]
        table = table.dropna(subset=filtered)
        self[symbol] = table.reset_index(drop=True)
        length = self[symbol].shape[0]
        starttime = self[symbol].loc[0][['timedate']].values
        endtime = self[symbol].loc[length-1][['timedate']].values
        self.intervals[symbol] = (starttime, endtime)
        if not silent:
            print('{0} Succesfully. Length: {1}. Start: {2}. End: {3}'
                  .format(symbol, self[symbol].shape[0], starttime, endtime))


        # table[['rsi_e', 'scaled_price', 'atr_e']].plot()
        plt.show()

    def glb_prepare_layer_indicators(self, preferences=None):
        self.intervals = {}
        if not preferences:
            if not self.preferences:
                raise Exception('Could not find layer preferences for Data. Means you must specificate layer preferences by '
                                'Layer.getPreferences() function.')
            else:
                prefs = self.preferences
        else:
            prefs = preferences
        for symbol in self.tables:
            self.prepare_layer_indicators(symbol, prefs)
        self.extended = True

    # Recalculate general data/indicators for last N datapoints
    # (a data point is usually meant as a dictionary, formed, for example,
    # from an instanse of Exc_rates class (see models.py).
    # Useful when you add current datapoint to row
    def recalc(self, symbol, backsize=None, n_points=1):
        if not backsize:
            if not self.preferences:
                raise Exception(
                    'Could not find layer preferences for Data. Means you must specificate layer preferences by '
                    'Layer.getPreferences() function.')
            else:
                backsize = max([self.preferences['largeTG'], self.preferences['smallHistory'],
                                self.preferences['eSmoothy']])
        prefs = self.preferences
        last_table = self[symbol].tail(backsize)
        #table = last_table
        table = self[symbol]
        last = table.index[-1]+1

        table.loc[last - n_points:last, 'scaled_price'] = calculate_(table[last - n_points - 1:last], 'Close',
                                                                   self.scalers[symbol].transform,
                                                                     reshaped=True)[-n_points:]
        table.loc[last-n_points:last, 'priceMoves'] = calculate_(table[last-n_points-1:last], 'Close',
                                                                     anl.toPriceMoves)[-n_points:]
        table.loc[last-n_points:last, 'volumeMoves'] = calculate_(table[last-n_points-1:last], 'baseVolume',
                                                                      anl.toPriceMoves)[-n_points:]
        table.loc[last-n_points:last, 'depth_ratio_10per'] = calculate_(table[last-n_points-1:last],
                                                                        ('bid10per', 'ask10per'),
                                                                        anl.to_DepthRatio)[-n_points:]
        table.loc[last-n_points:last, 'depth_ratio_25per'] = calculate_(table[last-n_points-1:last],
                                                                        ('bid25per', 'ask25per'),
                                                                        anl.to_DepthRatio)[-n_points:]
        table.loc[last-n_points:last, 'hours'] = calculate_(table[last-n_points-1:last], 'timedate',
                                                            anl.to_Hours)[-n_points:]

        table.loc[last-backsize//2:last, 'states_s'] = calculate_(table[last-backsize:last], 'Close', anl.toStandartSignals,
                                                                     prefs['smoothy'])[-backsize//2:]
        table.loc[last-backsize//2:last, 'states_m'] = calculate_(table[last-backsize:last], 'Close', anl.toStandartSignals,
                                                                     prefs['mSmoothy'])[-backsize//2:]
        table.loc[last-backsize//2:last, 'states_e'] = calculate_(table[last-backsize:last], 'Close', anl.toStandartSignals,
                                                                     prefs['eSmoothy'])[-backsize//2:]

        table.loc[last-n_points:last, 'macd_s'] = calculate_(table[last-backsize:last], 'Close', anl.MACDRatio,
                                                                 prefs['smoothy'], prefs['mSmoothy'],
                                                                 prefs['smoothy'] // 5)[-n_points:]
        table.loc[last-n_points:last, 'macd_m'] = calculate_(table[last-backsize:last], 'Close', anl.MACDRatio,
                                                                 prefs['mSmoothy'], prefs['eSmoothy'],
                                                                 prefs['eSmoothy'] // 5)[-n_points:]
        table.loc[last-n_points:last, 'macd_e'] = calculate_(table[last-backsize:last], 'Close', anl.MACDRatio,
                                                                 prefs['eSmoothy'] * 1.61, prefs['eSmoothy'] * 2.61,
                                                                 prefs['eSmoothy'] // 5)[-n_points:]

        table.loc[last-n_points:last, 'stoh_s'] = calculate_(table[last-backsize:last], 'Close', anl.Stochastic,
                                                                 prefs['smoothy'], prefs['smoothy'] // 6, True)[-n_points:]
        table.loc[last-n_points:last, 'stoh_m'] = calculate_(table[last-backsize:last], 'Close', anl.Stochastic,
                                                                 prefs['mSmoothy'], prefs['mSmoothy'] // 6, True)[-n_points:]
        table.loc[last-n_points:last, 'stoh_e'] = calculate_(table[last-backsize:last], 'Close', anl.Stochastic,
                                                                 prefs['eSmoothy'], prefs['eSmoothy'] // 6, True)[-n_points:]

        table.loc[last-n_points:last, 'fibo_s'] = calculate_(table[last-prefs['smallHistory']:last], 'Close',
                                                                 anl.FibonacciSlippery, prefs['smallHistory'], 20)[-n_points:]
        table.loc[last-n_points:last, 'fibo_m'] = calculate_(table[last-prefs['history']:last], 'Close',
                                                                 anl.FibonacciSlippery, prefs['history'], 40)[-n_points:]
        table.loc[last-n_points:last, 'fibo_e'] = calculate_(table[last-prefs['extendedHistory']:last], 'Close',
                                                                 anl.FibonacciSlippery, prefs['extendedHistory'], 50)[-n_points:]

        table.loc[last-n_points:last, 'fibo_fuzzy_s'] = calculate_(table[last-prefs['smallHistory']:last], 'Close',
                                                                       anl.FibonacciSlippery, prefs['smallHistory'], 20,
                                                                       True)[-n_points:]
        table.loc[last-n_points:last, 'fibo_fuzzy_m'] = calculate_(table[last-prefs['history']:last], 'Close',
                                                                       anl.FibonacciSlippery, prefs['history'], 40,
                                                                       True)[-n_points:]
        table.loc[last-n_points:last, 'fibo_fuzzy_e'] = calculate_(table[last-prefs['extendedHistory']:last], 'Close',
                                                                       anl.FibonacciSlippery, prefs['extendedHistory'], 50,
                                                                       True)[-n_points:]

        table.loc[last-prefs['smallTG']-1:last, 'tang_s'] = calculate_(table[last-prefs['smallTG']-1:last], 'Close',
                                                                         anl.getForward_OLS, prefs['smallTG'], True)
        table.loc[last-prefs['middleTG']-1:last, 'tang_m'] = calculate_(table[last-prefs['middleTG']-1:last], 'Close',
                                                                         anl.getForward_OLS, prefs['middleTG'], True)
        table.loc[last-prefs['largeTG']-1:last, 'tang_e'] = calculate_(table[last-prefs['largeTG'] - 1:last], 'Close',
                                                                            anl.getForward_OLS, prefs['largeTG'], True)

        table.loc[last-n_points:last, 'atr_e'] = ta.average_true_range(table.loc[last-prefs['eSmoothy']:last, 'Low'],
                                                                     table.loc[last-prefs['eSmoothy']:last, 'High'],
                                                                     table.loc[last-prefs['eSmoothy']:last, 'Close'],
                                                                     n=prefs['eSmoothy']).apply(lambda x: x / 100)[-n_points:]
        table.loc[last-n_points:last, 'rsi_e'] = ta.rsi(table.loc[last-prefs['eSmoothy']:last, 'Close'],
                                                        n=prefs['eSmoothy']).apply(lambda x: x / 100)[-n_points:]

        # if last > globalDataLen:
        #     table.drop(index=table.index[0], inplace=True)
        #     table = table.reset_index()
        print(self[symbol][['Close', 'timedate', 'scaled_price']].tail(5))
        # print(self[symbol].info())
        # index = table.index
        # print(index[0])
        # print(table.index.values)




    # This is the place for introduce the data structure independency of current trading system: we need to split data and
    # trading logic to get truly module system. Our future expectation is global data grabber. At now we are using django and totally sucks




    def get_price_data_from_bd(self, cp, start, end, step, wrapper=None):

        if not wrapper:
            print('Can\'t specialize wrapper. Default wrapper poloniex postgres will be used.')
            from hello.wrapper import Wrapper
            wrapper = Wrapper('poloniex')
        slash = cp.find('/')
        if slash == -1:
            raise Exception('Expected symbol format is QUOTE/BASE. You pass {}'.format(cp))
        quote = cp[:slash:]
        base = cp[slash + 1:]
        raw_data = wrapper.getRatesHistory(base, quote, start, end, step)
        if isinstance(raw_data, list):
            if isinstance(raw_data[0], Exc_rate):
                raw_data = toDictionary(raw_data)
                try:
                    key = list(raw_data.keys())[0]
                except IndexError:
                    return None
                Indexes = list(range(len(raw_data[key])))
                table = pd.DataFrame(raw_data, index=Indexes)
                self[cp] = table
                return self[cp]

