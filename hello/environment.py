﻿from hello.committee import *
from hello.committee import *
from hello.models import *
from hello.wrapper import *
from grabbing.data import Data


#Environment - вся общая инфа (котировки валютных пар, ордербуки, регрессии...)
class Environment():
    test = False #Тестовый режим?
    CurrencyTypes={}
    CurrencyPairTypes={}

    def __init__(self, base_currencies=('BTC', 'USDT'), Test = False,
                 length=0, API = '', secret = '', source='poloniex', CurrencyPairs=None):
        print('Создание окружающей среды...')
        self.test = Test
        self.wrapper = Wrapper(source, API=API, secret=secret)
        self.base_currencies = base_currencies
        self.Data = None
        if CurrencyPairs:
            self.setCurrencies(CurrencyPairs)
        if self.CurrencyPairTypes == {}:
            self.getCurrencies()
        self.length = int(length)
        dir = os.getcwd()+dataPath
        try:
            os.mkdir(dir)
        except OSError:
            pass

    #Полное обновление котировок
    def getExcRates(self, test=None, save=False):
        if not self.test or test==False:
            self.wrapper.setExcRates(self.currencyPairs)
        else:
            self.wrapper.setExcRates(self.currencyPairs, self.current)
            self.current += 1
        start_time = time.time()
        if save and self.Data:
            self.addCurrentToHistory()
        end_time = time.time()

    def addCurrentToHistory(self):
        for target in self.currencyPairs:
            for cp in self.currencyPairs[target]:
                #self.currencyPairs[target][cp].AddCurrent()
                if self.Data:
                    this = self.currencyPairs[target][cp].Current
                    names = self.Data[cp].columns
                    dict = this.toDict()
                    for name in names:
                        if not name in dict.keys():
                            dict[name] = None
                    self.Data[cp].loc[self.Data[cp].index[-1]+1] = dict
                    self.Data.recalc(cp)

    def popCurrentFromHistory(self):
        for target in self.currencyPairs:
            for cp in self.currencyPairs[target]:
                self.currencyPairs[target][cp].DeaddCurrent()

    def getCurrencies(self, reload=True):
        self.CurrencyTypes = self.wrapper.getAvailableCurrencies()
        self.CurrencyPairTypes = self.wrapper.getAvailableTradingPairs(self.base_currencies)
        if reload == True:
            self.currencyPairs = {}
            for currency in self.base_currencies:
                self.currencyPairs[currency] = {}
            for currency in self.currencyPairs:
                for cp in self.CurrencyPairTypes[currency]:
                    self.currencyPairs[currency][cp]=CurrencyPair()

    def setCurrencies(self, CurrencyPairTypes, reload=True):
        self.CurrencyPairTypes = CurrencyPairTypes
        CurrencyTypes = []
        if reload:
            self.currencyPairs = {}
        for base in self.CurrencyPairTypes:
            CurrencyTypes.append(base)
            if reload:
                self.currencyPairs[base] = {}
            for cp in self.CurrencyPairTypes[base]:
                if reload:
                    self.currencyPairs[base][cp] = CurrencyPair()
                slash = cp.find('/')
                first = cp[slash + 1::]
                if first not in CurrencyTypes:
                    CurrencyTypes.append(first)
        self.CurrencyTypes = CurrencyTypes

    #Получение ask/bid предложений
    def getOrderBook(self, currencyPair, depth):
        pass

    #Получение данных о валютной паре из БД. Возвращает ссылку на эти данные
    def getHistory(self, currencyPair, start, end, step, minlen=0):
        slash = currencyPair.find('/'); second = currencyPair[:slash:]; first = currencyPair[slash + 1::]
        this = self.currencyPairs[first][currencyPair]
        try:
            history=self.wrapper.getRatesHistory(first, second, start, end, step)
            if len(history) < minlen or not len(history):
                raise Exception('Not enought history on currency pair {0}. Min required {1}, found {2}.'
                                .format(currencyPair, minlen, len(history)))
            this.historyExist = True
            return history
        except Exception as ex:
            for i in range(len(self.CurrencyTypes)):
                if self.CurrencyTypes[i] == second:
                    print('Delete currency {0}. \n {1}.'.format(self.CurrencyTypes[i], ex))
                    self.CurrencyTypes.pop(i)
                    break
            for target in self.CurrencyPairTypes:
                for i in range(len(self.CurrencyPairTypes[target])):
                    currencyPair = self.CurrencyPairTypes[target][i]
                    slash2 = currencyPair.find('/')
                    second2 = currencyPair[:slash:]
                    if second == second2:
                        self.CurrencyPairTypes[target].pop(i)
                        try:
                            self.currencyPairs[target].pop(currencyPair)
                            print('Delete currency pair {0}.'.format(currencyPair))
                            del self.currencyPairs[target][currencyPair]
                        except Exception as e:
                            print(e)
                        break

    def getData(self, currencyPair, start, end, step, minlen=0):
        if not self.Data:
            self.Data = Data()
        data = toDictionary(self.getHistory(currencyPair, start, end, step, minlen))
        try:
            key = list(data.keys())[0]
        except IndexError:
            return
        Indexes = list(range(len(data[key])))
        table = pd.DataFrame(data, index=Indexes)
        self.Data[currencyPair] = table
        return self.Data[currencyPair]

    def getAllData(self, start, end, step, minlen=0, load_scalers=True):
        print('Получение истории...')
        if not self.Data:
            self.Data = Data()
        cpairs = copy.deepcopy(self.CurrencyPairTypes)
        lenFirst = None
        for tg in cpairs:
            for cp in cpairs[tg]:
                slash = cp.find('/')
                second = cp[:slash:]
                first = cp[slash+1::]
                # try:
                #     if not lenFirst:
                #         history = self.getData(cp, start, end, step, minlen=minlen)
                #         lenFirst = history.shape[0]
                #     else:
                #         self.getData(cp, start, end, step, minlen=lenFirst-2)
                # except Exception as e:
                #     print(e)
                if not lenFirst:
                    history = self.getData(cp, start, end, step, minlen=minlen)
                    if isinstance(history, pd.DataFrame):
                        self.currencyPairs[tg][cp].GlobalHistory = history
                        try:
                            lenFirst = self.currencyPairs[tg][cp].GlobalHistory.shape[0]
                        except:
                            pass
                else:
                    history = self.getData(cp, start, end, step, minlen=lenFirst - 2)
                    if isinstance(history, pd.DataFrame):
                        try:
                            self.currencyPairs[tg][cp].GlobalHistory = history
                        except Exception as e:
                            print(e)
        self.Data.glb_prepare_general_indicators(load_scalers=load_scalers)
        self.current = lenFirst - self.length

    def setupGlobalHistory(self):
        for tg in self.currencyPairs:
            for cp in self.currencyPairs[tg]:
                self.currencyPairs[tg][cp].GlobalHistory = self.Data[cp]
                self.currencyPairs[tg][cp].HistoryStart = self.Data.intervals[cp][0]
                self.currencyPairs[tg][cp].HistoryEnd = self.Data.intervals[cp][1]

    #Получение всех исторических данных. Ниче не возвращает, просто записывает.
    def getAllHistories(self,start,end,step, minlen=0):
        print('Получение истории...')
        cpairs = copy.deepcopy(self.currencyPairs)
        lenFirst = None
        for mc in cpairs:
            for currencyPair in cpairs[mc]:
                slash = currencyPair.find('/'); second = currencyPair[:slash:]; first = currencyPair[slash + 1::]
                try:
                    this = self.currencyPairs[first][currencyPair]
                    if not lenFirst:
                        his = self.getHistory(currencyPair, start, end, step, minlen=minlen)
                        lenFirst = len(his)
                    else:
                        self.getHistory(currencyPair, start, end, step, minlen=lenFirst-2)
                except:
                    pass

    #Получение исторических данных за промежуток времени. Обращается к БД только если исторических данных нет.
    def getHistoricInterval(self,currencyPair,start,end,step):

        slash = currencyPair.find('/'); second = currencyPair[:slash:]; first = currencyPair[slash + 1::]
        this = self.currencyPairs[first][currencyPair]
        if not this.historyExist:
            this.GlobalHistory = self.wrapper.getRatesHistory(first, second, start, end, step)
            this.HistoryStart = this.GlobalHistory[0].timedate
            this.HistoryEnd = this.GlobalHistory[-1].timedate
            this.historyExist=True
            return this.GlobalHistory
        else:
            return anl.getPeriod(this.GlobalHistory,start,end,step)

    #Вычисляет корелляцию двух валютных пар (месячную, недельную, дневную)
    def checkHistories(self):
        toDelete = []
        this = self.currencyPairs['BTC']['ETH/BTC']
        for c in self.currencyPairs:
            for cp in self.currencyPairs[c]:
                i = 0
                while (i < len(this.GlobalHistory)):
                    if self.currencyPairs[c][cp].GlobalHistory[i].timedate != this.GlobalHistory[i].timedate:
                        print('Валюта ' + cp + '. Дата ' + str(self.currencyPairs[c][cp].GlobalHistory[i].timedate))
                        toDelete.append(self.currencyPairs[c][cp].GlobalHistory.pop(i))
                    else:
                        i+=1
        for obj in toDelete:
            obj.delete()

    def cutCurrenciesByVolume(self, target, count):
        cps = copy.deepcopy(self.CurrencyPairTypes[target])
        CPS = copy.deepcopy(self.currencyPairs[target])
        new_cps = []
        new_CPS = {}
        for i in range(count):
            maxVolume = 0.0
            maxVolumeCurrency = ''
            maxVolumeWorkingCurrency = CurrencyPair
            maxIndex = 0
            for j in range(len(cps)):
                currency = CPS[cps[j]]
                try:
                    v = float(currency.Current['Volume'])
                except:
                    v = float(currency.Current.baseVolume)
                if v > maxVolume:
                    maxVolume = v
                    maxVolumeCurrency = cps[j]
                    maxVolumeWorkingCurrency = CPS[cps[j]]
                    maxIndex = j
            new_cps.append(maxVolumeCurrency)
            new_CPS[cps[maxIndex]] = maxVolumeWorkingCurrency
            CPS.pop(cps[maxIndex])
            cps.pop(maxIndex)
        self.CurrencyPairTypes[target] = new_cps
        self.currencyPairs[target] = new_CPS

#Portfolio - портфель валют. Здесь всё, что имеет отношение к приватной части аккаунта.
class Portfolio():
    # Balances[i]=(available - мобильная валюта, sell - +в ордерах на продажу, buy - +в ордерах на покупку)
    env = Environment

    # открытыми ордерами по каждой торгуемой паре;
    def __init__(self, environment):
        print('Создание портфолио...')
        self.env = environment
        self.wrapper = self.env.wrapper
        Balances = self.wrapper.getBalances()
        self.Info = Balances[1]
        self.Balances = Balances[0]

    def refresh(self):
        self.Balances = self.wrapper.getBalances()[0]

    # поиск ордера в списке по ключу (string), самому ордеру (dictionary)
    def findOrder(self, order):
        if type(order) == 'dict':
            for item in self.OpenOrders[order['fromCoin']]:
                print(str(item))
                if order['orderNumber'] == item['orderNumber']:
                    return item
            else:
                return None
        else:
            for orderType in self.OpenOrders:
                for item in self.OpenOrders[orderType]:
                    if int(item['orderNumber']) == int(order):
                        return item
            else:
                return None
        return None
    # добавление, отмена ордера делается средствами polo. Написать, если будем оптимизировать refresh().

    #Сколько target в volume of currency?
    def convert(self, target, currency, volume=0.0, type='free'):
        pair = currency + '/' + target
        btcpair = currency + '/BTC'
        price = float
        if target == currency:
            if volume == 0.0:
                return self.Balances[currency]['free']
            else:
                return volume
        if target == 'BTC':
            try:
                price = float(self.env.currencyPairs[target][pair].Current.last)
            except:
                price = 0.0
        elif target == 'USDT' or target == 'USD':
            corePair = 'BTC/' + target
            try:
                price = float(self.env.currencyPairs[target][pair].Current.last)
            except:
                price = float(self.env.currencyPairs['USDT'][corePair].Current.last)
                price *= float(self.env.currencyPairs['BTC'][pair].Current.last)
        if volume == 0.0:
            if type=='free':
                amount = price * self.Balances[currency]['free']
            if type=='used':
                amount = price * self.Balances[currency]['used']
            if type=='total':
                amount = price * self.Balances[currency]['total']
            #print(pair + '. Amount in : ' + target + ': ' + str(amount) + '=' + str(price) + '*' + str(self.Balances[currency]['actual']))
        else:
            amount = price * volume
            #print(pair + '. Amount in : ' + target + ': ' + str(amount) + '=' + str(price) + '*' + str(volume))
        return amount

    #Сколько currency в volume of target?
    def invert(self, target, currency, volume=0.0, type='free'):
        pair = currency + '/' + target
        price = 0.0
        if target == 'BTC':
            price = float(self.env.currencyPairs[target][pair].Current.last)
        elif target == 'USDT' or target == 'USD':
            corePair = 'BTC/' + target
            try:
                price = float(self.env.currencyPairs[target][pair].Current.last)
            except:
                price = float(self.env.currencyPairs['USDT'][corePair].Current.last)
                price *= float(self.env.currencyPairs['BTC'][pair].Current.last)
        if volume == 0.0:
            if type=='free':
                amount = self.Balances[currency]['free'] / price
            if type=='used':
                amount = self.Balances[currency]['used'] / price
            if type=='total':
                amount = self.Balances[currency]['total'] / price
        else:
            amount = volume/price
        return amount
    #Объем портфеля в target (включая открытые ордеры)

    def totalTo(self, target):
        result = 0.0
        for balance in self.Balances:
            if target == balance:
                result += self.Balances[balance]['total']
                continue
            try:
                result += self.convert(target=target, currency=balance, volume=self.Balances[balance]['total'])
            except:
                pass
        return result

    def show(self, target=None):
        dict = {}
        for balance in self.Balances:
            if self.Balances[balance]['free'] != 0.0:
                if target == None:
                    dict[balance] = self.Balances[balance]['free']
                else:
                    dict[balance] = self.convert(target, balance)
        print(dict)

    def showTotal(self, target=None):
        dict = {}
        for balance in self.Balances:
            if self.Balances[balance]['total'] != 0.0:
                if target == None:
                    dict[balance] = self.Balances[balance]['total']
                else:
                    dict[balance] = self.convert(target, balance, type='total')

    def totalActualTo(self, target=None):
        sum = 0.0
        for balance in self.Balances:
            if self.Balances[balance]['free'] != 0.0:
                if target == None:
                    sum += self.Balances[balance]['free']
                else:
                    sum += self.convert(target, balance)
        return sum

    def toZeros(self):
        for balance in self.Balances:
            self.Balances[balance]['free'] = 0.0
            self.Balances[balance]['used'] = 0.0
            self.Balances[balance]['total'] = 0.0

    def setBalance(self, target, amount):
        self.Balances[target]['free'] = amount
        self.Balances[target]['used'] = 0.0
        self.Balances[target]['total'] = amount

    def changeBalance(self, target, amount):
        self.Balances[target]['free'] += amount
        self.Balances[target]['total'] = self.Balances[target]['free']+self.Balances[target]['used']

