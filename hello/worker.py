from hello.environment import *
from preferences import *
from hello.system import TradingSystem
if not forheroku:
    import matplotlib.pyplot as plt
#Собственно здесь проживает сам бот.
class Worker():
    test = False
    def __init__(self, Test=False, length=500, counts=None, smoothy=20, API='', secret='', source='poloniex'):
        self.test = Test
        self.smooth = smoothy
        self.tradableCounts=counts
        self.environment = Environment(('BTC', 'USDT'), Test, length, API, secret, source)
        self.portfolio=Portfolio(self.environment)
        self.TS = TradingSystem(env=self.environment, port=self.portfolio)
        self.environment.Data = None
        self.TS.Data = None
    #Что сделать перед запуском бота?
    def prepare(self):
        td=timezone.now()
        self.environment.lastUpdateTime = td
        self.environment.LastPredictionTime = td - timedelta(minutes=historicStep+1)
        self.environment.getExcRates(test=False)
        if self.environment.wrapper.base != 'bitmex':
            for target in self.tradableCounts:
                self.environment.cutCurrenciesByVolume(target, self.tradableCounts[target])
        if self.test == True:
            self.environment.getAllData(td - timedelta(days=globalHistorySize), td, 1, minlen=self.environment.length)
        else:
            self.environment.getAllData(td - timedelta(weeks=6), td, 1)
        #self.environment.saveCurrencyPairs()
        #self.environment.loadCurrencyPairs()
        self.TS.addLayer(self.smooth, self.tradableCounts, 1.0)
        for i in range(len(self.TS.Layers)):
            self.TS.Layers[i].globalUpdate(self.portfolio, self.test, internalInstruments=True)
            self.TS.Layers[i].getLocalPortfolioShares(communism=True)
            #self.TS.Layers[i].Instruments['USDT']['BTC'].portfolioShare = 1.0
        if self.test == True:
            self.portfolio.toZeros()
            self.portfolio.setBalance('BTC', 1.0)
            self.portfolio.setBalance('USDT', 0.0)
    #Что делает бот каждый фрейм?
    def frame(self):
        td = timezone.now()
        if self.environment.lastUpdateTime + timedelta(hours=updateFrequency) < td:
            print('Обновление торговой системы...')
            for i in range(len(self.TS.Layers)):
                self.TS.Layers[i].globalUpdate(self.portfolio, self.test)
            self.environment.lastUpdateTime = td
            td = timezone.now()
        if self.test == False:
            self.portfolio.refresh()
            if self.environment.LastPredictionTime + timedelta(minutes=historicStep) < td:
                self.environment.LastPredictionTime = td
                self.environment.getExcRates(save=True)
                for Layer in self.TS.Layers:
                    Layer.frame(interact=False)
            else:
                for Layer in self.TS.Layers:
                    Layer.frame(interact=True)
        else:
            for Layer in self.TS.Layers:
                self.environment.getExcRates()
                Layer.frame(interact=False)
    #Я сказал стартуем!!
    def start(self):
        balanceUSDT = []
        balanceBTC = []
        success = []
        while(True):
            try:
                self.frame()
            except Exception as e:
                print(e)
                break
            if self.test == False:
                #print(self.environment.currencyPairs['BTC']['BTC_ETH'].Current.last)
                time.sleep(frameFrequency)
                self.portfolio.show(target='BTC')
            else:
                self.portfolio.show(target='BTC')
                print('total. BTC: ' + str(self.portfolio.totalTo(target='BTC')) + '. USDT: ' + str(self.portfolio.totalTo(target='USDT')))
                balanceUSDT.append(self.portfolio.totalTo(target='USDT'))
                balanceBTC.append(self.portfolio.totalTo(target='BTC'))
                success.append(self.TS.Layers[-1].historicEffectivity)
                print(self.environment.current)
        fig = plt.figure(1)
        fig.suptitle('BTC Balance', fontsize=14, fontweight='bold')
        plt.plot(balanceBTC)
        fig2 = plt.figure(2)
        fig2.suptitle('Эффективность торговой системы', fontsize=14, fontweight='bold')
        plt.plot(success)
        plt.show()
        saveData(self.TS.Layers[0].historicSuccess, 'success' + str(self.TS.Layers[0].smoothy))
        saveData(balanceBTC, 'balanceBTC20')
        sigs = (self.TS.Layers[-1].ticks, self.TS.Layers[-1].signals)
        saveData(sigs, 'signals20')

class HandPredictor():
    def __init__(self, smoothy, btcPairs=25, usdtPairs=10, days=None):
        self.smoothy = smoothy
        self.btcCount = btcPairs
        self.usdtCount = usdtPairs
        if days == None:
            self.days = 30
            self.length = 1000
            self.environment = Environment(length=1000)
        else:
            self.days = days
            self.length = (days - 30) * 144
            self.environment = Environment(length = self.length)
        # self.environment.current = length
        self.portfolio = Portfolio(self.environment)
        td = timezone.now()
        self.environment.getExcRates(test=False)
        self.environment.cutCurrenciesByVolume('BTC', self.btcCount)
        self.environment.cutCurrenciesByVolume('USDT', self.usdtCount)
        self.environment.getAllHistories(td - timedelta(days=self.days), td, 1, minlen=1000)
        self.environment.addLayer(smoothy, self.btcCount, self.usdtCount, 1.0)
        #self.environment.addLayer(self.smoothy+40, self.btcCount, self.usdtCount, 1.0)
        #self.environment.addLayer(self.smoothy+60, self.btcCount, self.usdtCount, 1.0)
        for i in range(len(self.TS.Layers)):
            self.TS.Layers[i].globalUpdate(self.portfolio, test=False, internalInstruments=True)
            self.TS.Layers[i].getLocalPortfolioShares(communism=True)
    def getPredictions(self):
        predictions = {'BTC':{}, 'USDT':{}}
        predictor = self.TS.Layers[-1].predictor
        for target in predictions:
            for cp in self.environment.CurrencyPairTypes[target]:
                predictions[target][cp] = self.TS.Layers[-1].predict(cp)
        for target in predictions:
            for cp in predictions[target]:
                print(cp + ': ')
                print(predictions[target][cp])
    def cycledPredictions(self):
        td = timezone.now()
        self.environment.lastPredictTime = td - timedelta(minutes=historicStep + 1)
        while True:
            td = timezone.now()
            self.environment.getExcRates()
            if self.environment.lastPredictTime + timedelta(minutes=historicStep) < td:
                self.environment.lastPredictTime = td
                predictions = {'BTC': {}, 'USDT': {}}
                for target in predictions:
                    for cp in self.environment.CurrencyPairTypes[target]:
                        predictions[target][cp] = {'20': self.TS.Layers[0].predict(cp, size=2), '60': self.TS.Layers[1].predict(cp, size=2)}
                                                   #'80': self.TS.Layers[1].predict(cp)}
                for target in predictions:
                    for cp in predictions[target]:
                        print(cp + ': ')
                        for pred in predictions[target][cp]:
                            print(pred + str(predictions[target][cp][pred]['prediction']))
            time.sleep(30)
            print(td)
    def emulateTradingSystem(self, pair):
        td = timezone.now()
        target = pair[pair.find('/')+1:]
        cp = self.environment.currencyPairs[target][pair]
        row = anl.toList(cp.GlobalHistory)
        row = row[len(row)-self.length:]
        signal = self.TS.Layers[0].predict(pair, size=len(row))
        signals = anl.fromStates(signal['prediction'], 0.0, 0.0)
        if not forheroku:
            vsl.visualiseSignals(row, signals)
            results = getProfit(row, signals)
            fig = plt.figure(2)
            fig.suptitle(pair + ' on balance', fontsize=14, fontweight='bold')
            t = plt.plot(results[0])
            plt.show()
        saveData((row, signals), 'strategy_' + str(self.smoothy) + '_' + pair)