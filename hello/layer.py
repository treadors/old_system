import math, os
from sklearn.preprocessing import MinMaxScaler, Normalizer, RobustScaler
from datetime import timedelta, timezone
import copy
from preferences import *
from hello.utils import loadData, saveData
from exceptions import *
from hello.committee import MoneyKeepingCommittee, ExpertSystem
from hello.environment import Environment, Portfolio
from hello.manager import MoneyManager
import hello.analysis as anl
from hitek.tsfresh_extraction import extract_multihistoric_features
from hitek.wavelets import multilevel_haar
from hello.instrument import Instrument
from hello.wrapper import CurrencyPair
from hello.StndardSignals import *
if not forheroku:
    import matplotlib
    matplotlib.use('Qt5Agg', warn=False, force=True)
    from matplotlib import pyplot as plt
    import hello.visuals as vsl
#Слой торговли. Слой отличается от слоя глубиной погружения в график (smooth) и количеством целевых валют для обмена с биткоином и долларом (tradablePairsCount)
class Layer():
    smoothy = int
    tradableCounts = []
    base_currenies = []
    TradableCurrencies = {}
    Instruments = {}
    predictor = MoneyKeepingCommittee
    moneyManager = MoneyManager
    #Share - доля портфеля, отводимая на слой.
    def __init__(self, environment, smooth, tradableCounts, Share, save=True):
        print('Создание торгового слоя (степень сглаживания: ' + str(smooth) + ')...')
        self.env = environment
        self.base_currenies = self.env.base_currencies
        self.smoothy = smooth
        self.mSmoothy = smooth + 10
        self.eSmoothy = smooth + 30
        self.fuzzier = 50
        self.smallHistory = int(smooth * 12 * 2) // self.fuzzier * self.fuzzier
        self.history = int(self.smoothy * 20 * 5) // self.fuzzier * self.fuzzier
        self.extendedHistory = int(self.eSmoothy * 25 * 7) // self.fuzzier * self.fuzzier
        self.batchSize = 50
        self.learningBatchSize = self.batchSize // 6
        self.fuzzyStepSmall = self.smallHistory // self.fuzzier
        self.fuzzyStep = self.history // self.fuzzier
        self.fuzzyStepExtended = self.extendedHistory // self.fuzzier
        self.tradableCounts = tradableCounts
        self.tangences = {}
        self.tangences['largeTG'] = int(self.fuzzyStepExtended * 3.22)
        self.tangences['middleTG'] = self.fuzzyStepExtended * 2
        self.tangences['smallTG'] = self.fuzzyStep * 2
        self.tangences['microTG'] = self.fuzzyStepSmall * 2
        self.regression = self.tangences['largeTG']
        self.backCompensation = self.smallHistory
        self.corellationHistory = self.extendedHistory  # int(112.816*math.exp(0.0397*smooth))
        self.historicEffectivityLen = 150  # (self.smoothy+10)
        self.enableSwitcher = True
        self.portfolioShare = Share
        self.originalPortfolioShare = Share
        if self.env.Data:
            self.env.Data.preferences = self.getPreferences()
            self.env.Data.glb_prepare_layer_indicators(self.getPreferences())
            self.env.setupGlobalHistory()
            self.Data = self.env.Data
        else:
            print('Expected to find the Data. An exception may occur in future. Please, load the data by '
                  'Environment.getAllData() func.')
        self.state = False
        self.moneyManager = MoneyManager(self.smoothy)
        print(self.getPreferences())

    #Здесь fuzzycells определит длину нечетких временных рядов, step - определит величину шага взятия истории при составлении датасета (6 = 1 час по умолчанию)
    def setHistorySize(self, smoothy, fuzzycells=50, step = 6):
        self.fuzzier = fuzzycells
        self.step = step
        self.history = int(smoothy[1]*30) // fuzzycells * fuzzycells
        self.smallHistory = int(smoothy[0]*20) // fuzzycells * fuzzycells
        self.extendedHistory = int(smoothy[2]*20*2.59*2) // step * step // fuzzycells * fuzzycells
        self.fuzzyStepSmall = int(self.smallHistory // self.fuzzier)
        self.fuzzyStep = int(self.history // self.fuzzier)
        self.fuzzyStepExtended = int(self.extendedHistory // self.fuzzier)
        self.batchSize = int(self.extendedHistory // step**2 * step)
        self.learningBatchSize = int(self.batchSize // step)
        self.tangences['largeTG'] = int(self.fuzzyStepExtended * 1.61)
        self.tangences['middleTG'] = int(self.fuzzyStepExtended)
        self.tangences['smallTG'] = int(self.fuzzyStep * 2)
        self.tangences['microTG'] = int(self.fuzzyStepSmall * 2)
        self.regression = int(self.tangences['largeTG'])
        self.backCompensation = int((self.smallHistory + self.history) / 2)
        self.corellationHistory = self.extendedHistory  # int(112.816*math.exp(0.0397*smooth))

    def getTradableCurrenciesByVolume(self):
        self.TradableCurrencies = {}
        for base in self.base_currenies:
            self.cutCurrenciesByVolume(base, self.tradableCounts[base])
            self.TradableCurrencies[base] = {}
            for pair in self.env.CurrencyPairTypes[base]:
                instrument = pair[:pair.find('/')]
                self.TradableCurrencies[base][instrument] = instrument

    def getTradableCurrencies(self, Defaults={'BTC': 'ETH/BTC', 'USDT': 'BTC/USDT'}):
        if len(Defaults.keys()) != len(self.base_currenies):
            raise ValueError("Defaults should be presented for each base currency!")
        topPairs = {}
        counts = {}
        for default in Defaults:
            topPairs[default] = {}
            topPairs[default][Defaults[default]] = 0.0
            counts[default] = 1
        topPairsBTC = {}
        topPairsUSDT = {}
        for base in self.base_currenies:
            while counts[base] < self.tradableCounts[base]:
                minCP = ''
                minCorr = 2.0
                for cp2 in self.env.CurrencyPairTypes[base]:
                    if cp2 in topPairs[base]:
                        continue
                    sumCorr=0.0
                    n = 0
                    for cp1 in topPairs[base]:
                        try:
                            sumCorr += self.Corellations[base][cp1][cp2]**2
                            n += 1
                        except:
                            pass
                    sumCorr = math.sqrt(sumCorr) / n
                    if sumCorr < minCorr:
                        minCP = cp2
                        minCorr = sumCorr
                topPairs[base][minCP] = sumCorr
                counts[base]+=1
        for base in self.base_currenies:
            topCurrencies = {}
            for toppair1 in topPairs[base]:
                sumCorr = 0.0
                for toppair2 in topPairs[base]:
                    if toppair1==toppair2:
                        continue
                    try:
                        sumCorr += self.Corellations[base][toppair1][toppair2]**2
                    except:
                        pass
                slash = toppair1.find('/');
                second = toppair1[:slash:]
                topCurrencies[second]=math.sqrt(sumCorr)
            self.TradableCurrencies[base] = topCurrencies

    def initializeTradingSystem(self, portfolio=None, committee=True):
        Defaults = {}
        self.Instruments = {}
        for target in self.env.currencyPairs:
            self.Instruments[target] = {}
            for cp in self.env.currencyPairs[target]:
                Defaults[target] = cp
                break
        self.getTradableCurrenciesByVolume()
        for target in self.TradableCurrencies:
            for instrument in self.TradableCurrencies[target]:
                self.Instruments[target][instrument]=Instrument(target, instrument, self.env, portfolio,
                                                                self.getPreferences())
        for target in self.TradableCurrencies:
            for pair in self.env.currencyPairs[target]:
                second = pair[:pair.find('/')]
                if not second in self.TradableCurrencies[target]:
                    self.env.currencyPairs[target][pair].GlobalHistory=[]
        self.getLocalPortfolioShares()
        self.setPredictor(committee)
        self.loadMoneyManager()

    def switch_test_mode(self, test):
        self.env.test = test
        for trg in self.Instruments:
            for instr in self.Instruments[trg]:
                self.Instruments[trg][instr].testing = test

    def generateTradingSystemFromPairs(self, pairs, portfolio=None):
        self.Instruments = {}
        for target in pairs:
            self.Instruments[target] = {}
            for pair in pairs[target]:
                instrument = pair[:pair.find('/')]
                self.Instruments[target][instrument] = Instrument(target, instrument, self.env, portfolio,
                                                                  self.getPreferences(), test=self.test)

    def cutTradingSystem(self, counts, shares=None, communism=False):
        Counts = {}
        for target in counts:
            self.tradableCounts[target]=counts[target]
            Counts[target] = 0
        tc = copy.deepcopy(self.TradableCurrencies)
        for target in tc:
            for instrument in tc[target]:
                if Counts[target] < counts[target]:
                    Counts[target] += 1
                else:
                    del self.Instruments[target][instrument]
                    del self.TradableCurrencies[target][instrument]
                    print('Удаление ' + instrument + '/' + target)
        self.getLocalPortfolioShares(shares=shares, communism=communism)

    def cutCurrenciesByVolume(self, target, count):
        cps = copy.deepcopy(self.env.CurrencyPairTypes[target])
        new_cps = []
        for i in range(count):
            maxVolume = 0.0
            maxVolumeCurrency = ''
            maxIndex = 0
            for j in range(len(cps)):
                v = float(self.env.currencyPairs[target][cps[j]].Current.baseVolume)
                if v > maxVolume:
                    maxVolume = v
                    maxVolumeCurrency = cps[j]
                    maxIndex = j
            new_cps.append(maxVolumeCurrency)
            cps.pop(maxIndex)
        self.env.CurrencyPairTypes[target] = new_cps

    def getPairs(self, pairsCount):
        result = []
        counts = []
        for targetCurrency in pairsCount:
            counts.append(0)
            for item in self.TradableCurrencies[targetCurrency]:
                if counts[-1] >= pairsCount[targetCurrency]:
                    continue
                else:
                    counts[-1] += 1
                pair = item + '/' + targetCurrency
                result.append(pair)
        return result

    def getLocalPortfolioShares(self, shares=None, communism=False):
        counts = {}
        for target in self.Instruments:
            counts[target] = len(self.Instruments[target].keys())
            for instrument in self.Instruments[target]:
                self.Instruments[target][instrument].portfolioShare = 1.0/counts[target]

    def getPreferences(self):
        condition = {'smoothy': self.smoothy, 'mSmoothy': self.mSmoothy, 'eSmoothy': self.eSmoothy,
                     'fuzzier': self.fuzzier, 'share': self.portfolioShare,
                    'smallHistory': self.smallHistory, 'history': self.history, 'extendedHistory': self.extendedHistory,
                    'corellationHistory': self.corellationHistory,
                    'fuzzyStepSmall': self.fuzzyStepSmall, 'fuzzyStep': self.fuzzyStep,
                    'fuzzyStepExtended': self.fuzzyStepExtended, 'regression': self.regression,
                    'batchSize': self.batchSize, 'learningBatchSize': self.learningBatchSize,
                     'portfolioShare': self.portfolioShare, 'predictor': self.predictor, 'predict': self.predict,
                     'moneyManager': self.moneyManager,
                     'currentSuccess': self.currentSuccess, 'globalDealHistory':self.globalDealHistory,
                     'microTG': self.tangences['microTG'], 'smallTG': self.tangences['microTG'],
                     'middleTG': self.tangences['middleTG'], 'largeTG': self.tangences['largeTG']}
        return condition

    def saveLayerConfiguration(self):
        name = 'layer' + str(self.smoothy)
        data = self.getPreferences()
        #saveData(data, name)

    def saveTradingSystem(self):
        for target in self.Instruments:
            for instrument in self.Instruments[target]:
                self.Instruments[target][instrument].layer = None
                self.Instruments[target][instrument].env = None
                self.Instruments[target][instrument].port = None
                self.Instruments[target][instrument].hist = None
                self.Instruments[target][instrument].history = None
                self.Instruments[target][instrument].source = None
                self.Instruments[target][instrument].Hist = None
        saveData((self.Instruments, self.TradableCurrencies, self.env.CurrencyTypes, self.env.CurrencyPairTypes),
                 'instruments' + str(self.smoothy))

    def loadTradingSystem(self, portfolio, test=False):
        name = 'instruments' + str(self.smoothy)
        tradingSystem = loadData(name)
        instruments = tradingSystem[0]
        self.TradableCurrencies = tradingSystem[1]
        toadd = []
        for target in instruments:
            for instrument in instruments[target]:
                if instrument in self.Instruments[target]:
                    self.Instruments[target][instrument].refresh(instruments[target][instrument])
                    self.Instruments[target][instrument].testing = test
                else:
                    self.Instruments[target][instrument] = instruments[target][instrument]
                    self.Instruments[target][instrument].layer = self
                    self.Instruments[target][instrument].env = self.env
                    self.Instruments[target][instrument].port = portfolio
                    self.Instruments[target][instrument].testing = test
                    self.Instruments[target][instrument].install()
                if not instrument in self.env.CurrencyTypes:
                    toadd.append(instrument)
        self.env.getCurrencies(reload=False)
        for instr in toadd:
            for tg in self.env.CurrencyPairTypes:
                pair = instr + '/' + tg
                if pair in self.env.CurrencyPairTypes[tg]:
                    self.env.currencyPairs[tg][pair] = CurrencyPair()
                    td = timezone.now()
                    self.env.getHistory(pair,td-timedelta(days=30),td,1)

        for target in self.Instruments:
            for instrument in self.Instruments[target]:
                if self.Instruments[target][instrument].succesfullyClosed:
                    if not instrument in instruments[target]:
                        self.Instruments[target].pop(instrument)
                    else:
                        self.Instruments[target][instrument].succesfullyClosed = False
                if not instrument in instruments[target]:
                    self.Instruments[target][instrument].closed = True
                else:
                    self.Instruments[target][instrument].closed = False

    def loadLayerConfiguration(self):
        name = 'layer' + str(self.smoothy)
        data = loadData(name)
        self.mSmoothy = data['mSmoothy']
        self.eSmoothy = data['eSmoothy']
        self.smallHistory = data['smallHistory']
        self.history = data['history']
        self.extendedHistory = data['extendedHistory']
        self.corellationHistory = data['corellationHistory']
        self.fuzzier = data['fuzzier']
        self.fuzzyStepSmall = data['fuzzyStepSmall']
        self.fuzzyStep = data['fuzzyStep']
        self.fuzzyStepExtended = data['fuzzyStepExtended']
        self.regression = data['regression']
        self.batchSize = data['batchSize']
        self.learningBatchSize = data['learningBatchSize']
        self.portfolioShare = data['share']
        self.tangences['largeTG'] = data['largeTG']
        self.tangences['middleTG'] = data['middleTG']
        self.tangences['smallTG'] = data['smallTG']
        self.tangences['microTG'] = data['microTG']
        #if self.env.Data:
        #    self.env.Data = self.getPreferences()
        print(data)

    def loadCommittee(self):
        dir = os.getcwd()
        self.predictor = MoneyKeepingCommittee(self.smoothy)
        try:
            self.predictor.load()
        except Exception as e:
            print('Не удалось загрузить комитет нейросетей! ({0})'.format(e))
            os.chdir(dir)

    def loadMoneyManager(self):
        self.moneyManager = MoneyManager(self.smoothy)
        dir = os.getcwd()
        try:
            self.moneyManager.load()
        except Exception as e:
            print('Не удалось загрузить систему управления капиталом! ({0})'.format(e))
            os.chdir(dir)

    #Если committee = False, то предиктор - экспертная система (см. committee.py ExpertSystem)
    def setPredictor(self, committee = True):
        if committee:
            self.loadCommittee()
        else:
            self.predictor = ExpertSystem(self.smoothy)

    def predict(self, cp, size=1, position=None, Type=None):
        this = self.env.Data[cp]
        length = this.index[-1] + 1
        if position == None:
            if self.env.test == True:
                position = self.env.current
            else:
                position = length
        if type(self.predictor) is MoneyKeepingCommittee:
            data = self.create_batch(cp, size=size, step=1, current=position, toPredict=True)
        elif type(self.predictor) is ExpertSystem:
            data = anl.toList(this[position-self.extendedHistory:position])
        prediction = self.predictor.predict(data, size, Type=Type)
        # if not self.env.test:
        #     self.env.popCurrentFromHistory()
        return prediction

    #Step - расстояние в тиках между обучающими примерами, division - частота взятия исторических данных
    def createDataSets(self, pairs, statesLength = 14, batch_size=1, step=1):
        cp = pairs[0]
        regression = self.tangences['largeTG']
        backCompensation = self.eSmoothy * 2
        this = self.env.Data[cp]
        print(this.shape)
        length = this.shape[0]
        lengths = [self.env.Data[cp].shape[0] for cp in pairs]
        position = self.extendedHistory + regression + backCompensation
        data = {'small': {}, 'middle': {},'large': {},'out': {},'outLarge': {}}
        isCreatedLocal = False
        Last = False
        while position < length:
            for pair in pairs:
                if not isCreatedLocal:
                    data = self.create_batch(pair, size=batch_size, step=step, current=position, toPredict=True)
                    isCreatedLocal = True
                else:
                    try:
                        res = self.create_batch(pair, size=batch_size, step=step, current=position, toPredict=True)
                        for category in data:
                            if category != 'batchDate':
                                for arr in data[category]:
                                    data[category][arr] = np.concatenate((data[category][arr], res[category][arr]), axis=0)
                            else:
                                data[category].append(*res[category])
                    except Exception as e:
                        print(e)
            print(str(position))
            if Last == True:
                break
            if length - position < batch_size*step:
                position = length - 1
                Last = True
            else:
                position += (batch_size)*step
        saveData(data, 'dataset' + str(self.smoothy))
        del data

    def create_batch(self, cp, size=1, step=1, current=None, toPredict=False):
        if not self.env.Data:
            raise DataError('No data available for {0}. Please create Data object for Environment and use'
                            'getAllData(...).'.format(cp))
        this = self.env.Data[cp]
        # length = this.shape[0]
        length = this.index[-1] + 1
        regression = self.tangences['largeTG']
        backCompensation = self.eSmoothy * 2 # Back compensation for smoothed states
        if current == None:
            if not self.env.test:
                position = self.env.current
            else:
                position = length
        else:
            position = current
        batchDate = this.loc[position-1]['timedate']
        # TODO: batchDate

        outputStatesS = np.zeros((1, 1), dtype=np.int32)
        outputStates = np.zeros((1, 1), dtype=np.int32)
        outputStatesE = np.zeros((1, 1), dtype=np.int32)
        outputMovesS = []
        outputMoves = []
        outputMovesE = []

        required_len = self.extendedHistory + (size - 1) * step + backCompensation
        if not toPredict:
            if length < required_len + regression:
                raise DataError('Not enought history of {0}. Required {1} data points, but have {2}.'
                                .format(cp, required_len, length))
            position -= regression
        else:
            if length < required_len:
                raise DataError('Not enought history of {0}. Required {1} data points, but have {2}.'
                                .format(cp, required_len, length))
        start = position - required_len

        f = anl.fuzzymodel(cells=self.fuzzier)

        count = 0
        i = 0
        isCreatedLocal = False
        while count < size:
            if toPredict:
                end = position + i - 1
                if position + i > length:
                    break
            else:
                end = position + i
                if position + i > length:
                    break
            # print(end)

            startS = end - self.smallHistory
            startM = end - self.history
            startE = end - self.extendedHistory
            if self.extendedHistory < 0:
                self.extendedHistory = 0


            sS = anl.toStandartSignals(this['Close'][startM-backCompensation:end].values, self.smoothy)
            sM = anl.toStandartSignals(this['Close'][startM-backCompensation:end].values, self.mSmoothy)
            sE = anl.toStandartSignals(this['Close'][startE-backCompensation:end].values, self.eSmoothy)

            scaledM = MinMaxScaler().fit_transform(this['Close'][startM:end].values.reshape(-1, 1)).flatten()
            scaledE = MinMaxScaler().fit_transform(this['Close'][startE:end].values.reshape(-1, 1)).flatten()

            # Doesn't include the last state, to avoid fitting on future
            iStatesS = f.fuzz(sS[-self.smallHistory:])
            iStatesSE = f.fuzz(sS[-self.history:])
            iStatesM = f.fuzz(sM[-self.history:])
            iStatesE = f.fuzz(sE[-self.extendedHistory:])
            iMovesM = f.fuzz(this['priceMoves'][startM:end].values)
            iMovesE = f.fuzz(this['priceMoves'][startE:end].values)
            iRowM = f.fuzz(this['Close'][startM:end].values)
            iRowE = f.fuzz(this['Close'][startE:end].values)
            iVolumesM = f.fuzz(this['volumeMoves'][startM:end].values)
            iVolumesE = f.fuzz(this['volumeMoves'][startE:end].values)
            iStohM = f.fuzz(this['stoh_m'][startM:end].values)
            iStohE = f.fuzz(this['stoh_e'][startE:end].values)
            iMacdM = f.fuzz(this['macd_m'][startM:end].values)
            iMacdE = f.fuzz(this['macd_e'][startE:end].values)
            iHoursM = f.fuzz(this['hours'][startM:end].values)
            iHoursE = f.fuzz(this['hours'][startE:end].values)
            iFiboM = f.fuzz(this['fibo_fuzzy_m'][startM:end].values)
            iFiboE = f.fuzz(this['fibo_fuzzy_e'][startE:end].values)
            iDepth25M = f.fuzz(this['depth_ratio_25per'][startM:end].values)
            iDepth25E = f.fuzz(this['depth_ratio_25per'][startE:end].values)

            tsfresh_features = extract_multihistoric_features(this, 'scaled_price', end,
                                                              (self.history, self.extendedHistory),
                                                              internal_fuzzier=100, external_fuzzier=self.fuzzier,
                                                              scaler=MinMaxScaler)

            iWaveletM = multilevel_haar(scaledM, MinMaxScaler, self.fuzzier, reconstruct=True)
            iWaveletE = multilevel_haar(scaledE, MinMaxScaler, self.fuzzier, reconstruct=True)

            iTSFreshM = tsfresh_features[0]
            iTSFreshE = tsfresh_features[1]

            iStatesS = np.reshape(iStatesS, (1, *iStatesS.shape))
            iStatesSE = np.reshape(iStatesSE, (1, *iStatesSE.shape))
            iStatesM = np.reshape(iStatesM, (1, *iStatesM.shape))
            iStatesE = np.reshape(iStatesE, (1, *iStatesE.shape))

            iMovesM = np.reshape(iMovesM, (1, *iMovesM.shape))
            iMovesE = np.reshape(iMovesE, (1, *iMovesE.shape))
            iRowM = np.reshape(iRowM, (1, *iRowM.shape))
            iRowE = np.reshape(iRowE, (1, *iRowE.shape))
            iVolumesM = np.reshape(iVolumesM, (1, *iVolumesM.shape))
            iVolumesE = np.reshape(iVolumesE, (1, *iVolumesE.shape))
            iStohM = np.reshape(iStohM, (1, *iStohM.shape))
            iStohE = np.reshape(iStohE, (1, *iStohE.shape))
            iMacdM = np.reshape(iMacdM, (1, *iMacdM.shape))
            iMacdE = np.reshape(iMacdE, (1, *iMacdE.shape))
            iHoursM = np.reshape(iHoursM, (1, *iHoursM.shape))
            iHoursE = np.reshape(iHoursE, (1, *iHoursE.shape))
            iFiboM = np.reshape(iFiboM, (1, *iFiboM.shape))
            iFiboE = np.reshape(iFiboE, (1, *iFiboE.shape))
            iDepth25M = np.reshape(iDepth25M, (1, *iDepth25M.shape))
            iDepth25E = np.reshape(iDepth25E, (1, *iDepth25E.shape))


            movesM = np.concatenate((iMovesM, iVolumesM))
            movesE = np.concatenate((iMovesE, iVolumesE))
            oscM = np.concatenate((iStohM, iMacdM))
            oscE = np.concatenate((iStohE, iMacdE))

            statesS = iStatesS.transpose()
            statesSE = iStatesSE.transpose()
            statesM = iStatesM.transpose()
            statesE = iStatesE.transpose()
            movesM = movesM.transpose()
            movesE = movesE.transpose()
            rowM = iRowM.transpose()
            rowE = iRowE.transpose()
            oscM = oscM.transpose()
            oscE = oscE.transpose()
            hoursM = iHoursM.transpose()
            hoursE = iHoursE.transpose()
            fiboM = iFiboM.transpose()
            fiboE = iFiboE.transpose()
            depth25M = iDepth25M.transpose()
            depth25E = iDepth25E.transpose()
            TSFreshM = iTSFreshM.transpose()
            TSFreshE = iTSFreshE.transpose()
            WaveletM = iWaveletM.transpose()
            WaveletE = iWaveletE.transpose()

            statesS = np.reshape(statesS, (1, *statesS.shape))
            statesSE = np.reshape(statesSE, (1, *statesSE.shape))
            statesM = np.reshape(statesM, (1, *statesM.shape))
            statesE = np.reshape(statesE, (1, *statesE.shape))
            movesM = np.reshape(movesM, (1, *movesM.shape))
            movesE = np.reshape(movesE, (1, *movesE.shape))
            rowM = np.reshape(rowM, (1, *rowM.shape))
            rowE = np.reshape(rowE, (1, *rowE.shape))
            oscM = np.reshape(oscM, (1, *oscM.shape))
            oscE = np.reshape(oscE, (1, *oscE.shape))
            hoursM = np.reshape(hoursM, (1, *hoursM.shape))
            hoursE = np.reshape(hoursE, (1, *hoursE.shape))
            fiboM = np.reshape(fiboM, (1, *fiboM.shape))
            fiboE = np.reshape(fiboE, (1, *fiboE.shape))
            depth25M = np.reshape(depth25M, (1, *depth25M.shape))
            depth25E = np.reshape(depth25E, (1, *depth25E.shape))

            TSFreshM = np.reshape(TSFreshM, (1, *TSFreshM.shape))
            TSFreshE = np.reshape(TSFreshE, (1, *TSFreshE.shape))
            WaveletM = np.reshape(WaveletM, (1, *WaveletM.shape))
            WaveletE = np.reshape(WaveletE, (1, *WaveletE.shape))

            if toPredict:
                TSFreshM, TSFreshE = self.rescale_tsf_features((TSFreshM, TSFreshE))

            if not isCreatedLocal:
                inputStatesSmall = statesS
                inputStatesExtended = statesSE
                inputStatesMiddle = statesM
                inputStatesLarge = statesE
                inputMovesMiddle = movesM
                inputMovesLarge = movesE
                inputRowMiddle = rowM
                inputRowLarge = rowE
                inputOscMiddle = oscM
                inputOscLarge = oscE
                inputHoursMiddle = hoursM
                inputHoursLarge = hoursE
                inputFiboMiddle = fiboM
                inputFiboLarge = fiboE
                input25perM = depth25M
                input25perE = depth25E
                inputTSFreshM = TSFreshM
                inputTSFreshE = TSFreshE
                inputWaveletM = WaveletM
                inputWaveletE = WaveletE

                if toPredict:
                    outputStatesS = np.array([this.loc[end]['states_s'], ])
                    outputStates = np.array([this.loc[end]['states_m'], ])
                    outputStatesE = np.array([this.loc[end]['states_e'], ])

                isCreatedLocal = True
            else:
                inputStatesSmall = np.concatenate((inputStatesSmall, statesS), axis=0)
                inputStatesExtended = np.concatenate((inputStatesExtended, statesSE), axis=0)
                inputStatesMiddle = np.concatenate((inputStatesMiddle, statesM), axis=0)
                inputStatesLarge = np.concatenate((inputStatesLarge, statesE), axis=0)
                inputMovesMiddle = np.concatenate((inputMovesMiddle, movesM), axis=0)
                inputMovesLarge = np.concatenate((inputMovesLarge, movesE), axis=0)
                inputRowMiddle = np.concatenate((inputRowMiddle, rowM), axis=0)
                inputRowLarge = np.concatenate((inputRowLarge, rowE), axis=0)
                inputOscMiddle = np.concatenate((inputOscMiddle, oscM), axis=0)
                inputOscLarge = np.concatenate((inputOscLarge, oscE), axis=0)
                inputHoursMiddle = np.concatenate((inputHoursMiddle, hoursM), axis=0)
                inputHoursLarge = np.concatenate((inputHoursLarge, hoursE), axis=0)
                inputFiboMiddle = np.concatenate((inputFiboMiddle, fiboM), axis=0)
                inputFiboLarge = np.concatenate((inputFiboLarge, fiboE), axis=0)
                input25perM = np.concatenate((input25perM, depth25M), axis=0)
                input25perE = np.concatenate((input25perE, depth25E), axis=0)
                inputTSFreshM = np.concatenate((inputTSFreshM, TSFreshM), axis=0)
                inputTSFreshE = np.concatenate((inputTSFreshE, TSFreshE), axis=0)
                inputWaveletM = np.concatenate((inputWaveletM, WaveletM), axis=0)
                inputWaveletE = np.concatenate((inputWaveletE, WaveletE), axis=0)

                if toPredict:
                    try:
                        outputStatesS = np.append(outputStatesS, [this.loc[end]['states_s'], ])
                    except:
                        outputStatesS = np.append(outputStatesS, [this.loc[end-1]['states_s'], ])
                    try:
                        outputStates = np.append(outputStates, [this.loc[end]['states_m'], ])
                    except:
                        outputStates = np.append(outputStates, [this.loc[end-1]['states_m'], ])
                    try:
                        outputStatesE = np.append(outputStatesE, [this.loc[end]['states_e'], ])
                    except:
                        outputStatesE = np.append(outputStatesE, [this.loc[end-1]['states_e'], ])

            count += 1
            i = count * step

        if not toPredict:
            outputMovesS = np.array(outputMovesS, dtype=np.float32)
            outputMoves = np.array(outputMoves, dtype=np.float32)
            outputMovesE = np.array(outputMovesE, dtype=np.float32)

        outputStatesS = outputStatesS.reshape(-1, 1)
        outputStates = outputStates.reshape(-1, 1)
        outputStatesE = outputStatesE.reshape(-1, 1)
        if not toPredict:
            outputMovesS = outputMovesS.reshape(-1, 1)
            outputMoves = outputMoves.reshape(-1, 1)
            outputMovesE = outputMovesE.reshape(-1, 1)
        # Собственно батч
        return {'small':
                     {'states': inputStatesSmall, 'statesE': inputStatesExtended},
                'middle':
                    {'states': inputStatesMiddle,
                     'row': inputRowMiddle, 'moves': inputMovesMiddle, 'osc': inputOscMiddle,
                     'hours': inputHoursMiddle, 'fibos': inputFiboMiddle, 'per25': input25perM,
                     'tsf': inputTSFreshM, 'wave': inputWaveletM},
                'large':
                    {'states': inputStatesLarge,
                     'row': inputRowLarge, 'moves': inputMovesLarge, 'osc': inputOscLarge, 'hours': inputHoursLarge,
                     'fibos': inputFiboLarge, 'per25': input25perE,
                     'tsf': inputTSFreshE, 'wave': inputWaveletE},
                'outSmall':
                    {'states': outputStatesS, 'moves': outputMovesS},
                'out':
                    {'states': outputStates, 'moves': outputMoves},
                'outLarge':
                    {'states': outputStatesE, 'moves': outputMovesE},
                'batchDate': [batchDate, ]}

    def rescale_datasets(self):
        data = loadData('dataset' + str(self.smoothy))
        tsf_m = data['middle']['tsf']
        tsf_e = data['large']['tsf']
        width = tsf_m.shape[1]
        height = tsf_m.shape[2]
        scalers_m, scalers_e = [], []
        for i in range(width):
            scalers_m.append([])
            scalers_e.append([])
            for j in range(height):
                row_m = tsf_m[:, i, j].reshape(-1, 1)
                row_e = tsf_e[:, i, j].reshape(-1, 1)
                scaler_m = MinMaxScaler().fit(X=row_m)
                scaler_e = MinMaxScaler().fit(X=row_e)
                tsf_m[:, i, j] = scaler_m.transform(row_m).flatten()
                tsf_e[:, i, j] = scaler_e.transform(row_e).flatten()
                scalers_m[i].append(scaler_m)
                scalers_e[i].append(scaler_e)
        scalers = (scalers_m, scalers_e)
        saveData(scalers, 'tsf_scalers' + str(self.smoothy))
        saveData(data, 'dataset' + str(self.smoothy))

    def rescale_tsf_features(self, tsf_features):
        if not hasattr(self, 'tsf_scalers'):
            self.tsf_scalers = loadData('tsf_scalers' + str(self.smoothy))
        scalers = self.tsf_scalers
        scalers_m, scalers_e = scalers[0], scalers[1]
        tsf_m, tsf_e = tsf_features[0], tsf_features[1]
        depth = tsf_m.shape[0]
        width = tsf_m.shape[1]
        height = tsf_m.shape[2]
        for i in range(width):
            for j in range(height):
                if depth == 1:
                    reshaped_m = scalers_m[i][j].transform(tsf_m[:, i, j].reshape(1, -1))
                    reshaped_e = scalers_e[i][j].transform(tsf_e[:, i, j].reshape(1, -1))
                else:
                    reshaped_m = scalers_m[i][j].transform(tsf_m[:,i,j].reshape(-1, 1))
                    reshaped_e = scalers_e[i][j].transform(tsf_e[:,i,j].reshape(-1, 1))
                tsf_m[:, i, j] = reshaped_m
                tsf_e[:, i, j] = reshaped_e
        return (tsf_m, tsf_e)

    # Обновление нейросети, сигнальных классификаций, корелляций, дисперсий, волатильностей..
    def globalUpdate(self, portfolio, test=False, committee=True, internalInstruments=False):
        self.setPredictor(committee)
        self.loadMoneyManager() #Вообще говоря, в режиме реальной торговли перегружать РЕАЛЬНЫЙ манименеджер на ТЕСТОВЫЙ может быть плохой идеей
        # self.loadLayerConfiguration()
        if not internalInstruments:
            self.loadTradingSystem(portfolio, test)
        else:
            self.generateTradingSystemFromPairs(self.env.CurrencyPairTypes, portfolio)


    #Управление капиталом. Торговая система.
    portfolioShare = float #Доля слоя в общем портфолио. Следим за тем, чтобы все доли в сумме были равны 1.
    currentSuccess = 0.0
    historicSuccess = []
    globalDealHistory = []
    historicEffectivity = 0.0
    historicEffectivityLen = 0
    knee = 0.75 #Пропорция, в которой делится отрезок исторической эффективности сети, (второй отрезок 1-knee)
    lastcount = 5 #Какие последние сделки? (каких больше, такой и режим)

    def setupPredictions(self):
        self.predictions = {}
        for tg in self.Instruments:
            self.predictions[tg] = {}
            for instr in self.Instruments[tg]:
                self.predictions[tg][instr] = {}
                self.Instruments[tg][instr].predictions = self.predictions[tg][instr]

    def savePredictions(self):
        saveData(self.predictions, 'predictions' + str(self.smoothy))
        for base in self.predictions:
            print(self.predictions[base].keys())

    def loadPredictions(self):
        self.predictions = loadData('predictions' + str(self.smoothy))
        for tg in self.Instruments:
            print(self.predictions[tg].keys())
            for instr in self.Instruments[tg]:
                self.Instruments[tg][instr].predictions = self.predictions[tg][instr]

    def test(self, lenght):
        pass

    def frame(self, interact=False, neural = True):
        self.historicEffectivity = sum(self.historicSuccess)
        #self.portfolioShare = self.defineFuzzyState()
        state = self.defineState()
        if state == False:
            self.portfolioShare = self.originalPortfolioShare * 0.02
            print('Режим экономии.')
        else:
            self.portfolioShare = self.originalPortfolioShare * 1.00
            print('Нормальный режим.')
        for target in self.Instruments:
            for instrument in self.Instruments[target]:
                try:
                    self.Instruments[target][instrument].applyPrediction(interact)
                except DataError as e:
                    print(str(e) + ' Skip.')
        self.historicSuccess.append(self.currentSuccess)
        if len(self.historicSuccess) > self.historicEffectivityLen:
            self.historicSuccess.pop(0)
        if self.currentSuccess != 0.0:
            self.globalDealHistory.append(self.currentSuccess)
        self.currentSuccess = 0.0
        # print(str(len(self.historicSuccess)))


    def defineState(self):
        self.historicEffectivity = sum(self.historicSuccess)
        laststack = []
        if len(self.historicSuccess) < self.historicEffectivityLen:
            effcount = 0
            uneffcount = 0
            for eff in self.historicSuccess:
                if eff > 0.0:
                    effcount += 1
                if eff < 0.0:
                    uneffcount += 1
            if effcount == 0 and uneffcount == 0:
                return False
            if effcount > uneffcount:
                return True
            else:
                return False
        else:
            breakpoint = int(self.historicEffectivityLen * self.knee)
            pre = self.historicSuccess[:breakpoint]
            post = self.historicSuccess[breakpoint:]
            mpre = anl.m(pre, 0.0)
            mpost = anl.m(post, 0.0)
            # print('PRE: ' + str(mpre) + '; POST: ' + str(mpost))
            if mpre == 0.0 and mpost == 0.0:
                return False
            if mpre > 0.0 and mpost >= 0.0:
                return True
            if mpre < 0.0 and mpost == 0.0:
                return False
            if mpre < 0.0 and mpost < 0.0:
                if abs(mpost/mpre) < 0.4:
                    return True
                return False
            if mpre < 0.0 and mpost > 0.0:
                if abs(mpost/mpre) < 0.2:
                    return False
                return True
            if mpre <= mpost:
                return True
            if mpre > mpost:
                return False
        '''
        for eff in self.historicSuccess:
            if len(laststack) == self.lastcount:
                break
            if eff >= 0.0:
                laststack.append(1)
            elif eff <= 0.0:
                laststack.append(0)
        if laststack == []:
            return False
        effcount = 0
        uneffcount = 0
        for item in laststack:
            if item == 0:
                uneffcount += 1
            else:
                effcount += 1
        if effcount > uneffcount:
            return True
        elif effcount <= uneffcount:
            return False
        '''

    def defineFuzzyState(self):
        f = anl.parametricFuzzyModel([-0.00943007, 0.00120403, 0.01234588], meanings=[0.02, 0.6, 1.0])
        point = anl.m(self.historicSuccess, 0.0)
        if point != 0.0:
            self.globalDealHistory.append(point)
        res = f.calculate(point)
        print('Текущий масштаб портфеля: ' + str(res) + ', Эффективность: ' + str(point))
        return res