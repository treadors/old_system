from datetime import timedelta, datetime
from django.utils import timezone
import time
import ccxt
import bitmex
from hello.models import *
from hello.utils import Try, TryKW
from preferences import *
import exceptions

class Wrapper():
    def __init__(self, source, API=None, secret=None):
        self.source = source
        self.API = API
        self.secret = secret
        self.base = source
        if source == 'poloniex':
            if API == None:
                self.source = ccxt.poloniex()
            else:
                self.source = ccxt.poloniex({
                    'apiKey': self.API,
                    'secret': self.secret,
                })
        elif source == 'binance':
            if API == None:
                self.source = ccxt.binance()
            else:
                self.source = ccxt.binance({
                    'apiKey': self.API,
                    'secret': self.secret,
                })
        elif source == 'bitmex':
            if API == None:
                self.source = ccxt.bitmex()
                self.bitmex_source = bitmex.bitmex()
            else:
                self.source = ccxt.bitmex()
                self.bitmex_source = bitmex.bitmex(test=True, api_key=self.API, api_secret=self.secret)
    def getAvailableCurrencies(self):
        currencies = []
        markets = Try(self.source.fetch_markets)
        if self.base == 'poloniex':
            for market in markets:
                if not market['quote'] in currencies:
                    currencies.append(market['quote'])
                if not market['base'] in currencies:
                    if int(market['info']['isFrozen']) == 0:
                        currencies.append(market['base'])
        if self.base == 'binance':
            for market in markets:
                if not market['quote'] in currencies:
                    currencies.append(market['quote'])
                if not market['base'] in currencies:
                    if market['active']:
                        currencies.append(market['base'])
        if self.base == 'bitmex':
            for market in markets:
                if not market['quote'] in currencies:
                    currencies.append(market['quote'])
                if not market['base'] in currencies:
                    if market['active']:
                        currencies.append(market['base'])
        return currencies
    def getAvailableTradingPairs(self, base_currencies):
        if self.base == 'bitmex':
            return self.getBitmexTradingPairs(base_currencies)[0]
        markets = Try(self.source.fetch_markets)
        trading_pairs = {}
        for base in base_currencies:
            trading_pairs[base] = []
        if self.base == 'poloniex':
            for market in markets:
                base = market['quote']
                if not base in trading_pairs:
                    continue
                quote = market['base']
                if int(market['info']['isFrozen']) == 1:
                    continue
                trading_pairs[base].append(market['symbol'])
        if self.base == 'binance':
            for market in markets:
                base = market['quote']
                if not base in trading_pairs:
                    continue
                quote = market['base']
                if not market['active']:
                    continue
                trading_pairs[base].append(market['symbol'])
        return trading_pairs
    def getBitmexTradingPairs(self, base_currencies):
        markets = Try(self.source.fetch_markets)
        trading_pairs = {}
        trading_symbols = {}
        for base in base_currencies:
            trading_pairs[base] = []
            trading_symbols[base] = []
        for market in markets:
            base = market['quote']
            if not base in trading_pairs:
                continue
            quote = market['base']
            if not market['active']:
                continue
            if base == quote:
                continue
            trading_pairs[base].append(quote + '/' + base)
            trading_symbols[base].append(market['symbol'])
        return trading_pairs, trading_symbols
    def getExcRate(self, cp):
        pass
    def getExcRates(self, pairs):
        excRates = {}
        td = datetime.utcnow()
        if self.base == 'bitmex':
            t_pairs, symbols = self.getBitmexTradingPairs(pairs.keys())
            for base in pairs:
                excRates[base] = {}
                for i in range(len(symbols[base])):
                    symbol = t_pairs[base][i]
                    e = TryKW(self.bitmex_source.Quote.Quote_get, symbol=symbols[base][i], count=1)
                    e = Try(e.result)[0][0]
                    e = self.bitmex_source.Quote.Quote_get(symbol=symbols[base][i], count=1).result()[0][0]
                    main = symbol[symbol.find('/') + 1:]
                    t = symbol[:symbol.find('/')]
                    exc_rate = Exc_rate(timedate=td, fromCoin=main, toCoin=t,
                                        last=(e['askPrice']+e['bidPrice'])/2, lowestAsk=e['askPrice'],
                                        highestBid=e['bidPrice'], percentChange=0.0,
                                        baseVolume=0.0, quoteVolume=0.0,
                                        isFrozen=0, high24hr=0.0,
                                        low24hr=0.0,
                                        ask1per=0.0, ask5per=0.0, ask10per=0.0,
                                        ask25per=0.0,
                                        bid1per=0.0, bid5per=0.0, bid10per=0.0,
                                        bid25per=0.0)
                    excRates[base][symbol] = exc_rate
            return excRates
        markets = Try(self.source.load_markets)
        symbols = markets.keys()
        try:
            # tickers = self.source.fetch_tickers(symbols)
            tickers = Try(self.source.fetch_tickers, symbols)
        except:
            tickers = {}
            for symbol in symbols:
                tickers[symbol] = Try(self.source.fetch_ticker, symbol)

        for base in pairs:
            excRates[base] = {}
            for symbol in pairs[base]:
                e = tickers[symbol]
                main = symbol[symbol.find('/')+1:]
                t = symbol[:symbol.find('/')]
                depth = self.getMarketDepths(symbol)
                exc_rate = Exc_rate(timedate=td, fromCoin=main, toCoin=t,
                                    last=e['last'], lowestAsk=e['ask'],
                                    highestBid=e['bid'], percentChange=e['change'],
                                    baseVolume=e['quoteVolume'], quoteVolume=e['baseVolume'],
                                    isFrozen=0, high24hr=e['high'],
                                    low24hr=e['low'],
                                    ask1per=depth[0][0], ask5per=depth[0][1], ask10per=depth[0][2],
                                    ask25per=depth[0][3],
                                    bid1per=depth[1][0], bid5per=depth[1][1], bid10per=depth[1][2],
                                    bid25per=depth[1][3])
                excRates[base][symbol] = exc_rate
        return excRates
    def setExcRates(self, pairs, current=None):
        exc_rates = {}
        if current == None:
            exc_rates = self.getExcRates(pairs)
        else:
            for base in pairs:
                exc_rates[base] = {}
                for pair in pairs[base]:
                    exc_rates[base][pair] = fromDataFrame(pairs[base][pair].GlobalHistory, current)
                    # exc_rates[base][pair] = pairs[base][pair].GlobalHistory[-current]
        for base in pairs:
            for pair in pairs[base]:
                pairs[base][pair].Update(exc_rates[base][pair])
    def getMarketDepths(self, cp):
        try:
            askbids = self.source.fetch_order_book(cp, 2000)
            askzero = float(askbids['asks'][0][0])
            bidzero = float(askbids['bids'][0][0])
        except:
            return [[0,0,0,0],[0,0,0,0]]
        asks = [askzero * 1.01, askzero * 1.05, askzero * 1.1, askzero * 1.25]
        bids = [bidzero * 0.99, bidzero * 0.95, bidzero * 0.9, bidzero * 0.75]
        sumAsk = 0.0
        sumBid = 0.0
        ask1per = 0.0
        ask5per = 0.0
        ask10per = 0.0
        ask25per = 0.0
        bid1per = 0.0
        bid5per = 0.0
        bid10per = 0.0
        bid25per = 0.0
        for ask in askbids['asks']:
            current = float(ask[0])
            if ask1per == 0.0 and current >= asks[0]:
                ask1per = sumAsk
            if ask5per == 0.0 and current >= asks[1]:
                ask5per = sumAsk
            if ask10per == 0.0 and current >= asks[2]:
                ask10per = sumAsk
            if ask25per == 0.0 and current >= asks[3]:
                ask25per = sumAsk
                break
            sumAsk += float(ask[1])
        for bid in askbids['bids']:
            current = float(bid[0])
            if bid1per == 0.0 and current <= bids[0]:
                bid1per = sumBid
            if bid5per == 0.0 and current <= bids[1]:
                bid5per = sumBid
            if bid10per == 0.0 and current <= bids[2]:
                bid10per = sumBid
            if bid25per == 0.0 and current <= bids[3]:
                bid25per = sumBid
                break
            sumBid += float(bid[1])
        return [[ask1per, ask5per, ask10per, ask25per], [bid1per, bid5per, bid10per, bid25per]]
    def getBidAskVolumes(self, cp):
        pass
    def getOrderBook(self, cp, depth):
        orderBooks = self.source.fetch_order
        pass
    def buyAtPrice(self, cp, price, amount):
        try:
            return self.source.create_limit_buy_order(cp, amount, price)
        except:
            return None
    def sellAtPrice(self, cp, price, amount):
        try:
            return self.source.create_limit_sell_order(cp, amount, price)
        except:
            return None
    def cancelOrder(self, orderNum):
        try:
            return self.source.cancel_order(orderNum)
        except:
            return None
    def getBalances(self):
        if self.base != 'bitmex':
            balance = Try(self.source.fetch_balance)
        Balances = {}
        Info = {}
        if self.base == 'poloniex':
            for currency in balance:
                curr = currency
                if curr == 'BTM':
                    curr = 'Bitmark'
                if curr == 'STR':
                    curr = 'XLM'
                if curr == 'info':
                    Info = balance[curr]
                else:
                    if curr in ('free', 'used', 'total'):
                        continue
                    Balances[curr] = {}
                    for type in balance[currency]:
                        Balances[curr][type] = float(balance[currency][type])
        elif self.base == 'bitmex':
            pass
        else:
            for currency in balance:
                if currency == 'info':
                    Info = balance[currency]
                else:
                    if currency in ('free', 'used', 'total'):
                        continue
                    Balances[currency] = {}
                    for type in balance[currency]:
                        Balances[currency][type] = float(balance[currency][type])
        return (Balances, Info)
    def getOpenOrders(self):
        return self.source.fetch_open_orders()
    def getOrderByID(self, id):
        try:
            return self.source.fetch_order(id)
        except:
            return None
    def getTodayOrders(self):
        yesterday = datetime.utcnow() - timedelta(days=1)
        return self.source.fetch_orders()
    def getFullExcRatesList(self):
        excRates = {}
        markets = Try(self.source.load_markets)
        symbols = markets.keys()
        tickers = Try(self.source.fetch_tickers, symbols)
        td = datetime.utcnow()
        for symbol in symbols:
            e = tickers[symbol]
            main = symbol[symbol.find('/')+1:]
            t = symbol[:symbol.find('/')]
            depth = self.getMarketDepths(symbol)
            if self.base == 'poloniex':
                exc_rate = Exc_rate(timedate=td, fromCoin=main, toCoin=t,
                                        last=e['last'], lowestAsk=e['ask'],
                                        highestBid=e['bid'], percentChange=e['change'],
                                        baseVolume=e['baseVolume'], quoteVolume=e['quoteVolume'],
                                        isFrozen=0, high24hr=e['high'],
                                        low24hr=e['low'],
                                        ask1per=depth[0][0], ask5per=depth[0][1], ask10per=depth[0][2],
                                        ask25per=depth[0][3],
                                        bid1per=depth[1][0], bid5per=depth[1][1], bid10per=depth[1][2],
                                        bid25per=depth[1][3])
            excRates[symbol] = exc_rate
        return excRates
    def getExcRatesListBitmex(self):
        excRates = {}
        pairs = self.getAvailableTradingPairs({'BTC',})
        excRates = self.getExcRates(pairs)
        return excRates['BTC']
    def saveExcRatesToDB(self, exc_rates):
        for exc_rate in exc_rates:
            try:
                exc_rates[exc_rate].save(using=self.base)
                print(str(exc_rate) + ' Succesfully.')
            except:
                print(str(exc_rate) + ' something goes wrong with saving...')
    def getRatesHistory(self, sellCoin, buyCoin, startDate, endDate, step):
        return Exc_rate.objects.using(self.base).filter(fromCoin__iexact=sellCoin, toCoin__iexact=buyCoin,
                                       timedate__range=(startDate, endDate)).order_by('timedate')[::step]
#Вся внешняя инфа о валютной паре.
class CurrencyPair():
    Current=Exc_rate
    exist=bool
    GlobalHistory=[]
    LocalHistory=[]
    historyExist=False
    HistoryStart=timezone
    HistoryEnd=timezone
    AskBook=[]
    BidBook=[]
    def __init__(self):
        self.exist = False
    #Обновить котировки.
    def Update(self, exc_rate):
        if not self.exist:
            pass
        elif exc_rate.timedate > self.HistoryEnd + timedelta(minutes=historicStep):
            self.GlobalHistory.append(self.Current)
            self.HistoryEnd = self.Current.timedate
            if self.HistoryEnd - timedelta(days=globalHistorySize):
                self.GlobalHistory.pop(0)
                self.HistoryStart = self.GlobalHistory[0].timedate
        self.Current = exc_rate
    def AddCurrent(self):
        self.GlobalHistory.append(self.Current)
    def DeaddCurrent(self):
        self.GlobalHistory.pop(len(self.GlobalHistory)-1)