﻿from scipy.stats.stats import spearmanr
import os, pickle
import numpy as np
import pandas as pd
import datetime
from sklearn.cluster import KMeans
from statistics import stdev, mean, median
from datetime import timedelta
from hello.StndardSignals import *
from hello.models import Exc_rate, toDictionary

def toStandartSignals(row, smooth):
    ss = StandardSignals(row, smooth)
    return ss.MakeHoldSignals()
def OrdinaryListSquares(Y, n):
    x1 = []
    Y1 = Y.copy()
    Y1 = Y1[len(Y1)-n:]
    minY = min(Y1)
    maxY = max(Y1)
    for i in range(n):
        x1.append([])
        x1[i].append(1)
        x1[i].append(i / (n-1))
        try:
            Y1[i] = (Y1[i] - minY) / (maxY - minY)
            if math.isnan(Y1[i]):
                Y1[i] = 0.0
        except:
            Y1[i] = 0.0
    res = []
    x = np.array(x1)
    xT = x.transpose()
    res = np.linalg.inv(np.dot(xT, x))
    res = np.dot(res, xT)
    res = np.dot(res, np.array(Y1))
    return res[1]
#Возвращает список значений индикатора для основного и дополнительного временных рядов.
#preInterval - необходимая история для корректной работы со стартовыми значениями mainInterval.
#postInterval - необходим, если моделируется регрессия (данные на самом деле есть, но их типа нет)
def EMA(period, mainInterval, postInterval=None):
    if type(mainInterval[0]) is Exc_rate:
        resultMain=[]
        ema=mainInterval[0].last
        alpha=2/(period+1)
        #resultMain.append(ema)
        for i in range(1,len(mainInterval)):
            resultMain.append(ema)
            ema=alpha*mainInterval[i].last+(1-alpha)*ema
        resultMain.append(ema)
        if postInterval != None:
            resultPost=[]
            for i in range(0, len(postInterval)):
                ema=alpha*postInterval[i].last+(1-alpha)*ema
                resultPost.append(ema)
            return (resultMain, resultPost)
        else:
            return resultMain
    else:
        resultMain = []
        ema = mainInterval[0]
        alpha = 2 / (period + 1)
        #resultMain.append(ema)
        for i in range(1, len(mainInterval)):
            resultMain.append(ema)
            ema = alpha * mainInterval[i] + (1 - alpha) * ema
        resultMain.append(ema)
        if postInterval != None:
            resultPost = []
            for i in range(0, len(postInterval)):
                ema = alpha * postInterval[i] + (1 - alpha) * ema
                resultPost.append(ema)
            return (resultMain, resultPost)
        else:
            return resultMain
#EMA1 - с меньшим периодом, EMA2 - с большим.
def MACD(period, EMASub):
    result = []
    return EMA(period, EMASub)
def STOH(period, pre, prices, smooth=None, appendNANs=False):
    if smooth == None:
        smooth = 18
    if period < 10:
        multiplier = 1
    else:
        multiplier = period//10
    if len(pre) < period:
        return None
    result = []
    row = [*pre, *prices]
    for i in range(len(pre), len(row)):
        tmp = row[i-period:i]
        try:
            result.append((row[i]-min(tmp))/(max(tmp)-min(tmp)))
        except:
            result.append(0.0)
    res = EMA(int(smooth*multiplier), result)
    if appendNANs:
        nans = [None for tick in pre]
        res = [*nans, *res]
    return res
    #return rescale(result, -1.0, 1.0)
def Stochastic(prices, period, smooth=1, appendNANs=False):
    if period < 10:
        multiplier = 1
    else:
        multiplier = period//10
    if len(prices) < period:
        raise Exception('Price row length should be larger than stochastic period.')
    result = []
    for i in range(period, len(prices)):
        tmp = prices[i-period:i]
        try:
            value = (prices[i]-min(tmp))/(max(tmp)-min(tmp))
            if math.isnan(value):
                value = 0.0
            if math.isinf(value):
                value = 1.0
        except:
            value = 0.0
        result.append(value)
    res = EMA(int(smooth*multiplier), result)
    if appendNANs:
        nans = [None for tick in range(period)]
        res = [*nans, *res]
    return res
def Bollinger(period, pre, prices, delta=2.5, smooth=None):
    hi=[]; lo=[]; ema=[]
    if smooth == None:
        smooth = period // 1.61
    if len(pre) < period:
        raise IOError("Недостаточно предыстории!")
    row = [*pre, *prices]
    ema = EMA(smooth, row)
    for i in range(len(pre), len(row)):
        #posneg = posNegDeviations(row[i-period:i])
        #hi.append(posneg[0])
        #lo.append(posneg[1])
        deviation = dev(ema[i-period:i])
        hi.append(ema[i]+deviation*delta)
        lo.append(ema[i]-deviation*delta)
    return [hi, ema[len(pre):], lo]
def BollingerNormalized(period, pre, prices, delta=2.5, smooth=None):
    hi = []
    lo = []
    if smooth == None:
        smooth = period // 1.61
    if len(pre) < period:
        raise IOError("Недостаточно предыстории!")
    row = [*pre, *prices]
    #ema = EMA(smooth, row)
    ema = []
    for i in range(len(pre), len(row)):
        #posneg = posNegDeviations(ema[i-period:i])
        #hi.append(posneg[0] * delta)
        #lo.append(posneg[1] * delta)
        deviation = dev(row[i - period:i])
        hi = deviation * delta
        lo = deviation * delta

        hi.append(deviation * delta)
        #ema.append(0.0)
        #lo.append(-deviation * delta)
    return [hi, ema, lo]
def Fibonacci100percent(row, fuzzy=False):
    minimum = min(row)
    maximum = max(row)
    diff = maximum-minimum
    percents = [0.0, 0.236, 0.382, 0.5, 0.618, 0.764, 1.0]
    meanings = [0, 1, 2, 3, 4, 5, 6]
    prices = [minimum+diff*percent for percent in percents]
    result = []
    if not fuzzy:
        for price in row:
            if price <= prices[1]:
                result.append(0)
            elif price <= prices[2]:
                result.append(1)
            elif price <= prices[3]:
                result.append(2)
            elif price <= prices[4]:
                result.append(3)
            elif price <= prices[5]:
                result.append(4)
            elif price <= prices[6]:
                result.append(5)
    else:
        pFModel = parametricFuzzyModel(prices, meanings)
        result = pFModel.calculate(row)
    return result
# Step is a step to fast Fibonnaccy eval.
#Course fibo required max and min among a large window, i decided to calculate several values at once.
def FibonacciSlippery(row, windowSize, step=10, fuzzy=False):
    if windowSize > len(row):
        raise Exception('Row len for Fibonacci levels evaluation should be larger than window size. '
                        'Your row len is {0}, but window size is {1}'.format(len(row), windowSize))
    result = Fibonacci100percent(row[:windowSize], fuzzy=fuzzy)
    last = len(result)
    while len(row) - len(result) > step:
        last += step
        res = Fibonacci100percent(row[last-windowSize:last], fuzzy=fuzzy)
        result.extend(res[len(res)-step:])
    res = Fibonacci100percent(row[len(row)-windowSize:len(row)], fuzzy=fuzzy)
    result.extend(res[len(res)-(len(row)-last):])
    return result

def ATR(period, pre, prices, zipped=False):
    multiplier=period//5
    row = [*pre, *prices]
    result = []
    for i in range(len(pre), len(row)):
        window = row[i-multiplier:i]
        dumpwindow = row[i-period:i]
        hilo = abs(max(window)-min(window))
        hipre = abs(max(window)-window[-1])
        lopre = abs(min(window)-window[-1])
        if zipped:
            result.append(max([hilo, hipre, lopre])/abs(max(dumpwindow)-min(dumpwindow)))
        else:
            result.append(max([hilo, hipre, lopre]))
    #subj = EMA(18*multiplier*2, result)
    normalize(result)
    return result
def deviations(period, pre, prices, VOLmode=False):
    pass
def toPriceMoves(row):
    result = []
    result.append(0.0)
    for i in range(1, len(row)):
        try:
            value = row[i]/row[i-1]-1.0
            if math.isnan(value) or math.isinf(value):
                value = 0.0
            result.append(value)
        except:
            result.append(0.0)
    return result
def dev(row):
    return stdev(row)
def m(row, ignoringValue=None):
    if ignoringValue == None:
        return mean(row)
    else:
        count = 0
        sum = 0.0
        for item in row:
            if item != ignoringValue:
                sum += item
                count += 1
        try:
            return sum/count
        except:
            return 0.0
def meanLocal(row):
    sum = 0.0
    for item in row:
        sum += float(item)
    return sum/len(row)
def posNegDeviations(row):
    m = meanLocal(row)
    plus = []
    minus = []
    for i in row:
        if i > m:
            plus.append(i)
        else:
            minus.append(i)
    pos = meanLocal(plus)
    neg = meanLocal(minus)
    return [pos-m, m-neg]

#Экспертные торговые системы
def doubleStochastic(row, period1, period2, smooth1, smooth2, onlyLast = True):
    maximumPeriod = max([period1, period2])
    pre = row[:maximumPeriod]
    row = row[maximumPeriod:]
    stoh1 = STOH(period1, pre, row, smooth1)
    stoh2 = STOH(period2, pre, row, smooth2)
    res = []
    for i in range(0, len(stoh1)):
        pass
def singleStochastic(row, period, smooth, upper, lower, closePercent = 0.3, onlyLast=False):
    pre = row[:period]
    row = row[period:]
    stoh = STOH(period, pre, row, smooth)
    #stoh = normalize(stoh)
    #stoh = [0.4, 0.3, 0.2, 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.71, 0.72, 0.75, 0.80, 0.77, 0.76, 0.75, 0.7, 0.6, 0.5, 0.4, 0.5, 0.6, 0.75, 0.4,
    #        0.31, 0.3, 0.29, 0.20, 0.17, 0.1, 0.08, 0.13, 0.14, 0.15, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.84, 0.8, 0.7]
    res = [[1.0,0.0], ]
    isOpened = False
    isLowerTouched = False
    isUpperTouched = False
    min = int
    max = int
    open = int
    close = int
    for i in range(1, len(stoh)):
        it = stoh[i]
        if it <= lower:
            if isLowerTouched:
                if it < min:
                    min = it
                else:
                    try:
                        progress = abs(it-min)/abs(open-min)
                        if progress >= closePercent:
                            isOpened = True
                    except:
                        pass
            else:
                isUpperTouched = False
                min = it
                open = it
                isLowerTouched = True
        if it >= upper:
            if isUpperTouched:
                if it > max:
                    max = it
                else:
                    try:
                        progress = abs(it-max)/abs(close-max)
                        if progress > closePercent:
                            isOpened = False
                    except:
                        pass
            else:
                isLowerTouched = False
                max = it
                close = it
                isUpperTouched = True
        if isOpened:
            res.append([0.0, 1.0])
        else:
            res.append([1.0, 0.0])
    result = np.array(res, dtype=np.float32)
    if onlyLast:
        return result[-1]
    else:
        return [stoh, result]
def expertMACD(row, periods, smooth, ignorances, closePercents, onlyLast = False):
    period1 = periods[0]
    period2 = periods[1]
    upper = 1.0+ignorances[0]
    lower = 1.0+ignorances[1]
    close1 = closePercents[0]
    close2 = closePercents[1]
    isOpened = False
    isLowerTouched = False
    isUpperTouched = False
    max = int
    min = int
    open = int
    close = int
    macd = MACDRatio(row, period1, period2, smooth)
    result = [0, ]
    for i in range(1, len(macd)):
        it = macd[i]
        if it <= lower:
            if isLowerTouched:
                if it < min:
                    min = it
                else:
                    try:
                        progress = abs(it-min)/abs(open-min)
                        if progress >= close1:
                            isOpened = True
                    except:
                        pass
            else:
                isUpperTouched = False
                min = it
                open = it
                isLowerTouched = True
        if it >= upper:
            if isUpperTouched:
                if it > max:
                    max = it
                else:
                    try:
                        progress = abs(it-max)/abs(close-max)
                        if progress >= close2:
                            isOpened = False
                    except:
                        pass
            else:
                isLowerTouched = False
                max = it
                close = it
                isUpperTouched = True
        if isOpened:
            result.append(1)
        else:
            result.append(0)
    if onlyLast:
        return result[-1]
    else:
        return [macd, result]
#Анализ временного ряда


#Получает стоплосс и тейк-профит. Mode:
def getHiLo(row, breaks=1.63):
    breakpoint = int(len(row)//breaks)
    row1 = row[:breakpoint]
    row2 = row[breakpoint:]
    m1 = meanLocal(row1)
    m2 = meanLocal(row2)
    credneH=0.0; countH = 0
    credneL=0.0; countL = 0
    for item in row1:
        if item < m1:
            countL += 1
            credneL += (m1-item)**2
        else:
            countH += 1
            credneH += (item-m1)**2
    hi1 = math.sqrt(credneH/countH)/m1
    lo1 = math.sqrt(credneL/countL)/m1
    credneH = 0.0; countH = 0
    credneL = 0.0; countL = 0
    for item in row2:
        if item < m2:
            countL += 1
            credneL += (m2 - item) ** 2
        else:
            countH += 1
            credneH += (item - m2) ** 2
    hi2 = math.sqrt(credneH / countH) / m2
    lo2 = math.sqrt(credneL / countL) / m2
    return ((hi1+hi2)/2, -(lo1+lo2)/2)
def getStopTakes(row, smooth):
    ss = StandardSignals(row, smooth)
    try:
        return ss.getHiLo()
    except:
        return getHiLo(row)

#ATR и EMA для стоп-лоссов.
def ATRMomental(window):
    hilo = abs(max(window)-min(window))
    hipre = abs(max(window)-window[-1])
    lopre = abs(min(window)-window[-1])
    return max([hilo, hipre, lopre])
def EMAMomental(window):
    alpha = 2 / (len(window) + 1)
    ema = window[0]
    for i in range(1, len(window)):
        ema = alpha * window[i] + (1 - alpha) * ema
    return ema

def EMASub(EMA1,EMA2):
    EMA3 = []
    for i in range(0, len(EMA1)):
        EMA3.append(EMA1[i]-EMA2[i])
    return EMA3
def EMARatio(EMA1,EMA2):
    EMA3 = []
    for i in range(0, len(EMA1)):
        EMA3.append(EMA1[i]/EMA2[i])
    return EMA3
def MACDRatio(prices, smooth1, smooth2, smooth):
    EMA1 = EMA(smooth1, prices)
    EMA2 = EMA(smooth2, prices)
    EMAR = EMARatio(EMA1, EMA2)
    return EMA(smooth, EMAR)
def updateEMA(period, row, value, lenght):
    alpha=2/(period+1)
    if len(row) >= lenght:
        row.pop(0)
    last = len(row)-1
    row.append(alpha*value+(1-alpha)*row[last])
def updateMACD(period, row, emaSub, lenght):
    updateEMA(period, row, emaSub, lenght)
def updateEMASub(row, ema1value, ema2value, lenght):
    if len(row) >= lenght:
        row.pop(0)
    last = len(row)-1
    row.append(ema1value-ema2value)
def updateSTOH(period, row, prices, lenght, smooth=None):
    if len(prices) < period:
        raise IOError("Недостаточно данных для обновления!")
    if len(row) >= lenght:
        row.pop(0)
    last = len(row)-1
    minN = 1000000.0
    maxN = -1000000.0
    for i in range(last-period+1,last):
        if prices[i].last < minN:
            minN=prices[i].last
        if prices[i].last > maxN:
            maxN=prices[i].last
        if smooth == None:
            row.append((prices[-1] - minN) / (maxN - minN))
        else:
            value = (prices[-1] - minN) / (maxN - minN)
            updateEMA(smooth,row,value,lenght)

#Расчет корреляции двух валютных пар. Желательно подавать туда скользящие средние с маленьким окном (сами цены валютных пар слишком хаотичны)
#Если подать непосредственно Exc_rate, вычислит скользящие средние сам.
def correlation(firstPair, secondPair):
    if len(firstPair) != len(secondPair):
        raise IOError("Временные ряды должны быть одинаковой длины!")
    pair1 = firstPair
    pair2 = secondPair
    if type(firstPair[0]) is Exc_rate:
        pair1 = EMA(12,firstPair)
    if type(secondPair[0]) is Exc_rate:
        pair2 = EMA(12,secondPair)
    return spearmanr(pair1,pair2)
def getPeriod(prices, start, end, step, elements = None):
    result = []
    starti = -1
    i = 0
    for item in prices:
        if item.timedate >= start and item.timedate <= end:
            if starti == -1:
                starti=i
            result.append(item)
        i+=1
    result = result[::step]
    if elements != None:
        if elements - 1 <= len(result) <= elements + 1:
            if len(result) < elements:
                result.append(prices[starti-step])
            if len(result) > elements:
                result.pop(0)
            return result
        else:
            return None
    else:
        if result == []:
            return None
    return result
#Ряд средневзвешенных по n предыдущим точкам
def getOLS(pre, prices, n):
    if len(pre) < n:
        return []
    result = []
    row = [*pre,*prices]
    for i in range(len(pre),len(row)):
        result.append(OrdinaryListSquares(row[i-n:i], n))
    return result
#Ряд средневзвешенных по n следующим точкам (регрессия средневзвешенного)
def getForwardOLS(prices, post, n):
    if len(post) < n:
        return []
    result = []
    row = [*prices, *post]
    for i in range(0, len(prices)):
        result.append(OrdinaryListSquares(row[i:i+n], n))
    return result
def getForward_OLS(prices, n, appendNANs=False):
    if len(prices) < n:
        raise Exception('Price row length should be larger than post-period N. Your row len in {0}, but N is {1}'.format(len(prices), n))
    res = []
    for i in range(0, len(prices) - n):
        res.append(OrdinaryListSquares(prices[i:i+n], n))
    if appendNANs:
        nans = [None for tick in range(n)]
        res = [*res, *nans]
    return res

def toList(prices, prediction = None, step=1):
    pr = []
    pr2 = []
    for i in range(0, len(prices), step):
        pr.append(float(prices[i].last))
    if prediction != None:
        for i in range(0, len(prediction), step):
            pr2.append(float(prediction[i].last))
        return (pr, pr2)
    return pr
def toVolume(prices, step=1):
    pr = []
    for i in range(0, len(prices), step):
        pr.append(float(prices[i].baseVolume))
    return pr
def toChanges(prices, step=1):
    pr = []
    for i in range(0, len(prices), step):
        pr.append(float(prices[i].percentChange))
    return pr
def to_DepthRatio(depth1, depth2, step=1):
    pr = []
    assert len(depth1) == len(depth2), "Lengths of arrays should be the same."
    for i in range(0, len(depth1), step):
        try:
            pr.append(float(depth1[i]/depth2[i]))
        except:
            pr.append(1.0)
    return pr

def toDepthRatio(prices, percent, step=1):
    pr = []
    if percent == 25:
        for i in range(0, len(prices), step):
            try:
                pr.append(float(prices[i].bid25per/prices[i].ask25per))
            except:
                pr.append(1.0)
    if percent == 10:
        for i in range(0, len(prices), step):
            try:
                pr.append(float(prices[i].bid10per/prices[i].ask25per))
            except:
                pr.append(1.0)
    return pr
def toHiLoRatio(prices, step=1):
    pr = []
    for i in range(0, len(prices), step):
        pr.append(float(prices[i].lowestAsk/prices[i].highestBid))
    return pr
def toLowestAsk(prices, step=1):
    pr = []
    for i in range(0, len(prices), step):
        pr.append(float(prices[i].lowestAsk))
    return pr
def toHighestBid(prices, step=1):
    pr = []
    for i in range(0, len(prices), step):
        pr.append(float(prices[i].highestBid))
    return pr
def toHours(prices, step = 1):
    hours = []
    for i in range(0, len(prices), step):
        hour = abs((prices[i].timedate.hour - 12))
        hours.append(hour)
    return hours

def to_Hours(dates, step=1):
    hours = []
    for i in range(0, len(dates), step):
        hour = abs(pd.to_datetime(pd.Timestamp(dates[i])).hour - 12)
        hours.append(hour)
    return hours

def ratio(arr1, arr2, smooth=0):
    res = []
    for i in range(0, len(arr1)):
        res.append(arr1[i]/arr2[i])
    if smooth != 0:
        return EMA(smooth, res)
    else:
        return res
def normalize(it):
    mi=min(it)
    ma=max(it)
    for i in range(0, len(it)):
        it[i]=(it[i]-mi)/(ma-mi)
    return it
def rescale(it, minimum, maximum):
    mi = min(it)
    ma = max(it)
    rang = maximum-minimum
    for i in range(0, len(it)):
        it[i] = ((it[i] - mi) / (ma - mi))*rang + minimum
    return it
def unzip(moves, regressionStep, startPrice=1.0):
    result = [startPrice,]
    #for i in range(1, len(moves)):
        #result.append(result[i-1] + result[i-1] * moves[i] / regressionStep)
    for i in range(0, len(moves), regressionStep):
        for j in range(regressionStep):
            if i==0 and j==0:
                continue
            result.append(result[i+j-1]+result[i+j-1]*moves[i]/regressionStep)
    return result

#Возвращает ряд логарифмов приращений цены
def toLnDifferences(it):
    result = []
    result.append(0.0)
    for i in range(1, len(it)):
        result.append(math.log(it[i]/it[i-1]))
    return result
#Делает ряд кратным n (специально для охуевших датасетов)
def cutRowTo(row, n):
    result = row
    while not len(result) % n == 0:
        result.pop(0)
    return result
def clusterisate(row, clusterCount):
    X = np.array(row)
    print(X.shape)
    X=X.reshape(-1, 1)
    print(X.shape)
    kmeans = KMeans(n_clusters=clusterCount, random_state=0).fit(X)
    centroids = kmeans.cluster_centers_
    data = np.array((0.20019479, 0.09647719, -0.08276109, 0.01251219, -0.03247942))
    data = data.reshape(-1,1)
    cls = kmeans.predict(data)
    print(centroids)
    print(cls)
    #with open(filename, 'wb') as f:
        #pickle.dump(kmeans, f)
        #f.close()
    return kmeans
def reshape(data, window, timesteps):
    flag=0
    sample=0
    while sample < len(data)-window:
        tmp = np.array(data[sample:sample+window])
        tmp = np.reshape(tmp, (1, *tmp.shape))
        if flag == 0:
            new_input = tmp
            flag = 1
        else:
            new_input = np.concatenate((new_input, tmp), axis=0)
        sample+=timesteps
    return new_input

#Работа с классами состояний
def toStates(array, confidence=None):
    confidence=np.mean(array, axis=0)
    confidence0=confidence[0]
    confidence1=confidence[1]
    states = []
    if array[0, 0] > array[0, 1]:
        states.append(0)
    else:
        states.append(1)
    if len(array)==1:
        return states[0]
    for i in range(1, len(array)):
        #if array[i,0] < confidence0 and array[i,2] < confidence1:
            #states.append(states[-1])
            #continue
        if array[i,0] > array[i,1]:
            states.append(0)
        elif array[i,1] > array[i,0]:
            states.append(1)
        #elif array[i,2] > array[i,1] and array[i,2] > array[i,0]:
            #states.append(1)
    return states
def fromStates(array, confidenceSell=None, confidenceBuy=None):
    result = []
    if confidenceBuy == None:
        confidenceBuy = 0.0
    if confidenceSell == None:
        confidenceSell = 0.0
    out = array[0]
    if out[0] > out[1]:
        result.append(0)
    else:
        result.append(1)
    for i in range(1, len(array)):
        out = array[i]
        if out[0] > out[1]:
            if out[0] > confidenceSell:
                result.append(0)
            else:
                result.append(result[i-1])
        else:
            if out[1] > confidenceBuy:
                result.append(1)
            else:
                result.append(result[i-1])
    return result
#States записывается как 0001111000... в данном случае классифицируем состояния по 3: [000]1111000, 0[001]111000, 00[011]11000, 000[111]1000...
def toStateClasses(array):
    classes = []
    for i in range(0, len(array)-3):
        states = array[i:i+3]
        if states == [0,0,0]:
            cl=0
        elif states == [1,0,0]:
            cl=1
        elif states == [0,1,0]:
            cl=2
        elif states == [0,0,1]:
            cl=3
        elif states == [0,1,1]:
            cl=4
        elif states == [1,0,1]:
            cl=5
        elif states == [1,1,0]:
            cl=6
        elif states == [1,1,1]:
            cl=7
        classes.append(cl)
    return classes
def toLongStateClasses(array, length = 14):
    classes = []
    half = length // 2
    for i in range(half, len(array)-half):
        states = array[i-half:i+half]
        first = states[:half]
        second = states[half:]
        state = []
        zeros = 0
        ones = 0
        for f in first:
            if f == 0:
                zeros += 1
            else:
                ones += 1
        if zeros > len(first) // 2:
            state.append(0)
        else:
            state.append(1)
        zeros = 0
        ones = 0
        for s in second:
            if s == 0:
                zeros += 1
            else:
                ones += 1
        if zeros > len(second) // 2:
            state.append(0)
        else:
            state.append(1)
        if state == [0,1]:
            classes.append(1)
        elif state == [1,1]:
            classes.append(1)
        elif state == [1,0]:
            classes.append(0)
        elif state == [0,0]:
            classes.append(0)
    return classes
#area - участок, в котором середина - текущий момент.
#mode = 0: states (0,1); mode = 1: stateClasses; mode = 2: longStateClasses.
def toState(area, mode=0):
    breakpoint = len(area)//2
    if mode == 0:
        return area[breakpoint]
    elif mode == 1:
        states = area[breakpoint:breakpoint+3]
        if states == [0, 0, 0]:
            cl = 0
        elif states == [1, 0, 0]:
            cl = 1
        elif states == [0, 1, 0]:
            cl = 2
        elif states == [0, 0, 1]:
            cl = 3
        elif states == [0, 1, 1]:
            cl = 4
        elif states == [1, 0, 1]:
            cl = 5
        elif states == [1, 1, 0]:
            cl = 6
        elif states == [1, 1, 1]:
            cl = 7
        return cl
    elif mode == 2:
        first = area[:breakpoint]
        second = area[breakpoint:]
        state = []
        zeros = 0
        ones = 0
        for f in first:
            if f == 0:
                zeros += 1
            else:
                ones += 1
        if zeros > len(first) // 2:
            state.append(0)
        else:
            state.append(1)
        zeros = 0
        ones = 0
        for s in second:
            if s == 0:
                zeros += 1
            else:
                ones += 1
        if zeros > len(second) // 2:
            state.append(0)
        else:
            state.append(1)
        if state == [0, 1]:
            cl = 1
        elif state == [1, 1]:
            cl = 1
        elif state == [1, 0]:
            cl = 0
        elif state == [0, 0]:
            cl = 0
        return cl
def toSignal(it, confidenceSell=None, confidenceBuy=None):
    if confidenceBuy == None:
        confidenceBuy = 0.0
    if confidenceSell == None:
        confidenceSell = 0.0
    if len(it.shape) == 1:
        if it[0] >= it[1]:
            return 0
        if it[1] > it[0]:
            if it[1] > confidenceBuy:
                return 1
            else:
                return 0
    elif len(it.shape) == 2:
        res = []
        for out in it:
            if out[0] >= out[1]:
                res.append(0)
            if out[1] > out[0]:
                if out[1] > confidenceBuy:
                    res.append(1)
                else:
                    res.append(0)
        return res

def tresholded_signals(it, confidenceBuy):

    return np.array([0 if out[1] < confidenceBuy else 1 for out in it])


def getStateMean(predictions):
    zeros = []
    ones = []
    for prediction in predictions:
        if prediction[0] > prediction[1]:
            zeros.append(prediction[0])
        else:
            ones.append(prediction[1])
    return [meanLocal(zeros), meanLocal(ones)]
#Нейросеть классифицирует состояния на 8 классов: [000], [100],.. [111]. Если нулей в представителе больше, чем единиц - продать, иначе - купить.
def fromStateClasses(output, confidenceSell=None, confidenceBuy=None):
    states = []
    if confidenceSell==None:
        confidenceSell=0.0
    if confidenceBuy==None:
        confidenceBuy=0.0
    sumSell = output[0]
    sumBuy = output[7]
    if sumSell > sumBuy:
        if sumSell > confidenceSell:
            states.append(0)
        else:
            states.append(1)
    else:
        if sumBuy > confidenceBuy:
            states.append(1)
        else:
            states.append(0)
    return states
def fromLongStateClasses(output, confidenceSell=None, confidenceBuy=None):
    result = []
    if confidenceBuy == None:
        confidenceBuy = 0.0
    if confidenceSell == None:
        confidenceSell = 0.0
    for out in output:
        if out[0] > out[1]:
            if out[0] > confidenceSell:
                result.append(0)
            else:
                result.append(0)
        else:
            if out[1] > confidenceBuy:
                result.append(1)
            else:
                result.append(0)
    return result
def fromComplexStateClasses(outputs, confidences=None):
    states = []
    if confidences==None:
        confidences=[]
        for output in outputs:
            confidences.append([0.0, 0.0, 0.0, 0.0])
    for i in range(len(outputs[0])):
        decisions = []
        for j in range(len(outputs)):
            sumSell = outputs[j][i][0]
            sumBuy = outputs[j][i][7]
            if sumSell > sumBuy:
                if sumSell > confidences[j][0]:
                    decisions.append(0)
                else:
                    decisions.append(1)
            else:
                if sumBuy > confidences[j][3]:
                    decisions.append(1)
                else:
                    decisions.append(0)
        #print(decisions)
        if decisions == [1,1,1]:
            states.append(1)
        else:
            states.append(0)
    return states
def fromMoveClasses(array):
    classes = []
    for i in range(0, len(array)):
        if array[i][0] > array[i][1] and array[i][2] and array[i][3] and array[i][4] and array[i][5] and array[i][6]:
            classes.append(0)
        elif array[i][1] > array[i][0] and array[i][2] and array[i][3] and array[i][4] and array[i][5] and array[i][6]:
            classes.append(1)
        elif array[i][2] > array[i][0] and array[i][1] and array[i][3] and array[i][4] and array[i][5] and array[i][6]:
            classes.append(2)
        elif array[i][3] > array[i][0] and array[i][1] and array[i][2] and array[i][4] and array[i][5] and array[i][6]:
            classes.append(3)
        elif array[i][4] > array[i][0] and array[i][1] and array[i][2] and array[i][3] and array[i][5] and array[i][6]:
            classes.append(4)
        elif array[i][5] > array[i][0] and array[i][1] and array[i][2] and array[i][3] and array[i][4] and array[i][6]:
            classes.append(5)
        elif array[i][6] > array[i][0] and array[i][1] and array[i][2] and array[i][3] and array[i][4] and array[i][5]:
            classes.append(6)
    return classes
#Сравнивает подкрепление и предсказания нейросети. type=0 - возвращает маску неправильных ответов в целом, type = 2 - отдельно неверных продать/купить.
def getMistakesMask(objectives, predictions, type=0):
    zeros = [] #Маска ошибочных "1"
    ones = [] #Маска ошибочных "0"
    for i in range(0, len(objectives)):
        if type == 0:
            if predictions[i] != objectives[i]:
                zeros.append(i)
        if type == 1:
            if predictions[i] == 0 and objectives[i] == 1:
                zeros.append(i)
            if predictions[i] == 1 and objectives[i] == 0:
                ones.append(i)
    if type == 0:
        return zeros
    elif type == 1:
        return [zeros, ones]

#Нечеткое моделирование временных рядов
def cut(npArray1, npArray2):
    if len(npArray1) == len(npArray2):
        return
    elif len(npArray1) > len(npArray2):
        while not len(npArray1) == len(npArray2):
            np.delete(npArray1,0)
    elif len(npArray2) > len(npArray1):
        while not len(npArray2) == len(npArray1):
            npArray2=np.delete(npArray2,0)


class fuzzymodel():
    smoothy = int
    step = int
    basisFunctionCount = int
    zeroclass = int
    def __init__(self, step=None, cells=None):
        self.step = step
        self.cells = cells
    def fuzz(self, row):
        if self.cells:
            self.basisFunctionCount = self.cells
            self.step = int(len(row)/self.basisFunctionCount)
        elif self.step:
            self.basisFunctionCount = round(len(row)/self.step)
        self.zeroclass = len(row) - self.basisFunctionCount*self.step
        self.A=np.zeros((self.basisFunctionCount, len(row)), dtype=np.float32)
        for i in range(0, self.zeroclass):
            self.A[0, i]=1.0
        for i in range(self.zeroclass, self.zeroclass+self.step):
            self.A[0, i]=1.0-((self.pos(i) % self.step)/self.step)
        for i in range(1, self.basisFunctionCount-1):
            for j in range(self.zeroclass+i*self.step, self.zeroclass+(i+1)*self.step):
                self.A[i, j] = ((self.pos(j) % self.step)/self.step)
            for j in range(self.zeroclass+(i+1)*self.step, self.zeroclass+(i+2)*self.step):
                self.A[i, j] = 1-((self.pos(j) % self.step)/self.step)
        for i in range(len(row)-self.step, len(row)):
            self.A[self.basisFunctionCount-1, i] = ((self.pos(i) % self.step) / self.step)
        self.F = self.components(row)
        return self.F

    def defuzz(self, row):
        pass
    def pos(self, i):
        return i-self.zeroclass
    def components(self, row):
        result = np.zeros(self.basisFunctionCount, dtype=np.float32)
        for i in range(self.basisFunctionCount):
            nom = 0.0; denom = 0.0
            for j in range(self.zeroclass+i*self.step, self.zeroclass+(i+1)*self.step):
                nom+=row[j]*self.A[i, j]
                denom+=self.A[i,j]
            result[i]=nom/denom
        return result
class parametricFuzzyModel():

    def __init__(self, centers, meanings=None):
        self.centers = centers
        self.meanings = meanings

    def attachmentDegrees(self, point):
        if point == None:
            point = 0.0
        attachmentDegrees = []
        for i in range(0, len(self.centers)):
            attachmentDegrees.append(0.0)
        if point <= self.centers[0]:
            attachmentDegrees[0] = 1.0
        elif point >= self.centers[-1]:
            attachmentDegrees[-1] = 1.0
        elif self.centers[0] < point < self.centers[-1]:
            for i in range(0, len(self.centers)):
                if self.centers[i] < point:
                    continue
                else:
                    toPrev = point - self.centers[i-1]
                    toNext = self.centers[i] - point
                    multiplier = 1.0/(toPrev+toNext)
                    attachmentDegrees[i-1] = toNext*multiplier
                    attachmentDegrees[i] = toPrev*multiplier
                    break
        self.degrees = attachmentDegrees
        return self.degrees

    def calculate(self, points):

        if isinstance(points, float) or isinstance(points, int) or isinstance(points, np.float32):
            degrees = self.attachmentDegrees(points)
            result = 0.0
            for i in range(len(degrees)):
                result += degrees[i]*self.meanings[i]
        elif isinstance(points, tuple) or isinstance(points, list):
            result = []
            for point in points:
                degrees = self.attachmentDegrees(point)
                res = 0.0
                for i in range(len(degrees)):
                    res += degrees[i]*self.meanings[i]
                result.append(res)
        elif isinstance(points, np.ndarray):
            result = []
            for j in range(points.shape[0]):
                degrees = self.attachmentDegrees(points[j])
                res = 0.0
                for i in range(len(degrees)):
                    res += degrees[i] * self.meanings[i]
                result.append(res)
        return result




