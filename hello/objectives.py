from tensorflow import reduce_sum, clip_by_value
from tensorflow import log as lg
from keras import backend as B
from keras.losses import categorical_crossentropy
def cbs(y_true, y_pred):
    return -reduce_sum(y_true*lg(clip_by_value(y_pred,1e-10,1.0-1e-10)))
def CBS(y_true, y_pred):
    y_pred = clip_by_value(y_pred, 1e-8, 1.0-1e-8)
    pred = B.eval(y_pred)
    truth = B.eval(y_true)
    l = pred.shape[0]
    if l == None:
        l = 0
    res = 0.0
    for i in range(l):
        if truth[i] > 0:
            res += 1.33 * truth[i]*lg(pred[i])
            print(truth[i])
        else:
            res += truth[i]*lg(pred[i])
    return -res
    #y_pred /= y_pred.sum(axis=-1, keepdims=True)
    #if y_true == 0.0 or y_true == 0:
    #    return 1.5*categorical_crossentropy(y_true, y_pred)
    #else:
    #    return categorical_crossentropy(y_true, y_pred)