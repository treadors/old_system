from django.db import models
from django.utils import timezone
import sys, os
import pandas as pd
os.environ['DJANGO_SETTINGS_MODULE'] = 'gettingstarted.settings'
from django.conf import settings
import django
django.setup()
# Create your models here.
class Greeting(models.Model):
    when = models.DateTimeField('date created', auto_now_add=True)

class Exc_rate(models.Model):
    fromCoin = models.CharField(max_length=10)
    toCoin = models.CharField(max_length=10)
    timedate = models.DateTimeField(
            default=timezone.now) 
    last = models.FloatField()
    lowestAsk = models.FloatField()
    highestBid = models.FloatField()
    percentChange = models.FloatField()
    baseVolume = models.FloatField()
    quoteVolume = models.FloatField()
    isFrozen = models.FloatField()
    high24hr = models.FloatField()
    low24hr = models.FloatField()
    ask1per = models.FloatField(null=True)
    ask25per = models.FloatField(null=True)
    ask5per = models.FloatField(null=True)
    ask10per = models.FloatField(null=True)
    bid1per = models.FloatField(null=True)
    bid25per = models.FloatField(null=True)
    bid5per = models.FloatField(null=True)
    bid10per = models.FloatField(null=True)

    def __str__(self):
        return self.toCoin + '/' + self.fromCoin

    def toDict(self):
        dict = {}
        dict['fromCoin'] = str(self.fromCoin)
        dict['toCoin'] = str(self.toCoin)
        dict['symbol'] = str(self)
        dict['timedate'] = self.timedate
        dict['Close'] = float(self.last)
        dict['Low'] = float(self.lowestAsk)
        dict['High'] = float(self.highestBid)
        dict['percentChange'] = float(self.percentChange)
        dict['baseVolume'] = float(self.baseVolume)
        dict['quoteVolume'] = float(self.quoteVolume)
        dict['isFrozen'] = bool(self.isFrozen)
        dict['high24hr'] = float(self.high24hr)
        dict['low24hr'] = float(self.low24hr)
        try:
            dict['ask1per'] = float(self.ask1per)
            if dict['ask1per'] == 0.0:
                dict['ask1per'] = 1.0
        except:
            dict['ask1per'] = 1.0
        try:
            dict['ask25per'] = float(self.ask25per)
            if dict['ask25per'] == 0.0:
                dict['ask25per'] = 1.0
        except:
            dict['ask25per'] = 1.0
        try:
            dict['ask5per'] = float(self.ask5per)
            if dict['ask5per'] == 0.0:
                dict['ask5per'] = 1.0
        except:
            dict['ask5per'] = 1.0
        try:
            dict['ask10per'] = float(self.ask10per)
            if dict['ask10per'] == 0.0:
                dict['ask10per'] = 1.0
        except:
            dict['ask10per'] = 1.0
        try:
            dict['bid1per'] = float(self.bid1per)
            if dict['bid1per'] == 0.0:
                dict['bid1per'] = 1.0
        except:
            dict['bid1per'] = 1.0
        try:
            dict['bid25per'] = float(self.bid25per)
            if dict['bid25per'] == 0.0:
                dict['bid25per'] = 1.0
        except:
            dict['bid25per'] = 1.0
        try:
            dict['bid5per'] = float(self.bid5per)
            if dict['bid5per'] == 0.0:
                dict['bid5per'] = 1.0
        except:
            dict['bid5per'] = 1.0
        try:
            dict['bid10per'] = float(self.bid10per)
            if dict['bid10per'] == 0.0:
                dict['bid10per'] = 1.0
        except:
            dict['bid10per'] = 1.0
        return dict

def fromDict(dict):
    exc_rate = Exc_rate(fromCoin=dict['fromCoin'], toCoin=dict['toCoin'], timedate=dict['timedate'],
                        last=dict['Close'], lowestAsk=dict['Low'], highestBid=dict['High'],
                        percentChange=dict['percentChange'], baseVolume=dict['baseVolume'],
                        quoteVolume=dict['quoteVolume'], isFrozen=dict['isFrozen'], high24hr=dict['high24hr'],
                        low24hr=dict['low24hr'], ask1per=dict['ask1per'], ask25per=dict['ask25per'],
                        ask5per=dict['ask5per'], ask10per=dict['ask10per'], bid1per=dict['bid1per'],
                        bid25per=dict['bid25per'], bid5per=dict['bid5per'], bid10per=dict['bid10per'])
    return exc_rate
def fromDataFrame(df, position):
    dict = df.loc[position, :]
    dict = fromDict(dict)
    return dict

def toDictionary(exc_rates):
    if not exc_rates:
        return {}
    if not isinstance(exc_rates[0], Exc_rate):
        raise Exception("Ожидается массив котировок в формате Exc_rate.")
    result = {}
    for category in exc_rates[0].toDict():
        result[category] = []
    for i in range(len(exc_rates)):
        dict = exc_rates[i].toDict()
        for category in dict:
            result[category].append(dict[category])
    return result

