from datetime import datetime
import backtrader as bt
import backtrader.analyzers as btanalyzers
from backtrader import Order
import matplotlib.pyplot as plt

from hello.environment import Environment, Portfolio
from backtrader.utils import AutoOrderedDict
from preferences import commission, slippage

# Бэктестер. Работает так: создаешь, -> prepare -> run -> make conclusions

class BackTester():

    def __init__(self, layer, portfolio=None, startcash=None, **kwargs):
        self.layer = layer
        self.env = layer.env
        if not portfolio:
            self.port = Portfolio(self.env)
        else:
            self.port = portfolio
        self.startcash = startcash
        # Это и есть торговый движок, в него мы устанавливаем данные, стратегии, визуализацию и мани-менеджмент
        self.cerebro = bt.Cerebro()
        self.layer.success = []
        self.layer.balanceBTC = [startcash, ]
        self.confidence_level_buy = kwargs['confidence_level_buy'] if 'confidence_level_buy' in kwargs else 0.5
        self.confidence_level_sell = kwargs['confidence_level_sell'] if 'confidence_level_sell' in kwargs else 0.5
        self.minProfit = kwargs['minProfit'] if 'minProfit' in kwargs else 0.0
        self.historic_eff_len = kwargs['historic_eff_len'] if 'historic_eff_len' in kwargs else 90
        self.enableSwitcher = kwargs['enableSwitcher'] if 'enableSwitcher' in kwargs else False

    def feedData(self):
        print(self.layer.Data.tableNames)
        for symbol in self.layer.Data.tableNames:
            self.layer.Data[symbol]['index'] = self.layer.Data[symbol].index
            data = StandartPriceData(dataname=self.layer.Data[symbol])
            self.cerebro.adddata(data=data, name=symbol)

    def prepare(self, startcash=None):
        if startcash:
            self.startcash = startcash
            self.port.toZeros()
            self.port.setBalance('BTC', float(startcash))
        self.env.current = 0
        self.cerebro.addstrategy(StandartStrategy, layer=self.layer,
                                 confidence_level_buy=self.confidence_level_buy,
                                 confidence_level_sell=self.confidence_level_sell,
                                 historic_eff_len=self.historic_eff_len,
                                 minProfit=self.minProfit,
                                 enableSwitcher=self.enableSwitcher)
        self.cerebro.addanalyzer(btanalyzers.SharpeRatio, _name='sharpe')
        self.cerebro.addanalyzer(btanalyzers.DrawDown, _name='drawdown')
        self.cerebro.addsizer(StandartSizer, layer=self.layer,
                              count=len(self.layer.Data.tableNames)+0.005)
        self.feedData()

        self.cerebro.broker = bt.brokers.BackBroker(slip_perc=slippage)
        self.cerebro.broker.set_coc(True)
        self.cerebro.broker.set_coo(True)
        self.cerebro.broker.set_cash(self.startcash)
        self.cerebro.broker.setcommission(commission)

    def run(self):
        self.runner = self.cerebro.run()

    def make_conclusions(self, plot=False):
        # Get final portfolio Value
        portvalue = self.cerebro.broker.getvalue()
        pnl = portvalue - self.startcash

        # Print out the final result
        print('Final Portfolio Value: ${}'.format(portvalue))
        print('P/L: ${}'.format(pnl))
        print('Sharpe Ratio: ', self.runner[0].analyzers.sharpe.get_analysis())
        print('Draw Down: ', self.runner[0].analyzers.drawdown.get_analysis())
        # Finally plot the end results
        self.layer.moneyManager.save()
        self.layer.savePredictions()
        if plot:
            self.cerebro.plot(iplot=True, volume=False)
            fig = plt.figure(1)
            fig.suptitle('BTC Balance', fontsize=14, fontweight='bold')
            plt.plot(self.layer.balanceBTC)
            fig2 = plt.figure(2)
            fig2.suptitle('Эффективность торговой системы', fontsize=14, fontweight='bold')
            plt.plot(self.layer.success)
            plt.show()
        return {
            'value': portvalue,
            'pnl': pnl,
            'sharpe': self.runner[0].analyzers.sharpe.get_analysis(),
            'drawdown': self.runner[0].analyzers.drawdown.get_analysis()
        }

# В этом классе мы приконнекчиваем данные из системы к бэктестеру. А потом

class StandartPriceData(bt.feeds.PandasData):
    params = (
        ('datetime', 'timedate'),
        ('open', 'Low'),  #  To connect with backtrader logic this is our entry price meaning the lowestAsk actually
        ('high', 'High'),
        ('low', 'Low'),
        ('close', 'High'), #  Our close price is highestBid as highest price which we can execute
        ('volume', 'quoteVolume'),
        ('openinterest', 'index')
    )

# Здесь мы приконнекчиваем стратегию к бэктестеру.

class StandartStrategy(bt.Strategy):

    # Это некоторые параметры, которые можно менять, и от этого поведение бэктестера поменяется.

    params = (
        ('layer', None),
        ('saved_predictions', True),
        ('silent', True),
        ('confidence_level_buy', 0.5),
        ('confidence_level_sell', 0.5),
        ('historic_eff_len', 90),
        ('minProfit', 0.0),
        ('enableSwitcher', False)
    )

    def __init__(self):
        super().__init__()
        # При создании стратегии мы вытаскием параметры из params (см. выше), чтобы их потом юзать
        self.layer = self.params.layer
        self.use_save_prediction = self.params.saved_predictions
        if self.use_save_prediction:
            self.layer.loadPredictions()
        else:
            self.layer.setupPredictions()
        self.environment = self.layer.env
        bt.indicators.AverageTrueRange(self.datas[0])
        self.state = False

        self.silent = self.params.silent
        self.confidence_level_buy = self.params.confidence_level_buy
        self.confidence_level_sell = self.params.confidence_level_sell
        self.minProfit = self.params.minProfit
        for tg in self.layer.Instruments:
            for instr in self.layer.Instruments[tg]:
                self.layer.Instruments[tg][instr].confidence_level_buy = self.confidence_level_buy
                self.layer.Instruments[tg][instr].confidence_level_sell = self.confidence_level_sell
                self.layer.Instruments[tg][instr].minProfit = self.minProfit
        self.layer.historicEffectivityLen = self.params.historic_eff_len
        self.layer.enableSwitcher = self.params.enableSwitcher

    # Эта функция выполняется каждый раз, когда бэктестер переходит ко следующей временной отметке (по умолчанию
    # у меня - делает шаг 10 мин. 10 минут по истории, которая была загружена в Backtester -> prepare)

    def next(self):

        self.layer.state = self.layer.defineState()

        for i, d in enumerate(self.datas):
            prediction = None
            current = int(d.openinterest[0])
            self.environment.current = current
            self.environment.getExcRates(test=True)
            self.environment.current = current
            date, name = self.datetime.date(0), d._name
            try:
                instr = name[:name.find('/')]
                trg = name[name.find('/') + 1:]
                instrument = self.layer.Instruments[trg][instr]
                if not self.use_save_prediction:
                    instrument.applyPrediction(interact=False)
                else:
                    instrument.applyPrediction(interact=False, use_saved_predictions=True)
                prediction, pred = instrument.exp, instrument.signal
                if not self.silent:
                    print('{} {} (Backtest: {}, System: {}) {} VALUE {}. PRED: {}'.format(current, name, d.close[0],
                                                                                             instrument.last, date,
                                                                                             self.broker.getvalue(), pred))
            except (IndexError, KeyError) as e:
                if not self.silent:
                    print(e)
            if prediction != None:
                pos = self.getposition(d).size
                if not pos:  # no market / no orders
                    if pred == 1:
                        self.prediction_buy = instrument.buyPrediction
                        if not self.silent:
                            print('{} BUY CREATE, {}, CONFIDENCE: {}'.format(name, d.open[0], self.prediction_buy))
                        self.buy(data=d)
                else:
                    if pred == 0:
                        self.prediction_sell = instrument.sellPrediction
                        # self.close(data=d)
                        if not self.silent:
                            print('{} SELL CREATE, {}, SIZE: {}, CONFIDENCE: {}'.format(name, d.close[0], pos, self.prediction_sell))
                        self.close(data=d)
                        # self.sell(data=d, exectype=Order.Close)
        self.renew_success()

    # Здесь обновляется история успешности торговли. Вообще она нахуй не нужна, её нужно перенести в moneyManager,
    # но ыыыыыыыыыыыы

    def renew_success(self):
        self.layer.historicSuccess.append(self.layer.currentSuccess)
        if len(self.layer.historicSuccess) > self.layer.historicEffectivityLen:
            self.layer.historicSuccess.pop(0)
        if self.layer.currentSuccess != 0.0:
            self.layer.globalDealHistory.append(self.layer.currentSuccess)
        self.layer.currentSuccess = 0.0
        self.layer.historicEffectivity = sum(self.layer.historicSuccess)
        self.layer.success.append(self.layer.historicEffectivity)
        self.layer.balanceBTC.append(self.broker.getvalue())


    # В этой функции записывается статистика торговли, она вызывается каждый раз, когда бэктестер совершает действие (продать/купить)

    def notify_trade(self, trade):
        if not hasattr(self, 'trade_info'):
            self.trade_info = {}
        dt = self.data.datetime.date()
        dtm = datetime.combine(dt, self.data.datetime.time())
        if trade.isopen:
            if not self.silent:
                print('{} Opened: Price {}, Time {}'.format(
                    trade.data._name,
                    trade.data.close[0],
                    dtm))
            factic_open_price = trade.data.low[0] * (1 + slippage + commission)
            self.trade_info[trade.data._name] = {'symbol': trade.data._name, 'ideal_open_price': trade.data.low[0],
                                                 'factic_open_price': factic_open_price,
                               'open_td': dtm, 'open_conf': self.prediction_buy[1]}
        if trade.isclosed:
            symbol = trade.data._name
            factic_close_price = trade.data.high[0] * (1 - slippage - commission)
            perc_pnl = (trade.data.high[0]/self.trade_info[symbol]['ideal_open_price']) - 1.0
            factic_perc_pnl = (factic_close_price / self.trade_info[symbol]['factic_open_price']) - 1.0
            if not self.silent:
                print('{} {} Closed: PnL Gross {}, Net {}, Perc = ({} / {}) - 1.0 = {}'.format(
                    dt,
                    symbol,
                    trade.pnl,
                    trade.pnlcomm,
                    trade.data.high[0], self.trade_info[symbol]['ideal_open_price'], perc_pnl))
            self.trade_info[symbol].update({'ideal_close_price': trade.data.high[0], 'factic_close_price': factic_close_price,
                                            'close_td': dtm, 'close_conf': self.prediction_sell[0],
                                            'ideal_abs_pnl': trade.pnl, 'factic_abs_pnl': trade.pnlcomm,
                                            'ideal_perc_pnl': perc_pnl, 'factic_perc_pnl': factic_perc_pnl})
            self.layer.moneyManager.add_trade(self.trade_info[symbol])
            self.layer.currentSuccess += perc_pnl

# Этот класс управляет капиталом. Функция _getsizing вызывается каждый раз перед тем, как бэктестер совершает сделку купли/ продажи,
# чтобы определить размер сделки.

class StandartSizer(bt.Sizer):

    params = (('layer', None),
              ('count', 1.02))

    def __init__(self):
        super().__init__()
        self.layer = self.params.layer
        self.count = self.params.count
        self.enableSwitcher = self.layer.enableSwitcher
        from hello.analysis import parametricFuzzyModel, rescale
        from hello.utils import loadData
        confidences = list(loadData('confidences'))
        pnl = rescale([c[1] for c in confidences], 0.0, 1.0)
        conf = [c[0] for c in confidences]
        self.fuzzier = parametricFuzzyModel(centers=conf,
                                            meanings=pnl)

    def _getsizing(self, comminfo, cash, data, isbuy):
        symbol = data._name
        instr = symbol[:symbol.find('/'):]
        tgt = symbol[symbol.find('/') + 1:]
        instrument = self.layer.Instruments[tgt][instr]
        if self.enableSwitcher:

            if self.layer.state:
            # if instrument.prev_perc_pnl > 0.0:
                print('({}) Нормальный режим'.format(self.layer.env.current))
                multiplier = 1.0
            else:
                print('({}) Режим экономии'.format(self.layer.env.current))
                multiplier = 0.01
        else:
            multiplier = 1.0
        position = self.broker.getposition(data)
        value = self.broker.getvalue()
        if not position:
            size = value / data.close[0] / self.count * multiplier
        else:
            size = position.size
        target_size = size * data.close[0]
        print('Target size: {}, Size: {}'.format(target_size, size))
        return size