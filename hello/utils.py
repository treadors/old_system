import os
import time
from preferences import *
from exceptions import ExchangeAPIError
import pickle as pick
import copy

try:
    os.mkdir(os.getcwd() + dataPath)
except OSError:
    pass
def loadData(name):
    Name = name + '.dat'
    olddir = os.getcwd()
    dir = os.getcwd() + dataPath
    os.chdir(dir)
    with open(Name, 'rb') as f:
        result = pick.load(f)
        f.close()
    os.chdir(olddir)
    return result
def saveData(object, name):
    olddir = os.getcwd()
    dir = os.getcwd() + dataPath
    os.chdir(dir)
    name += '.dat'
    with open(name, 'wb') as f:
        try:
            pick.dump(object, f)
        except TypeError as e:
            print(str(e))
            obj = copy.deepcopy(object)
            pick.dump(obj, f)
        f.close()
    os.chdir(olddir)
def Try(func, *args):
    result = None
    exception = None
    try:
        if not args:
            result = func()
        else:
            result = func(*args)
    except Exception as e:
        print('An error occured when trying to get market data from exchange. Reason:')
        print(e)
        for i in range(reconnect_count):
            try:
                print('Trying to reconnect ({})... '.format(i))
                if not args:
                    result = func()
                else:
                    result = func(args)
                break
            except Exception as e2:
                exception = e2
                print(e2)
                time.sleep(reconnect_timeout)
    finally:
        if not result:
            raise ExchangeAPIError(exception)
        else:
            return result

def TryKW(func, **kwargs):
    result = None
    exception = None
    try:
        if not kwargs:
            result = func()
        else:
            result = func(**kwargs)
    except Exception as e:
        print('An error occured when trying to get market data from exchange. Reason:')
        print(e)
        for i in range(reconnect_count):
            try:
                print('Trying to reconnect ({})... '.format(str(i)))
                if not kwargs:
                    result = func()
                else:
                    result = func(kwargs)
                break
            except Exception as e2:
                exception = e2
                time.sleep(reconnect_timeout)
    finally:
        if not result:
            raise ExchangeAPIError(exception)
        else:
            return result
