import os
import matplotlib.pyplot as plt
import hello.analysis as anl
import random
#Адрес для графиков
drawPath=r'\images\\'[:-1]
def resize(imageName, percent):
    image=Image.open(imageName)
    w, h = image.size
    imagePost = image.resize((int(percent * w), int(percent * h)), Image.ANTIALIAS)
    os.remove(imageName)
    imagePost.save(imageName)
def breakUpToGraph(currencyPair,prices, start, end, window, step, division):
    olddir = os.getcwd()
    dir = os.getcwd() + drawPath + currencyPair
    try:
        os.mkdir(dir)
    except OSError:
        pass
    totalToGraph = []
    starttime = start
    num=1
    while starttime < end-window:
        pricesToGraph = anl.getPeriod(prices,starttime,starttime+window,division)
        EMA1 = anl.EMA(9,pricesToGraph)
        EMA2 = anl.EMA(21,pricesToGraph)
        #MACD = anl.MACD(6,EMA1,EMA2)
        #EMASub = anl.EMASub(EMA1,EMA2)
        STOH = anl.STOH(10,pricesToGraph,3)
        ORIGINAL = []
        bias = pricesToGraph[0].last
        for item in pricesToGraph:
            ORIGINAL.append(item.last-bias)
        EMA = []
        for item in EMA1:
            EMA.append(item - bias)
        fig = plt.figure(1)
        original = plt.plot(ORIGINAL)
        plt.setp(original, color='k', linewidth=1.0)
        #ema1 = plt.plot(EMA)
        #plt.setp(ema1, color='m', linewidth=1.0)
        #ema2 = plt.plot(EMA2)
        #sub = fig.add_subplot(1,1,1)
        #plt.setp(ema2, color='b', linewidth=1.0)
        #macd = plt.plot(MACD)
        #plt.setp(macd, color='c', linewidth=1.0)
        #emasub = plt.plot(EMASub)
        #plt.setp(emasub, color = 'g', linewidth=1.0)
        stoh = plt.plot(STOH)
        plt.setp(stoh, color='b', linewidth=1.0)
        #fig.subplots_adjust(left=0.0, right=1.0, top=1.0, bottom=0.0)
        dir = os.getcwd() + drawPath + currencyPair + '\\\\'[:-1] + 'breakUps'
        print(dir)
        try:
            os.makedirs(dir)
        except OSError:
            pass
        os.chdir(dir)
        name = str(num) + currencyPair + '_' + '.png'
        plt.savefig(name)
        plt.close()
        num+=1
        starttime += step
        os.chdir(olddir)
        # resize(name,0.25)
def getGr(price, signal, signalIdeal, balances, pair):
    perLen=len(signal)
    fig = plt.figure(1)
    fig.suptitle('Как торгует нейросеть', fontsize=14, fontweight='bold')
    t = plt.plot(price)
    #p = plt.plot(approximation)
    if signal[0] == 0:
        plt.plot(0, price[0], 'ro')
    if signal[0] == 1:
        plt.plot(0, price[0], 'go')
    for i in range(perLen - 1):# сигналы купли-продажи (кроме стоп-лоссов)
        if signal[i] == 2:
            plt.plot(i, price[i], 'yo')
        if signal[i + 1] == 1 and signal[i] != 1:
            plt.plot(i + 1, price[i + 1], 'go')
        if signal[i + 1] == 0 and signal[i] != 0:
            plt.plot(i + 1, price[i + 1], 'ro')
    fig2 = plt.figure(2)
    fig2.suptitle('Так было бы идеально', fontsize=14, fontweight='bold')
    t2 = plt.plot(price)
    if signalIdeal[0] == 0:
        plt.plot(0, price[0], 'ro')
    if signalIdeal[0] == 1:
        plt.plot(0, price[0], 'yo')
    for i in range(perLen - 1):# сигналы купли-продажи (кроме стоп-лоссов)
        if signalIdeal[i + 1] == 1 and signalIdeal[i] != 1:
            plt.plot(i + 1, price[i + 1], 'go')
        if signalIdeal[i + 1] == 0 and signalIdeal[i] != 0:
            plt.plot(i + 1, price[i + 1], 'ro')
    fig3 = plt.figure(3)
    fig3.suptitle('Пошурудили инструментом ' + pair, fontsize=14, fontweight='bold')
    t3 = plt.plot(balances)
    plt.show()
def visualiseSignals(price, signal, isBinary=True):
    perLen = len(signal)
    fig = plt.figure(1)
    fig.add_subplot(2,1,1)
    t = plt.plot(price)
    if isBinary:
        if signal[0] == 0:
            plt.plot(0, price[0], 'ro')
        if signal[0] == 1:
            plt.plot(0, price[0], 'go')
        for i in range(perLen - 1):  # сигналы купли-продажи (кроме стоп-лоссов)
            if signal[i + 1] == 1 and signal[i] != 1:
                plt.plot(i + 1, price[i + 1], 'go')
            if signal[i + 1] == 0 and signal[i] != 0:
                plt.plot(i + 1, price[i + 1], 'ro')
    if not isBinary:
        if signal[0] == 0:
            plt.plot(0, price[0], 'ro')
        if signal[0] == 1:
            plt.plot(0, price[0], 'yo')
        if signal[0] == 2:
            plt.plot(0, price[0], 'go')
        for i in range(perLen - 1):  # сигналы купли-продажи (кроме стоп-лоссов)
            if signal[i + 1] == 1 and signal[i] != 1:
                plt.plot(i + 1, price[i + 1], 'yo')
            if signal[i + 1] == 0 and signal[i] != 0:
                plt.plot(i + 1, price[i + 1], 'ro')
            if signal[i + 1] == 2 and signal[i] != 2:
                plt.plot(i + 1, price[i + 1], 'go')
    plt.show()
def visualizeMultigraphSignals(price, signal, other1=None, other2=None, bottom=[]):
    perLen = len(signal)
    fig = plt.figure(1)
    fig.add_subplot(2,1,1)
    t = plt.plot(price)
    plt.setp(t, color='b', linewidth=1.6)
    if signal[0] == 0:
        plt.plot(0, price[0], 'ro')
    if signal[0] == 1:
        plt.plot(0, price[0], 'go')
    for i in range(perLen - 1):  # сигналы купли-продажи (кроме стоп-лоссов)
        if signal[i + 1] == 1 and signal[i] != 1:
            plt.plot(i + 1, price[i + 1], 'go')
        if signal[i + 1] == 0 and signal[i] != 0:
            plt.plot(i + 1, price[i + 1], 'ro')
    if other1 != None:
        it = plt.plot(other1)
        plt.setp(it, color = 'r', linewidth=1.0)
    if other2 != None:
        it2 = plt.plot(other2)
        plt.setp(it2, color='g', linewidth=1.0)
    if bottom != []:
        fig.add_subplot(2,1,2)
        it = plt.plot(bottom)
        if signal[0] == 0:
            plt.plot(0, bottom[0], 'ro')
        if signal[0] == 1:
            plt.plot(0, bottom[0], 'go')
        for i in range(perLen - 1):  # сигналы купли-продажи (кроме стоп-лоссов)
            if signal[i + 1] == 1 and signal[i] != 1:
                plt.plot(i + 1, bottom[i + 1], 'go')
            if signal[i + 1] == 0 and signal[i] != 0:
                plt.plot(i + 1, bottom[i + 1], 'ro')
    plt.show()