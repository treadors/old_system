from preferences import *
from hello.environment import Environment, Portfolio
import hello.analysis as anl
import numpy as np
#Тестовый режим торговли требует ручного задания Layer.TradableCurrencies, Portfolio.Balances!
class Instrument():

    closed = False
    enableStopLosses = True
    enableTakeProfits = True
    enableMoneyManagerSaving = True
    savePredictions = True
    silent = True
    confidence_level_buy = 0.5
    confidence_level_sell = 0.5
    minProfit = 0.045
    trailingStop = False
    hi = None
    lo = 0.02
    dynamic_hi = True
    dynamic_lo = True
    buyDecisions = []
    buyExpOut = []

    def __init__(self, target, instrument, env, port, layer, test=False):
        self.testing = test
        self.target = target
        self.instrument = instrument
        self.layer = layer
        self.pair = instrument + '/' + target
        self.env = env
        self.port = port
        self.historyLen = layer['history'] // 2
        self.manager = layer['moneyManager']
        self.portfolioShareGlobal = layer['portfolioShare']
        self.predictions = None

        self.signal = 0
        self.state = 0
        self.previousPrice = 0.0
        self.previousAmount = 0.0
        self.currentOnesCount = 0
        self.currentZerosCount = 0
        self.install()

    def act(self, expertOut, signals, share=None):
        if self.succesfullyClosed:
            return None
        self.getPrices()
        if isinstance(self.History, list):
            self.setupHistory()
        else:
            self.moveHistory()
        if share == None:
            self.share = self.portfolioShare
        else:
            self.share = share
        signal = self.simplifySignal(signals)
        self.rectify(signal)
        self.decisions = signals
        self.exp = expertOut
        if self.buyDecisions == []:
            self.buyDecisions = self.decisions
        if self.buyExpOut == []:
            self.buyExpOut = self.exp
        if self.signal == 0:
            if signal == 0:
                return self.getSuccessMessage(0.0, signal)
            if signal == 1:
                #if self.currentOnesCount < 4:
                    #print(self.pair + ' ignore buy ' + str(self.currentOnesCount))
                    #return self.getSuccessMessage(0.0, signal)
                if self.exp['prediction'][1] < self.confidence_level_buy:
                    return self.getSuccessMessage(0.0, signal)
                if self.enableStopLosses or self.enableTakeProfits:
                    self.defineStopLoss()
                return self.makeBuyDecision()
            else:
                return self.getSuccessMessage(0.0, signal)
        if self.signal == 1:
            if signal == 1:
                if self.enableStopLosses:
                    return self.checkStopLoss()
                else:
                    return self.getSuccessMessage(0.0, signal)
            if signal == 0:
                #if self.currentZerosCount < 4:
                    #print(self.pair + ' ignore sell ' + str(self.currentZerosCount))
                    #return self.getSuccessMessage(0.0, signal)
                if self.exp['prediction'][0] < self.confidence_level_sell:
                    return self.getSuccessMessage(0.0, signal)
                if self.enableStopLosses:
                    return self.checkStopLoss()
                else:
                    return self.makeSellDecision()
            else:
                return self.getSuccessMessage(0.0, signal)

    def interact(self):
        if self.succesfullyClosed:
            return None
        self.getPrices()
        pass

    def makeBuyDecision(self):
        self.target_vol_pre = self.port.totalTo(self.target)
        if self.enableStopLosses:
            self.target_vol_pre *= self.share * self.layer['portfolioShare'] * self.multiplier
        else:
            self.target_vol_pre *= self.share * self.layer['portfolioShare']
        amount = self.target_vol_pre / self.buyPrice
        #Здесь проверка стоплосса, тейкпрофита
        if self.port.Balances[self.target]['free'] <= 0.0:
            if not self.silent:
                print('Не осталось ' + self.target + ' для покупки ' + self.instrument + '!')
        if self.target_vol_pre >= self.port.Balances[self.target]['free']:
            if not self.silent:
                print('Недостаточно ' + self.target + ' для покупки ' + self.instrument + ' на сумму ' + str(
                    self.target_vol_pre) + self.target + '!')
            self.target_vol_pre = self.port.Balances[self.target]['free']
            amount = self.target_vol_pre / self.buyPrice
        self.previousAmount = amount * (1.0 - makerFee)
        return self.open_deal(amount)

    def makeSellDecision(self):
        self.dynamic = self.sellPrice/self.previousPrice - 1.0
        dynoatmax = self.sellPrice/self.maximumPrice - 1.0
        #Здесь проверка стоплосса, тейкпрофита
        if self.enableStopLosses and self.enableTakeProfits:
            if self.sellPrice >= self.stopLoss and self.sellPrice <= self.takeProfit:
                if not self.silent:
                    print('Игнорируем попытку продажи ' + self.instrument + ' с прибыльностью' + str(self.dynamic) + '. At max: ' + str(dynoatmax))
                return self.getSuccessMessage(0.0, self.signal)
        elif self.enableStopLosses:
            if self.sellPrice >= self.stopLoss and self.dynamic <= 0.0:
                if not self.silent:
                    print('Игнорируем попытку продажи ' + self.instrument + ' с прибыльностью' + str(
                        self.dynamic) + '. At max: ' + str(dynoatmax))
                return self.getSuccessMessage(0.0, self.signal)
        elif self.enableTakeProfits:
            if self.sellPrice <= self.takeProfit and self.dynamic >= 0.0:
                if not self.silent:
                    print('Игнорируем попытку продажи ' + self.instrument + ' с прибыльностью' + str(
                        self.dynamic) + '. At max: ' + str(dynoatmax))
                return self.getSuccessMessage(0.0, self.signal)
        # self.signal = 0
        return self.close_deal()

    def open_deal(self, amount):
        if not self.testing:
            self.interact()
        else:
            self.port.changeBalance(self.target, -self.target_vol_pre)
            self.port.changeBalance(self.instrument, self.previousAmount)
            if not self.silent:
                print(self.pair + '. Установлены ограничения - Hi: ' + str(self.hi) + ', Lo: ' + str(
                    self.lo) + ', доля портфеля: ' + str(self.share))
                print('Пoкупаем ' + str(amount) + ' ' + self.instrument + ' за ' + str(
                    amount * self.buyPrice) + ' ' + self.target + ' по курсу ' + str(self.buyPrice))
            self.buyDecisions = self.decisions
            self.buyExpOut = self.exp
            self.buyPrediction = self.exp['prediction']
            self.previousPrice = self.buyPrice
            self.maximumPrice = self.buyPrice
            self.buyTime = self.currentTime
            self.inDeal = True
            self.signal = 1
            factic_price = self.buyPrice * (1 + slippage)
            self.trade_info = {'symbol': self.pair, 'ideal_open_price': self.buyPrice,
                               'factic_open_price': factic_price, 'open_td': self.buyTime,
                               'open_conf': self.buyPrediction[1]}
            return self.getSuccessMessage(0.0, presignal=self.signal)

    def close_deal(self):
        amount = self.previousAmount
        if not self.testing:
            self.interact()
        else:
            self.dynamic = self.sellPrice / self.previousPrice - 1.0
            self.target_vol_post = amount * self.sellPrice * (1 - makerFee)
            absolute_pnl = self.target_vol_post - self.target_vol_pre
            self.port.changeBalance(self.target, self.target_vol_post)
            self.port.changeBalance(self.instrument, -amount)
            if not self.silent:
                print('Продаем ' + str(amount) + ' ' + self.instrument + '. ' + str(
                    amount * (self.sellPrice - self.previousPrice)) + ' ' + self.target + '. Прибыльность: ' + str(
                    self.sellPrice / self.previousPrice - 1.0))
            self.sellTime = self.currentTime
            self.dealDuration = self.sellTime - self.buyTime
            self.layer['globalDealHistory'].append(self.dynamic)
            self.inDeal = False
            self.sellPrediction = self.exp['prediction']
            self.signal = 0
            self.layer['currentSuccess'] += self.dynamic
            factic_price = self.sellPrice * (1+slippage)
            factic_perc_pnl = factic_price / self.trade_info['factic_open_price'] - 1.0
            self.prev_perc_pnl = factic_perc_pnl
            self.trade_info.update({'ideal_close_price': self.sellPrice, 'factic_close_price': factic_price,
                                    'close_td': self.sellTime, 'close_conf': self.sellPrediction[0],
                                    'ideal_abs_pnl': absolute_pnl, 'factic_abs_pnl': absolute_pnl,
                                    'ideal_perc_pnl': self.dynamic,
                                    'factic_perc_pnl': factic_perc_pnl})
            if self.enableMoneyManagerSaving:
                self.manager.add_trade(self.trade_info)
            return self.getSuccessMessage(self.dynamic, self.signal)

    def defineStopLoss(self):
        hilos = anl.getHiLo(self.History)
        self.hi = hilos[0] if self.dynamic_hi else self.hi
        self.lo = hilos[1] if self.dynamic_lo else self.lo
        self.multiplier = self.hi/abs(self.lo)
        if self.hi < self.minProfit:
            multiplier = self.minProfit/self.hi
            self.hi *= multiplier
            if self.dynamic_lo:
                self.lo *= multiplier
        if abs(self.lo) < self.minProfit and self.dynamic_lo:
            multiplier = self.minProfit/abs(self.lo)
            self.hi *= multiplier
            self.lo *= multiplier
        #self.hi = anl.stdev(self.History)
        #self.lo = -self.hi*0.8
        self.takeProfit = self.buyPrice*(1+self.hi)
        self.stopLoss = self.buyPrice*(1+self.lo)

    def checkStopLoss(self):
        self.dynamic = self.sellPrice/self.previousPrice-1
        if self.sellPrice > self.maximumPrice:
            premax = self.maximumPrice
            self.maximumPrice = self.sellPrice
            if self.enableTakeProfits:
                if self.trailingStop:
                    percentToTakeProfit = self.dynamic/self.hi
                    if percentToTakeProfit > 1.0:
                        self.stopLoss = self.maximumPrice*(0.999)
                    else:
                        self.stopLoss = self.maximumPrice*(1+self.lo)#*percentToTakeProfit)
                        if not self.silent:
                            print(self.pair + 'Предыдущий максимум ' + str(premax) + ' пробит ценой ' + str(
                            self.maximumPrice) + '. Новый стоплосс ' +
                            str(self.stopLoss) + ' . About start price: ' + str(
                            self.stopLoss / self.previousPrice - 1.0))
                return self.getSuccessMessage(0.0, self.signal)
            else:
                if self.trailingStop:
                    self.stopLoss = self.maximumPrice*(1+self.lo)
                    if not self.silent:
                        print(self.pair + 'Предыдущий максимум ' + str(premax) + ' пробит ценой ' + str(
                            self.maximumPrice) + '. Новый стоплосс ' +
                              str(self.stopLoss) + ' . About start price: ' + str(
                            self.stopLoss / self.previousPrice - 1.0))
                return self.getSuccessMessage(0.0, self.signal)
        else:
            message = self.makeSellDecision()
            return message

    def applyPrediction(self, interact=False, use_saved_predictions=False, Type='tcn'):
        predictor = self.layer['predictor']
        if interact == False:
            if not use_saved_predictions:
                message = self.layer['predict'](self.pair, Type=Type)
            else:
                message = self.predictions[self.env.current]
            output = [message['prediction'],]
            states = [message['signal'],]
            #message = self.layer.moneyManager.executePrediction(message)
            callback = self.act(message, states)
        else:
            callback = self.interact()
        if self.savePredictions:
            self.predictions[self.env.current] = self.exp

    def refresh(self, source):
        self.portfolioShare = source.portfolioShare
        #self.portfolioShareGlobal = source.portfolioShareGlobal
        self.corellation = source.corellation
        self.deviation = source.deviation
        self.afford = source.afford
        self.minDuration = source.minDuration
        # self.historyLen = int(self.layer['history'] / 2)
        self.historyLen = 144

    def getPrices(self):
        current = self.source.Current
        self.last = float(current.last)
        self.hb = float(current.highestBid)
        self.la = float(current.lowestAsk)
        self.buyPrice = self.la * (1+compensation)
        self.sellPrice = self.hb * (1-compensation)
        self.currentTime = current.timedate

    def moveHistory(self):
        self.History = np.append(self.History, [self.last, ])
        self.History = np.delete(self.History, 0)
        if not self.testing:
            print('Обновление истории ' + self.pair)

    def install(self):
        self.source = self.env.currencyPairs[self.target][self.pair]
        self.hist = self.source.GlobalHistory
        self.hist = self.env.Data[self.pair]
        length = self.hist.shape[0]
        # self.hist = self.layer.env.Data[self.pair]
        self.History = []
            # self.History = anl.toList(self.hist[len(self.hist) - self.historyLen:])
        self.signal = 0
        self.signalpre = 0
        self.onOrder = False #False - нет ордера, #True - ордер исполняется
        self.succesfullyClosed = False
        self.inDeal = False
        self.prev_perc_pnl = -0.01

    def setupHistory(self):
        length = self.hist.shape[0]
        if self.testing:
            self.History = self.hist.loc[length - self.env.current - self.historyLen:
                                         length - self.env.current][['Close']].values
        else:
            self.History = self.hist.loc[length - self.historyLen:][['Close']].values



    def simplifySignal(self, signal):
        for sig in signal:
            if sig == 1:
                return 1
        return 0
    def rectify(self, signal):
        if self.signalpre == 0 and signal == 1:
            self.currentOnesCount = 0
        if self.signalpre == 1 and signal == 0:
            self.currentZerosCount = 0
        if signal == 1:
            self.currentOnesCount += 1
        else:
            self.currentZerosCount += 1
    def getSuccessMessage(self, profit, presignal):
        message = {}
        succ = []
        message['target'] = self.target
        message['instrument'] = self.instrument
        for i in range(0, len(self.buyDecisions)):
            if self.buyDecisions[i] == 1:
                succ.append(profit)
            else:
                succ.append(0.0)
        message['success'] = np.array(succ, dtype = np.float32)
        message['prediction'] = self.buyExpOut
        self.signalpre = presignal
        #self.layer.signals[self.pair].append(self.signal)
        #self.layer.ticks[self.pair].append(self.last)
        return message