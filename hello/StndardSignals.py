import math
#import sklearn as skl
#import pybrain
import random
import numpy as np
from scipy import stats

class StandardSignals:
    def __init__(self, Y1, smoothy, minProfit=None):# maxOrderLength1):
        self.Y = Y1
        self.length = len(Y1)
        self.smoothy = smoothy  #коэффициент РАЗМЫТИЯ
        self.maxOrderLength = smoothy
        if minProfit == None:
            self.minProfit = 0.001088*smoothy + 0.02#0.000544*smoothy + 0.02#0.00555
        else:
            self.minProfit = minProfit
        #print(self.minProfit)
    def MakeHoldSignals(self, isBinary = True):
        signals = self.getRoll()
        signals_post = []
        if isBinary:
            for i in range(0, len(signals)):
                if signals[i] == -1:
                    signals[i] = 0
        else:
            for i in range(len(signals)):
                if signals[i] == -1:
                    signals[i] = 0
                elif signals[i] == 0:
                    signals[i] = 1
                elif signals[i] == 1:
                    signals[i] = 2
        return signals
    def getRoll(self):
        rolls = []
        #Ysmall = self.Y[:len(self.Y)]
        for i in range(1, self.smoothy+1):
            rolls.append([])
            minor = self.length%i
            end = self.length // i
            for j in range(0, end, 1):
                if minor == 0:
                    if j == end - 1:
                        if self.Y[j*i+i-1] >= self.Y[j*i]:
                            for k in range(i):
                                rolls[-1].append(1)
                        else:
                            for k in range(i):
                                rolls[-1].append(-1)
                    else:
                        if self.Y[j*i+i] >= self.Y[j*i]:
                            for k in range(i):
                                rolls[-1].append(1)
                        else:
                            for k in range(i):
                                rolls[-1].append(-1)
                else:
                    if self.Y[j*i+i] >= self.Y[j*i]:
                        for k in range(i):
                            rolls[-1].append(1)
                    else:
                        for k in range(i):
                            rolls[-1].append(-1)
            last = rolls[-1][-1]
            for j in range(minor):
                rolls[-1].append(last)
        modes = stats.mode(np.array(rolls))
        modes = modes[0][0].tolist()
        changes = []
        for i in range(len(modes) - 1):
            if modes[i] != modes[i+1]:
                changes.append(i+1)
        for i in range(len(changes) - 1):
            if changes[i+1]-changes[i] < self.smoothy:
                for j in range(changes[i], changes[i+1]):
                    modes[j] = modes[changes[i]-1]

        changes = []
        for i in range(len(modes) - 1):
            if modes[i] != modes[i+1]:
                changes.append(i+1)
        for i in range(len(changes) - 1):
            if abs(modes[changes[i+1]]/modes[changes[i]] - 1) < self.minProfit:
                for j in range(changes[i], changes[i+1]):
                    modes[j] = 1
        return modes
    def getAveragePeriod(self):
        row = self.MakeHoldSignals()
        previous = 0
        periods = []
        period = 0
        for item in row:
            if item == previous:
                period+=1
            else:
                previous = item
                periods.append(period)
                period = 0
        period = 0
        n = 0
        for per in periods:
            if per != 0.0:
                period += per
                n += 1
        return period/n
    def getHiLo(self):
        HIs = []
        LOs = []
        row = self.MakeHoldSignals()
        SellPrice = self.Y[0]
        BuyPrice = self.Y[0]
        for i in range(0, len(row)-1):
            if row[i] == 0 and row[i+1] == 1:
                BuyPrice = self.Y[i+1]
                HIs.append(SellPrice/BuyPrice-1.0)
            if row[i] == 1 and row[i+1] == 0:
                SellPrice = self.Y[i+1]
                LOs.append(BuyPrice/SellPrice-1.0)
        if HIs == [] or LOs == []:
            raise Exception("Непоказательный период статистики!")
        HIs.sort()
        LOs.sort()
        hi = HIs[len(HIs)//2]
        lo = LOs[len(LOs)//2]
        return (hi, lo)
        #return (sum(HIs)/len(HIs), sum(LOs)/len(LOs))

def getProfit(row, signals, startbalance=1.0):
    balances = [startbalance,]
    deals = []
    openprice = row[0]
    closeprice = row[0]
    for i in range(1, len(signals)):
        if signals[i-1] == 0 and signals[i] == 1:
            openprice = row[i]
        if signals[i-1] == 1 and signals[i] == 0:
            closeprice = row[i]
            deals.append(closeprice/openprice-1.0)
            newbalance = balances[-1]*(1+deals[-1])
            balances.append(newbalance)
    return (balances, deals)
