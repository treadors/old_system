from hello.environment import *
from hello.layer import *
from hello.committee import *
# from hello.worker import *
from hello.instrument import *
from hello.models import *
from hello.wrapper import *
from hello.backtester import BackTester

class TradingSystem():

    def __init__(self, env, port=None, data=None):
        self.env = env
        self.port = port
        self.Layers = []
        if not data:
            if not self.env.Data:
                self.Data = Data()
                self.env.Data = self.Data
            else:
                self.Data = self.env.Data
        else:
            self.Data = data
            self.env.Data = data

    def addLayer(self, smooth, tradableCounts, Share=1.0):
        self.Layers.append(Layer(self.env, smooth, tradableCounts, Share=Share))

    def Test(self, current):
        self.test = True
        self.env.test = True
        self.current = current
        self.env.current = current

    def Untest(self):
        self.test = False
        self.env.test = False
        self.current = 0
        self.env.current = 0

    def FullLayerSetup(self, layer, test=True):
        if not self.port:
            self.port = Portfolio(self.env)
        layer.initializeTradingSystem(portfolio=self.port)
        layer.switch_test_mode(test=test)
        # layer.globalUpdate(portfolio=self.port, test=test, committee=True, internalInstruments=True)

    def BackTest(self, layer=None, cash=None, **params):
        if not layer:
            layer = self.Layers[0]
        if not cash:
            cash = 100
        self.backtester = BackTester(layer, portfolio=None, **params)
        self.backtester.prepare(startcash=cash)
        self.backtester.run()
        result = self.backtester.make_conclusions(plot=True)
        print(result['pnl'])

    def Optimize(self, layer=None, cash=None, **params):
        if not layer:
            layer = self.Layers[0]
        if not cash:
            cash = 100
        optimized = [5 + x * 5 for x in range(40)]
        Pnls = []
        for opt in optimized:
            self.backtester = BackTester(layer, historic_eff_len=opt,
                                         enableSwitcher=True)
            self.backtester.prepare(startcash=cash)
            self.backtester.run()
            result = self.backtester.make_conclusions(plot=False)
            Pnls.append(result['pnl'])
            print('Len {0}. PnL: {1}'.format(opt, result['pnl']))
        saveData((optimized, Pnls), 'confidences')
        plt.plot(optimized, Pnls)
        plt.show()