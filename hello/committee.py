import keras.optimizers
import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.layers import Dropout, Flatten, Dense, LSTM, Conv1D, Input, BatchNormalization, concatenate, GaussianNoise, \
    ZeroPadding1D, Bidirectional, MaxPool1D, Concatenate
from keras.models import Model
from keras.models import load_model
from keras.regularizers import l2
from keras.utils.np_utils import to_categorical
from hitek.tcn.tcn import TCN

import hello.analysis as anl
from hello.utils import *
from hitek.learning_stuff import auc, validateNNonData, load, cutDataLabels, cutMultiDimentionalData, \
    roc_callback

def create_lstm_tcn(data, labels, neuronsCount, batch_size, epochs, validationShare=0.15, shuffle=False, asName='', returnName=True):

    name = asName + '.h5'
    length = data[0].shape[0]
    validationLength = int(length * validationShare // batch_size * batch_size)
    splittedData = cutDataLabels(data, labels, int(length // 1.3), int(length // 1.3 + validationLength))
    dataLearn = splittedData['dataEdge']
    dataVal = splittedData['dataMiddle']
    labelsLearn = splittedData['labelsEdge'][0]
    labelsLearn = to_categorical(labelsLearn)
    numclasses = labelsLearn.shape[1]
    labelsVal = splittedData['labelsMiddle'][0]
    labelsVal = to_categorical(labelsVal, numclasses)
    if len(data) > 1:
        inputs = []
        for i in range(0, len(data)):
            inputs.append(Input(shape=(data[i].shape[1], data[i].shape[2]), name='inp' + str(i)))
        input = concatenate(inputs)
    else:
        inputs = Input(shape=(data[0].shape[1], data[0].shape[2]), name='inp' + str(0))
        input = BatchNormalization()(inputs)

    tcn = create_fit_save_one_out_tcn(data, labels, neuronsCount, batch_size,
                                      epochs, validationShare, False, '', False)
    lstm = createFitSaveOneOutConvLSTM(data, labels, neuronsCount, batch_size,
                                       epochs, validationShare, False, '', False)
    for layer in tcn.layers:
        layer.trainable = False
    for layer in lstm.layers:
        layer.trainable = False

    Inputs = tcn._feed_inputs
    first = tcn(Inputs)
    second = lstm(Inputs)
    concatenated = concatenate([first, second])
    # count = concatenated._keras_shape[1]
    all = Dense(4, activation='softmax')(concatenated)
    out = Dense(numclasses, activation='softmax', name='out_final')(all)
    res = Model(inputs=Inputs, outputs=out)
    res.compile(optimizer='rmsprop', loss={'out_final': 'binary_crossentropy'}, metrics=[auc])

    print(res.summary())
    Inputs = {}
    for i in range(0, len(data)):
        Inputs['inp' + str(i)] = dataLearn[i]
    res.fit(Inputs, {'out_final': labelsLearn},
              epochs=epochs, batch_size=batch_size, validation_data=(dataVal, labelsVal), shuffle=shuffle, verbose=2,
            callbacks=[ModelCheckpoint(name, monitor='val_auc', verbose=2, save_best_only=True, save_weights_only=False, mode='max', period=1),])
                       # roc_callback(training_data=(dataLearn, labelsLearn), validation_data=(dataVal, labelsVal))])
    if not returnName:
        return res
    else:
        del (res)
        return name


def create_fit_save_one_out_tcn(data, labels, neuronsCount, batch_size, epochs, validationShare=0.15, shuffle=False, asName='', returnName=True, inp=None):
    LreluSoft = keras.layers.advanced_activations.LeakyReLU(alpha=0.05)
    LreluHard = keras.layers.advanced_activations.LeakyReLU(alpha=0.01)
    name = asName + '.h5'
    l = 0.005
    if not inp:
        length = data[0].shape[0]
        validationLength = int(length * validationShare // batch_size * batch_size)
        splittedData = cutDataLabels(data, labels, int(length // 1.3), int(length // 1.3 + validationLength))
        dataLearn = splittedData['dataEdge']
        dataVal = splittedData['dataMiddle']
        labelsLearn = splittedData['labelsEdge'][0]
        labelsLearn = to_categorical(labelsLearn)
        numclasses = labelsLearn.shape[1]
        labelsVal = splittedData['labelsMiddle'][0]
        labelsVal = to_categorical(labelsVal, numclasses)
        if len(data) > 1:
            inputs = []
            for i in range(0, len(data)):
                inputs.append(Input(shape=(data[i].shape[1], data[i].shape[2]), name='inp' + str(i)))
            input = concatenate(inputs)
        else:
            inputs = Input(shape=(data[0].shape[1], data[0].shape[2]), name='inp' + str(0))
            input = BatchNormalization()(inputs)
        # noise = GaussianNoise(stddev=0.015)(input)
    else:
        dataLearn, dataVal, labelsLearn, labelsVal = data[0], data[1], labels[0], labels[1]
        inputs, input = inp[0], inp[1]
        numclasses = labelsLearn.shape[1]

    all = BatchNormalization()(input)
    all = TCN(nb_filters=64, return_sequences=True, name='TCN_1')(all)
    all = TCN(nb_filters=64, return_sequences=True, name='TCN_2')(all)
    all = TCN(nb_filters=64, return_sequences=False, name='TCN_3')(all)
    all = LreluSoft(all)
    out = Dense(numclasses, activation='softmax', name='out')(all)
    res = Model(inputs=inputs, outputs=out)
    res.compile(optimizer='rmsprop', loss={'out': 'binary_crossentropy'}, metrics=[auc])
    print(asName)
    print(res.summary())
    Inputs = {}
    for i in range(0, len(inputs)):
        Inputs['inp' + str(i)] = dataLearn[i]
    res.fit(Inputs, {'out': labelsLearn},
              epochs=epochs, batch_size=batch_size, validation_data=(dataVal, labelsVal), shuffle=shuffle, verbose=2,
            callbacks=[ModelCheckpoint(name, monitor='val_auc', verbose=2, save_best_only=True, save_weights_only=False, mode='max', period=1),])
                       # roc_callback(training_data=(dataLearn, labelsLearn), validation_data=(dataVal, labelsVal))])
    if not returnName:
        return res
    else:
        del (res)
        return name


def createFitSaveOneOutConvLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare=0.15, shuffle=False, asName='', returnName=True, inp=None):
    LreluSoft = keras.layers.advanced_activations.LeakyReLU(alpha=0.05)
    LreluHard = keras.layers.advanced_activations.LeakyReLU(alpha=0.01)
    name = asName + '.h5'
    l = 0.005
    if not inp:
        length = data[0].shape[0]
        validationLength = int(length * validationShare // batch_size * batch_size)
        splittedData = cutDataLabels(data, labels, int(length // 1.3), int(length // 1.3 + validationLength))
        dataLearn = splittedData['dataEdge']
        dataVal = splittedData['dataMiddle']
        labelsLearn = splittedData['labelsEdge'][0]
        labelsLearn = to_categorical(labelsLearn)
        numclasses = labelsLearn.shape[1]
        labelsVal = splittedData['labelsMiddle'][0]
        labelsVal = to_categorical(labelsVal, numclasses)
        if len(data) > 1:
            inputs = []
            for i in range(0, len(data)):
                inputs.append(Input(shape=(data[i].shape[1], data[i].shape[2]), name='inp' + str(i)))
            input = concatenate(inputs)
        else:
            inputs = Input(shape=(data[0].shape[1], data[0].shape[2]), name='inp' + str(0))
            input = BatchNormalization()(inputs)
            numclasses = labelsLearn.shape[1]
    else:
        dataLearn, dataVal, labelsLearn, labelsVal = data[0], data[1], labels[0], labels[1]
        inputs, input = inp[0], inp[1]
        numclasses = labelsLearn.shape[1]
    # noise = GaussianNoise(stddev=0.015)(input)
    # all = BatchNormalization()(input)
    all = Conv1D(filters=neuronsCount, kernel_size=3, bias_regularizer=l2(l), kernel_regularizer=l2(l), padding='same')(input)
    # all = Conv1D(filters=neuronsCount, kernel_size=3, bias_regularizer=l2(l), kernel_regularizer=l2(l), padding='same')(all)
    # all = LreluSoft(all)
    # all = Dropout(0.15)(all)
    # all = MaxPool1D(padding='same')(all)
    # all = Conv1D(filters=neuronsCount, kernel_size=3, bias_regularizer=l2(l), kernel_regularizer=l2(l), padding='same')(all)
    # all = Conv1D(filters=neuronsCount, kernel_size=3, bias_regularizer=l2(l), kernel_regularizer=l2(l), padding='same')(all)
    all = LreluSoft(all)
    # all = Dropout(0.15)(all)
    all = LSTM(units=neuronsCount, dropout=0.15,
               activity_regularizer=l2(l), bias_regularizer=l2(l), recurrent_regularizer=l2(l))(all)
    # all = LSTM(neuronsCount, recurrent_dropout=0.15, dropout=0.15,
    #            activity_regularizer=l2(l), bias_regularizer=l2(l), recurrent_regularizer=l2(l),
    #            bias_initializer='glorot_uniform', recurrent_initializer='glorot_uniform')(all)
    # all = LreluSoft(all)
    # all = Dense(neuronsCount, activation='tanh')(all)
    out = Dense(numclasses, activation='softmax', name='out')(all)
    res = Model(inputs=inputs, outputs=out)
    res.compile(optimizer='rmsprop', loss={'out': 'binary_crossentropy'}, metrics=[auc])
    print(asName)
    print(res.summary())
    Inputs = {}
    for i in range(0, len(inputs)):
        Inputs['inp' + str(i)] = dataLearn[i]
    res.fit(Inputs, {'out': labelsLearn},
              epochs=epochs, batch_size=batch_size, validation_data=(dataVal, labelsVal), shuffle=shuffle, verbose=2,
            callbacks=[ModelCheckpoint(name, monitor='val_auc', verbose=2, save_best_only=True, save_weights_only=False, mode='max', period=1),])
                       # roc_callback(training_data=(dataLearn, labelsLearn), validation_data=(dataVal, labelsVal))])
    if not returnName:
        return res
    else:
        del (res)
        return name
def createEmbeddedLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare=0.05, shuffle=False, asName='', returnName=True, regression=False):
    LreluSoft = keras.layers.advanced_activations.LeakyReLU(alpha=0.05)
    LreluHard = keras.layers.advanced_activations.LeakyReLU(alpha=0.01)
    name = asName + '.h5'
    l = 0.005
    length = data[0].shape[0]
    validationLength = int(length * validationShare // batch_size * batch_size)
    splittedData = cutDataLabels(data, labels, int(length // 1.3), int(length // 1.3 + validationLength))
    dataLearn = splittedData['dataEdge']
    dataVal = splittedData['dataMiddle']
    labelsLearn1 = splittedData['labelsEdge'][0]
    labelsVal1 = splittedData['labelsMiddle'][0]
    if not regression:
        labelsLearn1 = to_categorical(labelsLearn1)
        labelsVal1 = to_categorical(labelsVal1)
        numclasses1 = labelsLearn1.shape[1]
        activation = 'softmax'
        metric = 'crosscategorical_accuracy'
    else:
        numclasses1 = 1
        activation = 'tanh'
    labelsLearn2 = splittedData['labelsEdge'][0]
    labelsLearn2 = to_categorical(labelsLearn2)
    labelsVal2 = splittedData['labelsMiddle'][0]
    labelsVal2 = to_categorical(labelsVal2)
    numclasses2 = labelsLearn2.shape[1]
    if len(data) > 1:
        inputs = []
        for i in range(0, len(data)):
            inputs.append(Input(shape=(data[i].shape[1], data[i].shape[2]), name='inp' + str(i)))
        input = concatenate(inputs)
    else:
        inputs = Input(shape=(data[0].shape[1], data[0].shape[2]), name='inp' + str(0))
        input = BatchNormalization()(inputs)
    allInp = BatchNormalization()(input)

    all = LSTM(neuronsCount, recurrent_dropout=0.2, dropout=0.2,
               activity_regularizer=l2(l), bias_regularizer=l2(l), recurrent_regularizer=l2(l),
               bias_initializer='glorot_uniform', recurrent_initializer='glorot_uniform')(allInp)
    all = LreluHard(all)
    out = Dense(numclasses1, activation=activation, name='out')(all)
    model = Model(inputs=inputs, outputs=out)
    model.compile(optimizer='rmsprop', loss={'out': 'mse'}, metrics=['accuracy'])
    print(asName)
    print(model.summary())
    Inputs = {}
    for i in range(0, len(data)):
        Inputs['inp' + str(i)] = dataLearn[i]
    model.fit(Inputs, {'out': labelsLearn1},
            epochs=epochs, batch_size=batch_size, validation_data=(dataVal, labelsVal1), shuffle=shuffle, verbose=2,
            callbacks=[ModelCheckpoint(name, monitor='accuracy', verbose=2, save_best_only=True,
                                       save_weights_only=False, mode='max', period=1)])
    for layer in model.layers:
        layer.trainable = False
    Inputs = model._feed_inputs
    mod = model(Inputs)
    hidden = Dense(numclasses2, activation=activation)(mod)
    hidden = Dropout(0.1)(hidden)
    hidden = Dense(numclasses2, activation=activation)(hidden)
    hidden = Dropout(0.1)(hidden)
    out = Dense(numclasses2, activation='softmax', name='outFinal')(hidden)
    res = Model(inputs=Inputs, outputs=out)
    res.compile(optimizer='rmsprop', loss={'outFinal': 'categorical_crossentropy'}, metrics=['categorical_accuracy'])
    print(asName)
    print(res.summary())
    Inputs = {}
    for i in range(0, len(data)):
        Inputs['inp' + str(i)] = dataLearn[i]
    res.fit(Inputs, {'outFinal': labelsLearn2},
            epochs=3, batch_size=1, validation_data=(dataVal, labelsVal2), shuffle=False, verbose=2,
            callbacks=[ModelCheckpoint(name, monitor='categorical_accuracy', verbose=2, save_best_only=True,
                                       save_weights_only=False,
                                       mode='max', period=1)])
    if not returnName:
        return res
    else:
        del (res)
        return name
def createFitSaveMultipleOutLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare=0.1, shuffle=False, asName='', returnName=True):
    LreluSoft = keras.layers.advanced_activations.LeakyReLU(alpha=0.05)
    LreluHard = keras.layers.advanced_activations.LeakyReLU(alpha=0.01)
    Name = asName + '.h5'
    l = 0.005
    for i in range(0, len(labels)):
        Type = labels[i].dtype.type
        if labels[i].dtype.type is np.int32:
            labels[i] = to_categorical(labels[i])
    length = data[0].shape[0]
    validationLength = int(length * validationShare // batch_size * batch_size)
    splittedData = cutDataLabels(data, labels, int(length // 1.3), int(length // 1.3 + validationLength))
    dataLearn = splittedData['dataEdge']
    dataVal = splittedData['dataMiddle']
    labelsLearn = splittedData['labelsEdge']
    labelsVal = splittedData['labelsMiddle']
    if len(data) > 1:
        inputs = []
        for i in range(0, len(data)):
            inputs.append(Input(shape=(data[i].shape[1], data[i].shape[2]), name='inp_' + asName + str(i)))
        input = concatenate(inputs)
    else:
        inputs = Input(shape=(data[0].shape[1], data[0].shape[2]), name='inp' + str(0))
        input = BatchNormalization()(inputs)
    allInp = BatchNormalization()(input)
    all = LSTM(neuronsCount, recurrent_dropout=0.2, dropout=0.2,
               activity_regularizer=l2(l), bias_regularizer=l2(l), recurrent_regularizer=l2(l),
               bias_initializer='glorot_uniform', recurrent_initializer='glorot_uniform')(allInp)
    all = LreluHard(all)
    outs = []
    losses = {}
    for i in range(0, len(labels)):
        numclasses = labels[i].shape[1]
        name = 'out_' + asName + str(i)
        if numclasses > 1:
            activation = 'softmax'
            losses[name] = 'categorical_crossentropy'
        else:
            activation = 'sigmoid'
            losses[name] = 'mse'
        out = Dense(numclasses, activation=activation, name=name)(all)
        outs.append(out)
    res = Model(inputs=inputs, outputs=outs)
    res.compile(optimizer='rmsprop', loss=losses, metrics=['accuracy'])
    print(asName)
    print(res.summary())
    Inputs = {}
    Outputs = {}
    for i in range(0, len(data)):
        Inputs['inp_' + asName + str(i)] = dataLearn[i]
    for i in range(0, len(labels)):
        name = 'out_' + asName + str(i)
        Outputs[name] = labelsLearn[i]
    res.fit(Inputs, Outputs,
            epochs=epochs, batch_size=batch_size, validation_data=(dataVal, labelsVal), shuffle=shuffle, verbose=2,
            callbacks=[ModelCheckpoint(Name, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False,
                                       mode='max', period=1)])
    if not returnName:
        return res
    else:
        del (res)
        return Name
def createMultiInOutLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare=0.1, shuffle=False, asName=''):
    Name = asName + '.h5'
    length = data[0][0].shape[0]
    validationLength = length // validationShare // batch_size * batch_size
    splittedData = cutMultiDimentionalData(data, labels, int(length // 1.3), int(length // 1.3 + validationLength))
    dataLearn = splittedData['dataEdge']
    dataVal = splittedData['dataMiddle']
    labelsLearn = splittedData['labelsEdge']
    labelsVal = splittedData['labelsMiddle']
    firstLevelModels = []
    for i in range(0, len(data)):
        name = 'firstLevel' + str(i)
        returns = createFitSaveMultipleOutLSTM(data[i], labels[0][i], neuronsCount, batch_size, epochs, validationShare, shuffle, asName=name, returnName=False)
        model = returns[0]
        for layer in model.layers:
            layer.trainable = False
        outCount = len(labels[0][i])
        layer = model.layers[-1]
        #for i in range(0, outCount):
            #print(layer.get_output_at(0).get_shape().as_list())
        firstLevelModels.append(model)
    concatenated = concatenate(returns[1])
    hidden = Dense(20, activation='softmax')(concatenated)
    '''secondLevelOuts = []
    losses = {}
    for i in range(0, len(labels[1])):
        name = 'secondLevel' + str(i)
        Type = labels[1][i].dtype.type
        if labels[1][i].dtype.type is np.int32:
            labels[1][i] = to_categorical(labels[1][i])
        numclasses = labels[1][i].shape[1]
        if numclasses > 1:
            activation = 'softmax'
            losses[name] = 'categorical_crossentropy'
        else:
            activation = 'sigmoid'
            losses[name] = 'mse'
        out = Dense(numclasses, activation=activation, name=name)(hidden)
        secondLevelOuts.append(out)
    concatenatedFinal = concatenate(secondLevelOuts)'''
    labels[2][0] = to_categorical(labels[2][0])
    numclasses = labels[2][0].shape[1]
    #losses['out'] = 'categorical_crossentropy'
    out = Dense(numclasses, activation='softmax', name='out')(hidden)
    res = Model(inputs=firstLevelModels, outputs=out)
    res.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
    Inputs = {}
    for i in range(0, len(data)):
        for j in range(0, data[i]):
            Inputs['inp_' + 'firstLevel' + str(i) + str(j)] = dataLearn[i][j]
    lVal = labelsVal[2][-1]
    res.fit(Inputs, out,
            epochs=epochs, batch_size=batch_size, validation_data=(dataVal, lVal), shuffle=shuffle, verbose=2,
            callbacks=[ModelCheckpoint(Name, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False,
                                       mode='max', period=1)])
    return res
def createBoostedOneOutLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare=0.05, shuffle=False, asName='', returnName=True):
    model1 = createFitSaveOneOutConvLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare, shuffle=False, asName='', returnName=False)
    mistakesDataset = validateNNonData(model1, data, labels, 0, batch_size)
    model2 = createFitSaveDeepOneOutConvLSTM(mistakesDataset[0], mistakesDataset[1], neuronsCount//8, 1, epochs, validationShare, shuffle=False, asName='', returnName=False)
    mistakesDataset = validateNNonData(model2, data, labels, 0, batch_size)
    model3 = createFitSaveDeepOneOutConvolutional(mistakesDataset[0], mistakesDataset[1], neuronsCount//4, batch_size, epochs, validationShare, shuffle=False, asName='', returnName=False)
    mistakesDataset = validateNNonData(model3, data, labels, 0, batch_size)
    model4 = createFitSaveDeepOneOutConvLSTM(mistakesDataset[0], mistakesDataset[1], neuronsCount//8, 1, epochs, validationShare, shuffle=False, asName='', returnName=False)
    for layer in model1.layers:
        layer.trainable = False
    for layer in model2.layers:
        layer.trainable = False
    for layer in model3.layers:
        layer.trainable = False
    for layer in model4.layers:
        layer.trainable = False
    name = asName + '.h5'
    length = data[0].shape[0]
    validationLength = int(length * validationShare // batch_size * batch_size)
    splittedData = cutDataLabels(data, labels, int(length // 1.4), int(length // 1.4 + validationLength))
    dataLearn = splittedData['dataEdge']
    dataVal = splittedData['dataMiddle']
    labelsLearn = splittedData['labelsEdge'][0]
    labelsLearn = to_categorical(labelsLearn)
    labelsVal = splittedData['labelsMiddle'][0]
    labelsVal = to_categorical(labelsVal)
    numclasses = labelsLearn.shape[1]
    Inputs = model1._feed_inputs
    first = model1(Inputs)
    second = model2(Inputs)
    third = model3(Inputs)
    fourth = model4(Inputs)
    concatenated = concatenate([first, second, third, fourth])
    count = concatenated._shape[1]
    hidden = Dense(8, activation='softmax')(concatenated)
    hidden = Dropout(0.1)(hidden)
    hidden = Dense(8, activation='softmax')(hidden)
    hidden = Dropout(0.1)(hidden)
    out = Dense(numclasses, activation='softmax', name='outFinal')(hidden)
    res = Model(inputs=Inputs, outputs=out)
    res.compile(optimizer='rmsprop', loss={'outFinal': 'categorical_crossentropy'}, metrics=['categorical_accuracy'])
    print(asName)
    print(res.summary())
    Inputs = {}
    for i in range(0, len(data)):
        Inputs['inp' + str(i)] = dataLearn[i]
    res.fit(Inputs, {'outFinal': labelsLearn},
            epochs=epochs, batch_size=3, validation_data=(dataVal, labelsVal), shuffle=False, verbose=2,
            callbacks=[ModelCheckpoint(name, monitor='categorical_accuracy', verbose=2, save_best_only=True, save_weights_only=False,
                                       mode='max', period=1)])
    if not returnName:
        return res
    else:
        del (res)
        return name
def createBoostedEmbeddedLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare=0.1, shuffle=False, asName='', returnName=True):
    model1 = createEmbeddedLSTM(data, labels, neuronsCount, batch_size, epochs, validationShare, shuffle=False,
                                     asName='', returnName=False)
    mistakesDataset = validateNNonData(model1, data, labels, 1, batch_size)
    model2 = createEmbeddedLSTM(mistakesDataset[0], mistakesDataset[1], neuronsCount // 3, 1, epochs,
                                     validationShare, shuffle=False, asName='', returnName=False)
    for layer in model1.layers:
        layer.trainable = False
    for layer in model2.layers:
        layer.trainable = False
    name = asName + '.h5'
    length = data[0].shape[0]
    validationLength = int(length * validationShare // batch_size * batch_size)
    splittedData = cutDataLabels(data, labels, int(length // 1.4), int(length // 1.4 + validationLength))
    dataLearn = splittedData['dataEdge']
    dataVal = splittedData['dataMiddle']
    labelsLearn = splittedData['labelsEdge'][0]
    labelsLearn = to_categorical(labelsLearn)
    labelsVal = splittedData['labelsMiddle'][0]
    labelsVal = to_categorical(labelsVal)
    numclasses = labelsLearn.shape[1]
    Inputs = model1._feed_inputs
    first = model1(Inputs)
    second = model2(Inputs)
    concatenated = concatenate([first, second])
    count = concatenated._shape[1]
    hidden = Dense(2, activation='softmax')(concatenated)
    hidden = Dropout(0.1)(hidden)
    hidden = Dense(2, activation='softmax')(hidden)
    hidden = Dropout(0.1)(hidden)
    out = Dense(numclasses, activation='softmax', name='outFinal')(hidden)
    res = Model(inputs=Inputs, outputs=out)
    res.compile(optimizer='rmsprop', loss={'outFinal': 'categorical_crossentropy'}, metrics=['categorical_accuracy'])
    print(asName)
    print(res.summary())
    Inputs = {}
    for i in range(0, len(data)):
        Inputs['inp' + str(i)] = dataLearn[i]
    res.fit(Inputs, {'outFinal': labelsLearn},
            epochs=epochs, batch_size=3, validation_data=(dataVal, labelsVal), shuffle=False, verbose=2,
            callbacks=[ModelCheckpoint(name, monitor='val_categorical_accuracy', verbose=2, save_best_only=True,
                                       save_weights_only=False,
                                       mode='max', period=1)])
    if not returnName:
        return res
    else:
        del (res)
        return name
def fitOneOutLSTM(model, data, labels, batch_size, epochs, shuffle=False, asName=''):
    labels = to_categorical(labels[0])
    numclasses = labels.shape[1]
    Inputs = {}
    for i in range(0, len(data)):
        Inputs['inp'+str(i)]=data[i]
    model.fit(Inputs, {'out': labels},
                    epochs=epochs, batch_size=batch_size, validation_split=0.05, shuffle=shuffle)
    if asName != '':
        model.save(model, asName)
        del(model)
#Прогоняет нейросеть на датасете, возвращает подмножество, на котором нейросеть в результате отображения (confSell, confBuy) ошиблась.
#Ошибки продажи и покупки не различает. Типа бустинг ок


class MoneyKeepingCommittee():
    smoothy = int
    batch_size = int
    committee = {}
    internals = {}
    success = {}
    dataMasks = {}
    labelMasks = {}
    historicSuccess = []
    expertsCount = 1
    complex = False
    n = int
    def __init__(self, smoothy=None, successLength=3):
        if smoothy != None:
            self.smoothy = smoothy
            self.successLength = successLength
    def create(self, batch_size, neurons=80, epochs=(10,10), without_learning=False):
        self.complex = False
        self.batch_size = batch_size
        data = loadData('dataset' + str(self.smoothy))
        self.committee = {}
        self.dataMasks = {}
        self.labelMasks = {}
        epochsStates = epochs
        olddir = os.getcwd()
        dir = os.getcwd() + dataPath
        os.chdir(dir)
        self.dataMasks['tcn'] = [['small', 'states'], ['small', 'statesE'],
                                              ['middle', 'states'], ['middle', 'moves'], ['middle', 'osc'],
                                              ['middle', 'hours'], ['middle', 'fibos'], ['middle', 'per25'],
                                              ['large', 'states'], ['large', 'moves'], ['large', 'osc'],
                                              ['large', 'hours'], ['large', 'fibos'], ['large', 'per25'],
                                              ['middle', 'tsf'], ['large', 'tsf'], ['middle', 'wave'],
                                              ['large', 'wave']]
        self.labelMasks['tcn'] = [['out', 'states'],]
        self.dataMasks['convlstm'] = [['small', 'states'], ['small', 'statesE'],
                                              ['middle', 'states'], ['middle', 'moves'], ['middle', 'osc'],
                                              ['middle', 'hours'], ['middle', 'fibos'], ['middle', 'per25'],
                                              ['large', 'states'], ['large', 'moves'], ['large', 'osc'],
                                              ['large', 'hours'], ['large', 'fibos'], ['large', 'per25'],
                                              ['middle', 'tsf'], ['large', 'tsf'], ['middle', 'wave'],
                                              ['large', 'wave']]
        self.labelMasks['convlstm'] = [['out', 'states'],]
        dataTcn = self.fromDataMask(data, 'tcn')
        labelTcn = self.fromLabelMask(data, 'tcn')
        '''self.committee['simple']['embedded'] = createEmbeddedLSTM(dataEmbedded,
                                                                         labelEmbedded, neurons,
                                                                         batch_size, epochsEmbedded, shuffle=False,
                                                                         asName='embedded' + str(self.smoothy), regression=True)'''
        if not without_learning:
            # self.committee['tcn'] = 'tcn20.h5'
            self.committee['tcn'] = create_fit_save_one_out_tcn(dataTcn, labelTcn, neurons, batch_size, epochs[0], shuffle=False, asName='tcn' + str(self.smoothy))
            self.committee['convlstm'] = createFitSaveOneOutConvLSTM(dataTcn, labelTcn, neurons, batch_size, epochs[1], shuffle=False, asName='convlstm' + str(self.smoothy))
        else:
            self.committee['tcn'] = 'tcn' + str(self.smoothy) + '.h5'
            self.committee['convlstm'] = 'convlstm' + str(self.smoothy) + '.h5'
        os.chdir(olddir)
        saveData([self.committee, self.dataMasks, self.labelMasks, self.complex], 'committee' + str(self.smoothy))

    def createBoosted(self, batch_size, neuronsCount, epochs=20):
        self.complex = True
        self.batch_size = batch_size
        data = loadData('dataset' + str(self.smoothy))
        self.committee = {'simple': {},}
        self.dataMasks = {'simple': {},}
        self.labelMasks = {'simple': {},}
        epochsStates = epochs
        neuronsCountSmall = neuronsCount // 15
        neuronsCountNormal = neuronsCountSmall * 2
        olddir = os.getcwd()
        dir = os.getcwd() + dataPath
        os.chdir(dir)
        self.dataMasks['complex']['default'] = [['small', 'states'], ['small', 'moves'], ['small', 'osc'],
                                              ['small', 'tanh'], ['small', 'hours'], ['small', 'bolls'],
                                              ['middle', 'states'], ['middle', 'moves'], ['middle', 'osc'],
                                              ['middle', 'tanh'], ['middle', 'hours'], ['middle', 'bolls'],
                                              ['large', 'states'], ['large', 'moves'], ['large', 'osc'],
                                              ['large', 'tanh'], ['large', 'hours']]
        self.labelMasks['complex']['default'] = [['out', 'states'], ]
        dataStates = self.fromDataMask(data, ['complex', 'default'])
        labelStates = self.fromLabelMask(data, ['complex', 'default'])
        name = 'complex' + str(self.smoothy)
        names = {'simple': {}, 'complex': {}}
        self.committee['complex']['0'] = createFitSaveDeepOneOutConvLSTM(dataStates, labelStates, neuronsCountNormal, batch_size, epochsStates, 0.01,
                                             shuffle=False, asName=name + '_0', returnName=False)
        names['complex']['0'] = name + '_0.h5'
        mistakesDataset = validateNNonData(self.committee['complex']['0'], dataStates, labelStates, 0, batch_size)
        del self.committee['complex']['0']
        self.committee['complex']['1'] = createFitSaveDeepOneOutConvLSTM(mistakesDataset[0], mistakesDataset[1], neuronsCountSmall,
                                                                      1, epochsStates, 0.01,
                                                                      shuffle=False, asName=name + '_1', returnName=False)
        names['complex']['1'] = name + '_1.h5'
        for i in range(2, 9, 2):
            mistakesDataset = validateNNonData(self.committee['complex'][str(i - 1)], dataStates, labelStates, 0, batch_size)
            del self.committee['complex'][str(i-1)]
            self.committee['complex'][str(i)] = createFitSaveOneOutConvLSTM(mistakesDataset[0], mistakesDataset[1], neuronsCountNormal,
                                                                      batch_size, epochsStates, 0.01,
                                                                      shuffle=False, asName=name + '_' + str(i), returnName=False)
            names['complex'][str(i)] = name + '_' + str(i) + '.h5'
            mistakesDataset = validateNNonData(self.committee['complex'][str(i)], dataStates, labelStates, 0, batch_size)
            del self.committee['complex'][str(i)]
            self.committee['complex'][str(i+1)] = createFitSaveOneOutConvLSTM(mistakesDataset[0], mistakesDataset[1],
                                                                            neuronsCountSmall,
                                                                            1, epochsStates, 0.01,
                                                                            shuffle=True, asName=name + '_' + str(i+1), returnName=False)
            names['complex'][str(i+1)] = name + '_' + str(i+1) + '.h5'

        os.chdir(olddir)
        self.committee = names
        saveData([self.committee, self.dataMasks, self.labelMasks, self.complex], 'committee' + str(self.smoothy))

    def load(self):
        olddir = os.getcwd()
        self.n = 1
        committee = loadData('committee' + str(self.smoothy))
        self.committee = committee[0]
        self.dataMasks = committee[1]
        self.labelMasks = committee[2]
        #self.complex = committee[3]
        try:
            self.internals = loadData('internals' + str(self.smoothy))
        except:
            os.chdir(olddir)
        dir = olddir + dataPath
        os.chdir(dir)
        self.success = copy.deepcopy(self.committee)
        for type in self.committee:
            name = self.committee[type]
            self.committee[type] = load(name)
            print(name + ' loaded')
            #self.internals[type][subtype]['share'] = 1/self.n
            #self.success[type][subtype] = []
        os.chdir(olddir)

    def save(self):
        saveData((self.committee, self.dataMasks, self.labelMasks, self.complex), 'committee' + str(self.smoothy))

    def predict(self, data, batch_size=None, Type=None):

        message = {}
        if batch_size == None:
            batch_size = self.batch_size
        if not Type:
            for predictor in self.committee:
                dataMask = self.fromDataMask(data, predictor)
                prediction = self.committee[Type].predict(x=dataMask, batch_size=batch_size)
                if prediction.shape[0] == 1:
                    prediction = prediction.flatten()
                row = np.transpose(data['middle']['moves'][0])
                message[predictor] = {'row': row[0], 'date': data['batchDate'],
                                'prediction': prediction, 'signal': self.getSignal(prediction)}
        else:
            dataMask = self.fromDataMask(data, Type)
            prediction = self.committee[Type].predict(x=dataMask, batch_size=batch_size)
            if prediction.shape[0] == 1:
                prediction = prediction.flatten()
            row = np.transpose(data['middle']['moves'][0])
            message = {'row': row[0], 'date': data['batchDate'],
                                  'prediction': prediction, 'signal': self.getSignal(prediction)}
        return message


    def getSignal(self, predictions, single = True):
        statePrediction = []
        if single == True:
            return anl.toSignal(predictions)
        for i in range(0, len(predictions)):
            if len(predictions.shape) == 2:
                statePrediction.append(anl.toSignal(predictions[i]))
            elif len(predictions.shape) == 3:
                statePrediction.append(anl.toSignal(predictions[i])[-1])
        return statePrediction

    def fromFeedback(self, feedback):
        result = {'simple': {}, 'states': {}}
        i = 0
        for type in self.committee:
            for subtype in self.committee[type]:
                result[type][subtype] = feedback[i]
        return result

    def toSignal(self, signals):
        result = []
        for type in self.committee:
            for subtype in self.committee[subtype]:
                result.append(signals[type][subtype])
        return result

    def calculateShares(self, results, fuzzier=None, weighted=False):
        if fuzzier == None:
            fuzzier = anl.parametricFuzzyModel([-0.01, 0.0, 0.015], [0.01, 1/self.n / 2, 1/self.n*1.5])
        currentSucc = self.fromFeedback(results)
        for type in currentSucc:
            for subtype in currentSucc[type]:
                if currentSucc[type][subtype] != 0.0:
                    self.success[type][subtype].append(currentSucc[type][subtype])
                    if len(self.success[type][subtype]) > self.successLength:
                        self.success[type][subtype].pop(0)
                    self.internals[type][subtype]['share'] = fuzzier.calculate(anl.meanLocal(self.success[type][subtype]))
        succ = self.get(structure=self.success)
        self.historicSuccess.append(succ)

    def getShare(self, predictions):
        result = 0.0
        preDict = self.fromFeedback(predictions)
        for type in self.internals:
            for subtype in self.internals[type]:
                if preDict[type][subtype] == 1:
                    result += self.internals[type][subtype]['share']
        if result > 1.0:
            result = 1.0
        return result

    def get(self, structure):
        result = []
        for type in structure:
            for subtype in structure[type]:
                result.append(structure[type][subtype])
        return result

    def getInternals(self, key):
        result = []
        for type in self.internals:
            for subtype in self.internals[type]:
                result.append(self.internals[type][subtype][key])
        return result

    def fromDataMask(self, data, Type):
        dataMask = self.dataMasks[Type]
        needData = []
        for mask in dataMask:
            needData.append(data[mask[0]][mask[1]])
        return needData

    def fromLabelMask(self, data, Type):
        labelMask = self.labelMasks[Type]
        needLabels = []
        for mask in labelMask:
            needLabels.append(data[mask[0]][mask[1]])
        return needLabels



class ExpertSystem():
    def __init__(self, smoothy, count=3):
        self.smoothy = smoothy
        self.expertsCount = count
    def predict(self, data, size=1):
        if size > len(data):
            raise IOError('Недостаточно данных для использования индикатора!')
        onlyLast = False
        if size == 1:
            onlyLast = True
        prediction1 = anl.singleStochastic(data, self.smoothy-20, 9, 0.75, 0.1, 0.3, onlyLast)
        prediction2 = anl.singleStochastic(data, self.smoothy, 9, 0.75, 0.2, 0.3, onlyLast)
        prediction3 = anl.singleStochastic(data, self.smoothy+30, 9, 0.75, 0.15, 0.3, onlyLast)
        pred = [prediction1, prediction2, prediction3]
        prediction = np.array(pred, dtype=np.float32)
        return prediction
#Описательный класс эксперта. (Вставляется в MoneyManager, управление капиталом, описание эффективности поведения в конкретной системе)
class Expert():
    def __init__(self):
        self.dealHistory = {}
        self.predHistory = {}
    def applyCallback(self, pair, predictions, returns):
        try:
            self.dealHistory['total'] = np.append(self.dealHistory['total'], returns)
        except:
            self.dealHistory['total'] = np.array([returns,])
        try:
            self.predHistory['total'] = np.append(self.predHistory['total'], predictions)
        except:
            self.predHistory['total'] = np.array([predictions,])
        try:
            self.dealHistory[pair] = np.append(self.dealHistory[pair], returns)
        except:
            self.dealHistory[pair] = np.array([returns,])
        try:
            self.predHistory[pair] = np.append(self.predHistory[pair], predictions)
        except:
            self.predHistory[pair] = np.array([predictions,])
#Описательный класс нейроэксперта. (Вставляется в MoneyKeepingCommittee, управление экспертными прогнозами, описание эффективности предсказания вне конкретной системы)
class NeuralExpert():
    def __init__(self, name):
        self.name = name
    def load(self):
        olddir = os.getcwd()
        dir = olddir + dataPath
        os.chdir(dir)
        self.model = load_model(self.name)
        os.chdir(olddir)
    def setMasks(self, dataMask = None, labelMask = None):
        self.dataMask = dataMask
        self.labelMask = labelMask
    def setLabelMask(self, mask):
        self.labelMask = mask
    def extractDataMask(self, data):
        res = []
        for comp in self.dataMask:
            res.append(data[comp[0]][comp[1]])
        return res
    def extractLabelMask(self, data):
        res = []
        for comp in self.labelMask:
            res.append(data[comp[0]][comp[1]])
        return res

