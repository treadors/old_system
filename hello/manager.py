import numpy as np
import pandas as pd
from hello.utils import loadData, saveData

class MoneyManager():
    def __init__(self, smoothy):
        self.smoothy = smoothy
        self.history = None
    def add_trade(self, trade):
        if not hasattr(self.history, 'shape'):
            for key in trade:
                trade[key] = [trade[key], ]
            self.history = pd.DataFrame.from_dict(trade)
            #self.history.setIndex(['index', 'symbol'], inplace=True)
        else:
            length = self.history.shape[0]
            self.history.loc[length] = trade
    def load(self):
        self.history = loadData('manager'+str(self.smoothy))
    def save(self):
        saveData(self.history, 'manager'+str(self.smoothy))
        pass
    def executePrediction(self, message):
        return message
    def executeCallback(self):
        pass