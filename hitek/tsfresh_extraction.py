import sys
import warnings
from tsfresh import extract_features, extract_relevant_features, select_features
from tsfresh.utilities.dataframe_functions import impute
import pandas as pd
import numpy as np
import multiprocessing
from hello.analysis import fuzzymodel

cpus = multiprocessing.cpu_count() - 1

def extract_multihistoric_features(df, column, position, histories, internal_fuzzier, external_fuzzier, scaler=None):

    # Internal fuzzier determinate row len to put in tsfresh feature extractor
    # External fuzzier determinate the widths of output features matrix

    end = position
    starts = [end-history for history in histories]
    str_histories = [str(his) for his in histories]
    f = fuzzymodel(cells=internal_fuzzier)
    if not scaler:
        iFuzzed = [f.fuzz(df[column].values[start:end]) for start in starts]
    else:
        iFuzzed = [(scaler().fit_transform(f.fuzz(df[column].values[start:end]).reshape(-1, 1))).flatten() for start in starts]
    dataframes = [pd.DataFrame(fuzzied, columns=['values', ]) for fuzzied in iFuzzed]
    concatenated = pd.concat(tuple(dataframes), keys=str_histories)
    concatenated = pd.DataFrame(concatenated.to_records())
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore")
        features = extract_features(concatenated, column_id='level_0', column_sort='level_1', impute_function=impute,
                                    n_jobs=cpus, disable_progressbar=True, show_warnings=False)
    mtrx = features.values
    rows_count = mtrx[0].size // external_fuzzier + 1
    zeros_count = external_fuzzier * rows_count - mtrx[0].size
    results = []
    for i in range(mtrx.shape[0]):
        results.append(np.append(mtrx[i], [0 for j in range(zeros_count)]).reshape((rows_count, external_fuzzier)))
    return results