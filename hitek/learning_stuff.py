import os

import keras.models
import numpy as np
import tensorflow as tf
from keras import backend as K
from keras.layers import Input
from sklearn.metrics import roc_auc_score
from keras.regularizers import l1_l2
from keras.callbacks import Callback

from hello import analysis as anl
from preferences import dataPath


reg = l1_l2(0.005, 0.005)
rmsprop_high = keras.optimizers.rmsprop(lr=0.1)
LreluHard = keras.layers.advanced_activations.LeakyReLU(alpha=0.01)
LreluMiddle = keras.layers.advanced_activations.LeakyReLU(alpha=0.02)
LreluSoft = keras.layers.advanced_activations.LeakyReLU(alpha=0.05)


def auc(y_true, y_pred):
    auc, up_opt = tf.metrics.auc(y_true, y_pred)
    K.get_session().run(tf.local_variables_initializer())
    with tf.control_dependencies([up_opt]):
        auc = tf.identity(auc)
    return auc


def auroc(y_true, y_pred):
    return tf.py_func(roc_auc_score, (y_true, y_pred), tf.double)


def roc_auc(y_true, y_pred):
    return roc_auc_score(y_true=y_true, y_score=y_pred)


class roc_callback(Callback):

    def __init__(self,training_data,validation_data):
        self.x = training_data[0]
        self.y = training_data[1]
        self.x_val = validation_data[0]
        self.y_val = validation_data[1]


    def on_train_begin(self, logs={}):
        return

    def on_train_end(self, logs={}):
        return

    def on_epoch_begin(self, epoch, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        y_pred = self.model.predict(self.x)
        roc = roc_auc_score(self.y, y_pred)
        y_pred_val = self.model.predict(self.x_val)
        roc_val = roc_auc_score(self.y_val, y_pred_val)
        print('\rroc-auc: %s - roc-auc_val: %s' % (str(round(roc,6)),str(round(roc_val,6))),end=100*' '+'\n')
        return

    def on_batch_begin(self, batch, logs={}):
        return

    def on_batch_end(self, batch, logs={}):
        return


def validateNNonData(model, data, labels, outNum, batch_size, confSell=0.0, confBuy=0.0):
    predictions = model.predict(x=data, batch_size=batch_size)
    if len(predictions.shape) > 2:
        prediction = predictions[outNum]
    else:
        prediction = predictions
    label = labels[outNum]
    statePrediction = anl.fromStates(prediction, confSell, confBuy)
    mistakesMask = anl.getMistakesMask(label, statePrediction)
    mistakesData = extractDataFromMask(data, labels, mistakesMask)
    mistakesPred = np.take(prediction, mistakesMask)
    return [mistakesData[0], mistakesData[1], mistakesPred]


def save(model, name):
    olddir = os.getcwd()
    dir = os.getcwd() + dataPath
    os.chdir(dir)
    name = name + '.h5'
    model.save(name)
    os.chdir(olddir)


def load(name):
    olddir = os.getcwd()
    if not 'data' in olddir:
        dir = os.getcwd() + dataPath
    else:
        dir = olddir
    os.chdir(dir)
    # name = name + '.h5'
    model = keras.models.load_model(name, custom_objects={'auc': auc})
    os.chdir(olddir)
    return model


def cutDataSet(data, start, end):
    dataMiddle = {}
    dataEdge = {}
    for type in data:
        dataMiddle[type] = {}
        dataEdge[type] = {}
        for subtype in data[type]:
            res = np.split(data[type][subtype], [start,end], axis=0)
            dataEdge[type][subtype] = np.concatenate([res[0], res[2]])
            dataMiddle[type][subtype] = res[1]
    return [dataEdge, dataMiddle]


def cutData(data, start, end):
    if not type(data) is list:
        data = [data,]
    res = {'dataMiddle': [], 'dataEdge': []}
    for memba in data:
        dataRes = np.split(memba, [start, end], axis=0)
        res['dataMiddle'].append(dataRes[1])
        res['dataEdge'].append(np.concatenate([dataRes[0], dataRes[2]]))
    return res


def cutDataLabels(data, labels, start, end):
    if not type(labels) is list:
        labels = [labels,]
    if not type(data) is list:
        data = [data,]
    res = {'dataMiddle': [], 'labelsMiddle': [], 'dataEdge': [], 'labelsEdge': []}
    for memba in data:
        dataRes = np.split(memba, [start, end], axis=0)
        res['dataMiddle'].append(dataRes[1])
        res['dataEdge'].append(np.concatenate([dataRes[0], dataRes[2]]))
    for memba in labels:
        labelsRes = np.split(memba, [start, end], axis=0)
        res['labelsMiddle'].append(labelsRes[1])
        res['labelsEdge'].append(np.concatenate([labelsRes[0], labelsRes[2]]))
    return res


def cutMultiDimentionalData(data, labels, start, end):
    res = {'dataMiddle':[], 'labelsMiddle':[], 'dataEdge':[], 'labelsEdge':[]}
    for d in data:
        splitted = cutData(d, start, end)
        res['dataMiddle'].append(splitted['dataMiddle'])
        res['dataEdge'].append(splitted['dataEdge'])
    for l in labels:
        splitted = cutData(l, start, end)
        res['labelsMiddle'].append(splitted['dataMiddle'])
        res['labelsEdge'].append(splitted['dataEdge'])
    return res


def extractDataFromMask(data, labels, mask, type=0):
    if type == 0:
        dataNew = []
        labelsNew = []
        for d in data:
            dataNew.append(np.take(d, mask, axis=0))
        for l in labels:
            labelsNew.append(np.take(l, mask, axis=0))
        return [dataNew, labelsNew]
    if type == 1:
        zeros = mask[0]
        ones = mask[1]
        dataZerosNew = []
        labelsZerosNew = []
        dataOnesNew = []
        labelsOnesNew = []
        for d in data:
            dataZerosNew.append(np.take(d, zeros))
            dataOnesNew.append(np.take(d, ones))
        for l in labels:
            labelsZerosNew.append(np.take(l, zeros))
            labelsOnesNew.append(np.take(l, ones))
        return {'zeros': [dataZerosNew, labelsZerosNew], 'ones': [dataOnesNew, labelsOnesNew]}


# Concatenate data from list/tuple (or dict with/without dataMask) into numpy multidimentional array
def dummy_data_constructor(data, dataMask=None, axis=2):

    res = None

    if isinstance(data, dict) and not dataMask:
        for name, d in enumerate(data):
            if not hasattr(res, 'shape'):
                res = d
            else:
                res = np.concatenate((res, d), axis=axis)
    elif isinstance(data, dict) and dataMask:
        for item in dataMask:
            print(item)
            try:
                d = data[item[0]][item[1]]
                if not hasattr(res, 'shape'):
                    res = d
                else:
                    res = np.concatenate((res, d), axis=axis)
            except Exception as e:
                print(e)
    elif isinstance(data, list) or isinstance(data, tuple):
        for d in range(0, len(data)):
            if not hasattr(res, 'shape'):
                res = d
            else:
                res = np.concatenate((res, d), axis=axis)

    print('Finally data has a shape {}'.format(res.shape))
    return res

# Create sequentional data from list/tuple (or dict, with/without dataMask). You need to specialize, which data need to be
# executed as timeseries (at now the last point in data table will be keeped as timestep), and which data need to be executed
# as addition features (will be flatten and then append to sequantional)
def time_series_constructor(data, timeSeriesDataMask=None, additionalDataMask=None, axis=2):
    if isinstance(data, dict) and timeSeriesDataMask:
        time_series = dummy_data_constructor(data, timeSeriesDataMask)[:,-1,:]
        features = dummy_data_constructor(data, additionalDataMask)
        features = np.reshape(features, (features.shape[0], -1))
    res = np.concatenate((time_series, features), axis=1)
    res = np.reshape(res, (res.shape[0], 1, res.shape[1]))
    print('Finally data has a shape {}'.format(res.shape))
    return res

def create_input_by_data(data):
    return Input(shape=(data.shape[1], data.shape[2]), name='GeneralInput')