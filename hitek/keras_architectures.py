from keras.layers import Input, concatenate, Dense, GaussianNoise, Dropout, BatchNormalization, Reshape
from keras.layers import LSTM, RepeatVector
from keras import Model
from hitek.learning_stuff import reg, LreluMiddle


# Create input tensors for list (or tuple or dict) of input data. Than possibly concate this inputs into one (here we can meet the problem
# of dimensions mismatch, and here possible to flat overall input. In the case when dataMask != None, data apologize as
# dict of different inputs. For now you need to specify which input you need to construct in input_name variable
def dummy_input_creator(data, dataMask=None, input_name=None, concat=True, flatten=False):

    inputs = []

    if isinstance(data, dict) and not dataMask:
        for name, d in enumerate(data):
            inputs.append(Input(shape=(d.shape[1], d.shape[2]), name=name))
    elif isinstance(data, dict) and dataMask:
        if dataMask and not input_name:
            raise (Exception('Please specify in \'input_name\' which input you want to construct (from datamask).'))
        data_mask = dataMask[input_name]
        for item in data_mask:
            print(item)
            try:
                d = data[item[0]][item[1]]
                inputs.append(Input(shape=(d.shape[1], d.shape[2]), name=item[0]+'_'+item[1]))
            except Exception as e:
                print(e)
    elif isinstance(data, list) or isinstance(data, tuple):
        for i in range(0, len(data)):
            inputs.append(Input(shape=(data[i].shape[1], data[i].shape[2]), name='inp' + str(i)))
    if concat:
        try:
            inputs = concatenate(inputs)
        except Exception as e:
            if flatten:
                # Special option if you pass concat and flatten to true, but can't concat inputs course dimension mismatch.
                inputs = concatenate(inputs, axis=1)
            else:
                raise e
    if flatten:
        inputs = concatenate(inputs, axis=1)
    return inputs


# Create simple autoencoder model which compress input tensor along 1 axis by compressive factor
# (for example Nx100x5 tensor with compressive factor 0.5 will be compressed to Nx50x5 tensor, Nx100 flat
# tensor become Nx50 e.g.)
def simple_compressive_autoencoder(input, compressing_factor=0.5, denoising=None,
                                   regularize=None, dropout=None, normalize=True, counter=0, layers=2):
    inputs = input
    if denoising:
        inputs = GaussianNoise(stddev=denoising)(input)
    if normalize:
        inputs = BatchNormalization()(input)
    input_shapes = input._keras_shape
    print(input_shapes)
    compressing = int(input_shapes[2]*compressing_factor)
    hidden = inputs
    for i in range(layers):
        name = 'hidden_' + str(counter) + '_layer_'+str(i)
        if regularize:
            hidden = Dense(compressing, kernel_regularizer=regularize, bias_regularizer=reg, activity_regularizer=reg,
                           activation='tanh', name=name)(hidden)
        else:
            hidden = Dense(compressing, activation='tanh', name=name)(hidden)
        if dropout:
            hidden = Dropout(dropout)(hidden)
    out = Dense(input_shapes[2], activation='tanh', name='out'+str(counter))(hidden)
    print(out._keras_shape)

    res = Model(inputs=input, outputs=out)
    res.compile(optimizer='rmsprop', loss={'out'+str(counter): 'mse'}, metrics=['accuracy'])
    print(res.summary())
    return res

#
def stacked_fullconnect_encoder(data, compressions, batch_size, epochs,
                                     denoising=None, regularize=None, dropout=None, normalize=True):

    if not (isinstance(compressions, list) or isinstance(compressions, tuple)):
        compressions = tuple(compressions)
    input = Input(shape=(data.shape[1], data.shape[2]))
    print('Input for autoencoder has shape {0}'.format(input._keras_shape))
    for i in range(len(compressions)):
        if i == 0:
            aec = simple_compressive_autoencoder(input=input, compressing_factor=compressions[i],
                                                 denoising=denoising, regularize=regularize, dropout=dropout,
                                                 normalize=normalize, counter=i)
            aec.fit(data, data, batch_size=batch_size, epochs=epochs, verbose=2)
    return aec




# Here the place to experiments with autoencoders.
# Let's start from the Fchollet tutorial

def lstm_autoencoder(data, timesteps, batch_size, epochs,
                     denoising=None, regularize=None, dropout=None, normalize=True):

    input_dim = data.shape[2]
    input = Input(shape=(batch_size, input_dim))
    input_shapes = input._keras_shape
    print('Input for autoencoder has shape {0}'.format(input._keras_shape))

    encoded = LSTM(timesteps, recurrent_regularizer=regularize if regularize else None,
                   kernel_regularizer=regularize if regularize else None,
                   bias_regularizer=regularize if regularize else None)(input)
    print('Encoded layer output has shape {0}'.format(encoded._keras_shape))
    encoded = LreluMiddle(encoded)

    decoded = RepeatVector(batch_size)(encoded)
    print('RepeatVector output has shape {0}'.format(decoded._keras_shape))
    decoded = LSTM(input_dim, return_sequences=True)(decoded)
    print('Decoder output has shape {0}'.format(decoded._keras_shape))

    sequence_autoencoder = Model(input, decoded)
    encoder = Model(input, encoded)
    sequence_autoencoder.compile(optimizer='rmsprop', loss='mse', metrics=['accuracy'])
    print(sequence_autoencoder.summary())
    sequence_autoencoder.fit(data, data, batch_size=25, epochs=epochs, verbose=2)
    return (sequence_autoencoder, encoder)



