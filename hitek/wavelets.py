import pywt
import numpy as np
import warnings

from hello.analysis import fuzzymodel


# Now support only numpy array as row. Scaler is to rescale results along timeseries axis.
# Cell is to fuzzy compressing/decompressing results. Reconstruct is to return reconstructed row.
def multilevel_haar(row, scaler=None, cells=50, reconstruct=True):

    rowsize = row.shape[0]
    level = pywt.dwt_max_level(rowsize, 'haar')
    dec = pywt.wavedec(row, 'haar', 'smooth', level=level)
    f = fuzzymodel(cells=cells)
    if reconstruct:
        appr = f.fuzz(pywt.waverec(dec, 'haar'))
        appr = np.reshape(appr, (*appr.shape, 1))
    dec_matrix = None
    for level in dec:
        fuzzied_level = f.fuzz(np.repeat(level, rowsize // level.shape[0] + 1))
        fuzzied_level = np.reshape(fuzzied_level, (*fuzzied_level.shape, 1))
        if not hasattr(dec_matrix, 'shape'):
            dec_matrix = fuzzied_level
        else:
            dec_matrix = np.concatenate((dec_matrix, fuzzied_level), axis=1)
    if reconstruct:
        dec_matrix = np.concatenate((dec_matrix, appr), axis=1)
    dec_matrix = dec_matrix.transpose()
    if scaler:
        for i in range(dec_matrix.shape[0]):
            dec_matrix[i, :] = (scaler().fit_transform(dec_matrix[i, :].reshape(-1, 1))).flatten()
        # if reconstruct:
        #     appr[:, 0] = scaler().fit_transform(appr[:, 0])
    return dec_matrix
