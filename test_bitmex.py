import sys, os
from hello.models import Exc_rate
from hello.wrapper import Wrapper
from datetime import datetime
from datetime import timedelta
from django.utils import timezone
import time
mainCurrencies = ['BTC', 'ETH', 'XMR', 'USDT']

wrapper = Wrapper('bitmex')
while(True):
	td = timezone.now()
	succ = False
	while not succ:
		try:
			exc_rates = wrapper.getExcRatesListBitmex()
			succ = True
		except:
			succ = False
	wrapper.saveExcRatesToDB(exc_rates)
	td_new = timezone.now()
	while (td_new - timedelta(minutes=10) < td):
		time.sleep(1)
		td_new = timezone.now()