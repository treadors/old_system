from accounts import SergeyKeys
from hello.environment import *
from hello.system import TradingSystem
from matplotlib import pyplot as plt
import pandas as pd
from dateutil.parser import parse
CPs = {'BTC': ['ZRX/BTC'],
       'USDT': []}
env = Environment(Test=False, API=SergeyKeys[0], secret=SergeyKeys[1], source='poloniex', CurrencyPairs=CPs)
port = Portfolio(env)
TS = TradingSystem(env, port)
env.getExcRates()
# env.cutCurrenciesByVolume('BTC', 7)
# env.cutCurrenciesByVolume('USDT', 0)
start = parse('2017-08-26')
end = parse('2018-11-26')
# start = timezone.now() - timedelta(weeks=27)
# end = timezone.now()
env.getAllData(start, end, 1)
TS.addLayer(20, {'BTC': len(env.CurrencyPairTypes['BTC']), 'USDT': len(env.CurrencyPairTypes['USDT'])}, Share=1.0)
TS.FullLayerSetup(TS.Layers[0])
TS.Optimize()
# TS.BackTest(minProfit=0.045, enableSwitcher=True, historic_eff_len=15, confidence_level_buy=0.999)
TS.Layers[0].moneyManager.history.plot.scatter(x='open_conf', y='factic_perc_pnl')
TS.Layers[0].moneyManager.history.plot.scatter(x='close_conf', y='factic_perc_pnl')
plt.show()