from hello.system import *

class Learner():
    env = Environment
    models = []
    TradableBTC = int
    TradableUSDT = int
    LearningBTC = int
    LearningUSDT = int
    def __init__(self, BTCTradableCount=3, USDTTradableCount=0, BTCLearningCount=0, USDTLearningCount=0):
        self.TradableBTC = BTCTradableCount
        self.TradableUSDT = USDTTradableCount
        self.LearningBTC = BTCLearningCount
        self.LearningUSDT = USDTLearningCount
        self.KernelBTC = self.LearningBTC + self.TradableBTC
        self.KernelUSDT = self.LearningUSDT + self.TradableUSDT
        self.Start = True
    def initialize(self):
        self.env = Environment()
        self.TS = TradingSystem(self.env, None)
    def step(self):
        self.initialize()
        for i in range(1):
            self.env.getCurrencies()
            self.env.getExcRates()
            self.env.cutCurrenciesByVolume('BTC', self.KernelBTC)
            self.env.cutCurrenciesByVolume('USDT', self.KernelUSDT)
            self.env.getAllData(timezone.now()-timedelta(weeks=25), timezone.now()-timedelta(weeks=10), 1, load_scalers=True)
            self.TS.addLayer(20, {'BTC': len(self.env.CurrencyPairTypes['BTC']),
                                  'USDT': len(self.env.CurrencyPairTypes['USDT'])}, Share=1.0)
            self.TS.Layers[i].initializeTradingSystem()
            pairs = self.TS.Layers[i].getPairs({'BTC': self.KernelBTC,'USDT': self.KernelUSDT})
            # self.TS.Layers[i].createDataSets(pairs, statesLength=10, batch_size=250, step=6)
            # self.TS.Layers[i].rescale_datasets()
            self.TS.Layers[i].cutTradingSystem({'BTC': self.TradableBTC, 'USDT': self.TradableUSDT}, communism=True)
            self.TS.Layers[i].saveLayerConfiguration()
            self.TS.Layers[i].saveTradingSystem()
            committee = MoneyKeepingCommittee(self.TS.Layers[i].smoothy)
            committee.create(batch_size=25, neurons=100, epochs=(200, 50), without_learning=False)
        self.clear()
    def clear(self):
        self.models = []
        self.env = Environment
    def start(self):
        while True:
            self.step()
            break

lrnr = Learner()
lrnr.start()
#LM = LearnModel(40)
#LM.createMover()
#LM.fitMover()
env = Environment()
env.addLayer(20, 10, 1, 1.0)
committee = MoneyKeepingCommittee(env.Layers[0].smoothy)
committee.create(batch_size=25)
committee.load()
input_shapes1 = committee.committee['simple']['states']._feed_input_shapes
out_shapes1 = committee.committee['simple']['states']._feed_output_shapes
print('ahaha')
#data = loadData('dataset' + str(committee.smoothy))
#prediction = committee.predict(data)
#print(prediction.shape)
#print('ahah')

