# coding: utf-8
# В минутах
historicStep = 10
# В днях
globalHistorySize = 50 # Емкость исторических данных в днях
globalDataLen = 2900 # History capacity in 10 minutes ticks
# Линуксовая версия программы?
forheroku = True
# частота диверсификации, м.
correlationAnalysisFrequency = 120
# частота обновления данных, ч.
updateFrequency = 10
# цикл жизни программы, с.
frameFrequency = 60
# 30*24*7 = 4320 - ёмкость исторических данных
historySize = 4320
localHistorySize = 180
# Степень занижения/завышения ордера на продажу/покупку.
# Идея в том, что это позволит не проебаться с тем, что ордер может быть выставлен, но не исполнен из-за промедления.
compensation = 0.001
if not forheroku:
    dataPath=r'\data\\'[:-1]
else:
    dataPath=r'/data//'[:-1]
#комиссия poloniex, maker - снимается при исполнении собственных ордеров, taker - снимается при исполнении чужих
makerFee = 0.0015
takerFee = 0.0025

commission = 0.0
slippage = 0.0

#The time in seconds which program will wait till try to reconnect (if failed to connect with exchange API)
reconnect_timeout = 20
#How many times program will try to reconnect to exchange if sth goes wrong
reconnect_count = 100
