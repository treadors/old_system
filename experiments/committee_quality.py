import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.metrics import confusion_matrix
import pandas as pd

from hello.utils import loadData
from hello.committee import MoneyKeepingCommittee
from hello.analysis import tresholded_signals
from hitek.learning_stuff import cutMultiDimentionalData
from hitek.utils import plot_confusion_matrix

# data = loadData('dataset20')
# committee = MoneyKeepingCommittee(20)
# committee.load()
# labels = data['out']['states'].flatten()
# predictions_tcn = committee.predict(data, batch_size=25, Type='tcn')['prediction']
# predictions_lstm = committee.predict(data, batch_size=25, Type='convlstm')['prediction']
# df = pd.DataFrame({'label': labels, 'lstm_preds_0': predictions_lstm[:, 0], 'lstm_preds_1': predictions_lstm[:, 1],
#                    'tsn_preds_0': predictions_tcn[:, 0], 'tsn_preds_1': predictions_tcn[:, 1]})
# print(df.head)
# df.to_csv('preds20.csv')
# fig = plt.figure()
# ax = Axes3D(fig)
# ax.scatter(predictions_tcn, predictions_lstm, labels)
# ax.set_xlabel('TCN')
# ax.set_ylabel('LSTM')
# ax.set_zlabel('Labels')
# plt.show()
df = pd.read_csv('preds20.csv', index_col='Unnamed: 0')
df['preds'] = (df['lstm_preds_1'] + df['tsn_preds_1']) / 2
df['labels_lstm'] = df['lstm_preds_1'].apply(lambda x: 0 if x < 0.75 else 1)
df['labels_tsn'] = df['tsn_preds_1'].apply(lambda x: 0 if x < 0.15 else 1)
df['labels_pred'] = df['preds'].apply(lambda x: 0 if x < 0.5 else 1)
df['diff'] = df['label'] - df['lstm_preds_1']
# df['diff'].plot.hist(bins=1000)
# df['preds'].plot.hist(bins=1000)
plt.show()
labels = df['label'].values
preds = df['labels_lstm'].values
cm = confusion_matrix(labels, preds)

plot_confusion_matrix(cm, classes=('sell', 'buy'), normalize=True,
                      title='Normalized confusion matrix')
# print(df.head())
plt.show()