import matplotlib
matplotlib.use('Qt5Agg', warn=False, force=True)
from matplotlib import pyplot as plt
from tsfresh.examples.robot_execution_failures import download_robot_execution_failures, load_robot_execution_failures
from tsfresh import extract_features, extract_relevant_features, select_features
from tsfresh.utilities.dataframe_functions import impute
from tsfresh.feature_extraction import ComprehensiveFCParameters
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
import pandas as pd
import numpy as np

from hello.analysis import fuzzymodel

df = pd.read_csv('example_table.csv')
print(df.info())

fuzzier = 50

end = df.index[-1]
f = fuzzymodel(cells=fuzzier)
smallHistory = 200
middleHistory = 400
extendedHistory = 1200
startS = end - smallHistory
startM = end - middleHistory
startE = end - extendedHistory
iScaledS = f.fuzz(df['scaled_price'].values[startS:end])
iScaledM = f.fuzz(df['scaled_price'].values[startM:end])
iScaledE = f.fuzz(df['scaled_price'].values[startE:end])
iScaledS = pd.DataFrame(iScaledS, columns=['values',])
iScaledM = pd.DataFrame(iScaledM, columns=['values',])
iScaledE = pd.DataFrame(iScaledE, columns=['values',])
prices = pd.concat((iScaledS, iScaledM, iScaledE), keys=['0', '1', '2'])
prices = pd.DataFrame(prices.to_records())
print(prices.tail(10))
features = extract_features(prices,
                     column_id='level_0', column_sort='level_1',
                     impute_function= impute)
print(features.columns)
mtrx = features.as_matrix()
s = mtrx[0]
m = mtrx[1]
e = mtrx[2]
rows_count = s.size // fuzzier + 1
zeros_count = fuzzier * rows_count - s.size
s = np.append(s, [0 for i in range(zeros_count)])
m = np.append(m, [0 for i in range(zeros_count)])
e = np.append(e, [0 for i in range(zeros_count)])
s = s.reshape((rows_count, fuzzier))
m = m.reshape((rows_count, fuzzier))
e = e.reshape((rows_count, fuzzier))

# s = features[features.columns]['0']
# m = features[features.columns]['1']
# e = features[features.columns]['2']
print(features.head())
print(features.info())
