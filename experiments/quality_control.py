import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

from sklearn.metrics import roc_curve, auc, confusion_matrix
from keras.utils import to_categorical
import itertools

from hello.utils import loadData
from hello.committee import MoneyKeepingCommittee
from hello.analysis import toSignal, tresholded_signals
from hitek.utils import plot_confusion_matrix

data = loadData('dataset20')
committee = MoneyKeepingCommittee(20)
committee.load()
labels = data['out']['states'].flatten()
predictions = committee.predict(data, batch_size=25, Type='tcn')['prediction']
flat = tresholded_signals(predictions, 0.7)
cm = confusion_matrix(labels, flat)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cm, classes=('sell', 'buy'),
                      title='Confusion matrix, without normalization')

# Plot normalized confusion matrix
plt.figure()
plot_confusion_matrix(cm, classes=('sell', 'buy'), normalize=True,
                      title='Normalized confusion matrix')

plt.show()

# fpr = dict()
# tpr = dict()
# roc_auc = dict()
# for i in range(2):
#     fpr[i], tpr[i], _ = roc_curve(labels[:, i], predictions[:, i])
#     roc_auc[i] = auc(fpr[i], tpr[i])
#
# # Compute micro-average ROC curve and ROC area
# fpr["micro"], tpr["micro"], _ = roc_curve(labels.ravel(), predictions.ravel())
# roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
#
# plt.figure()
# lw = 2
# plt.plot(fpr[0], tpr[0], color='darkorange',
#          lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[0])
# plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
# plt.xlim([0.0, 1.0])
# plt.ylim([0.0, 1.05])
# plt.xlabel('FPR')
# plt.ylabel('TPR')
# plt.title('ROC')
# plt.legend(loc="lower right")
# plt.show()
