import matplotlib
matplotlib.use('Qt5Agg', warn=False, force=True)
from matplotlib import pyplot as plt
from tsfresh.examples.robot_execution_failures import download_robot_execution_failures, load_robot_execution_failures
from tsfresh import extract_features, extract_relevant_features, select_features
from tsfresh.utilities.dataframe_functions import impute
from tsfresh.feature_extraction import ComprehensiveFCParameters
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report

download_robot_execution_failures()
df, y = load_robot_execution_failures()
print(df.tail(50))
print(df.info())
extraction_settings = ComprehensiveFCParameters()
X = extract_features(df,
                     column_id='id', column_sort='time',
                     default_fc_parameters=extraction_settings,
                     impute_function= impute)
print(X.head())
print(X.info())
