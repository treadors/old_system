from dateutil.parser import parse
import pandas as pd
import pywt
from sklearn.preprocessing import MinMaxScaler
from mpl_toolkits import mplot3d
from hello.StndardSignals import StandardSignals, getProfit


from hello.utils import loadData, saveData
from hello.analysis import fuzzymodel
from grabbing.data import *
from hitek.wavelets import multilevel_haar
# datamanager = Data()
# df = datamanager.get_price_data_from_bd('ZRX/BTC', parse('2017-06-26'), parse('2018-11-05'), 1)
# print(df.info())
# df.to_csv('zrx.csv')
df = pd.read_csv('zrx.csv', index_col='Unnamed: 0')
to_analyze = df.loc[(df['timedate'] > '2017-09-01') & (df['timedate'] < '2018-11-01'), 'Close']
x = to_analyze.values
S = StandardSignals(x, 20)
states = S.MakeHoldSignals()
balances, deals = getProfit(x, states, 1.0)
plt.plot(balances)
plt.show()
# xline = np.arange(0, cA.shape[0], 1)
# ax.plot(xline, cD, cA, 'green')
# plt.plot(xline, cA)
# plt.plot(xline, x)
# plt.show()

