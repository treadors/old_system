import pandas as pd
import numpy as np
from functools import reduce

import matplotlib
matplotlib.use('Qt5Agg', warn=False, force=True)
import matplotlib.pyplot as plt

dict = {'first': [-0.1103, 0.1208, 0.3785, 0.2105, 0.1400, 0.2363, 0.6614, 0.5519, -0.0228, 0.5485, 0.2733, 0.2963, 0.1198, -0.023, 0.127, 0.25],
        'second': [-0.0086, 0.6729, 0.5755, -0.0348, -0.1934, -0.0182, 0.5430, 0.6439, 0.1290, 0.3230, 0.1290, -0.0138, 0.0272, -0.0121, 0.2470, 0.2921],
        'third': [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1140, 0.0205, 0.3725, -0.1670,-0.0748, -0.1981, -0.0633, -0.1191],
        'fouth': [0.2000, 0.14, 0.1, 0.5900, -0.88,	0.45, 0.58, 0.92, 0.16, -0.035,	0.20, -0.12, 0.10, 0.0, 0.1474,	0.0863],
        'fifth': [-0.0033, 0.0197, 0.1293, -0.0224, -0.0558, -0.0136, 0.3390, 1.1202, 0.2163, 0.3562, 0.1277, -0.0171, 0.0338, 0.0032, 0.0540, 0.0311],
        'sixth': [-0.0294, 0.0066, 0.0492, 0.0445, 0.0894, 0.0855, 0.4194, 1.2118, 0.0895, 0.3083, 0.1241, 0.1136, 0.0908, 0.0238, 0.1590, 0.1342],
        'seventh': [-0.0449, 0.1956, 0.1812, 0.0586, 0.1058, -0.0382, 0.2735, -0.0298, 0.1309, -0.1426, -0.0145, 0.0968, 0.0320, -0.0178, 0.0261, -0.2364],
        'eighth': [-0.0253, 0.03, 0.0837, 0.0603, 0.0715, 0.0841, 1.5594, 0.00370, 0.5246, 0.2965, 0.2387, 0.1010, -0.0093, 0.0, 0.0, -0.1039],
        'ninth': [-0.0199, 0.0551, 0.4815, -0.0087, -0.0910, 0.1196, 0.2623, 0.5161, 0.2860, 0.2122, 0.1883, -0.0300, -0.0940, 0.1079, 0.2206, 0.0],
        'tenth': [0.0019, -0.0081, 0.0377, 0.0571, 0.0271, 0.0933, 0.2841, 0.5509, 0.3241, 0.2262, 0.0960, 0.0468, -0.0033, 0.0698, 0.1086, 0.0],
        'eleventh': [0.0079, 0.005, 0.0844, 0.1029, 0.0358, 0.1110, 0.4900, 1.1197, 0.1710, 0.4142, 0.2000, 0.1997, 0.0790, 0.0398, 0.0376, 0.0],
        'twelfth': [0.3500, -0.0375, 0.1149, 0.0, 1.1591, 0.2922, 0.0799, 0.4370, 0.3003, -0.0591, 0.1927, 0.0685, -0.0182, 0.3374, 0.0, 0.0048]}
df = pd.DataFrame(dict)

returns_annual = df.mean() * 16
cov_monthly = df.cov()
cov_annual = cov_monthly * 16

num_assets = len(df.keys())
num_portfolios = 2000000

port_returns = []
port_volatility = []
sharpe_ratio = []
stock_weights = []

np.random.seed(101)

for portfolio in range(num_portfolios):
    weights = np.random.random(num_assets)
    weights /= np.sum(weights)
    returns = np.dot(weights, returns_annual)
    volatility = np.sqrt(np.dot(weights.T, np.dot(cov_annual, weights)))
    sharpe = returns / volatility
    sharpe_ratio.append(sharpe)
    port_returns.append(returns)
    port_volatility.append(volatility)
    stock_weights.append(weights)

portfolio = {'Returns': port_returns,
             'Volatility': port_volatility,
             'Sharpe Ratio': sharpe_ratio}

for counter,symbol in enumerate(df.keys()):
    portfolio[symbol] = [Weight[counter] for Weight in stock_weights]

pdf = pd.DataFrame(portfolio)

column_order = ['Returns', 'Volatility', 'Sharpe Ratio', *df.keys()]

pdf = pdf[column_order]

min_volatility = pdf['Volatility'].min()
max_sharpe = pdf['Sharpe Ratio'].max()

sharpe_portfolio = pdf.loc[pdf['Sharpe Ratio'] == max_sharpe]
min_variance_port = pdf.loc[pdf['Volatility'] == min_volatility]

plt.style.use('seaborn-dark')
pdf.plot.scatter(x='Volatility', y='Returns', c='Sharpe Ratio',
                cmap='RdYlGn', edgecolors='black', figsize=(10, 8), grid=True)
plt.scatter(x=sharpe_portfolio['Volatility'], y=sharpe_portfolio['Returns'], c='red', marker='D', s=200)
plt.scatter(x=min_variance_port['Volatility'], y=min_variance_port['Returns'], c='blue', marker='D', s=200 )
plt.xlabel('Volatility (Std. Deviation)')
plt.ylabel('Expected Returns')
plt.title('Efficient Frontier')
plt.show()
print(sharpe_portfolio.T)

weights = sharpe_portfolio[df.keys()].values.flatten()
#weights = np.array([1/12, 1/12, 1/12, 1/12, 1/12, 1/12, 1/12, 1/12, 1/12, 1/12, 1/12, 1/12])

cumulative_port = df*weights
cumulative_port['sum'] = cumulative_port.apply(np.sum, axis=1)
balance = 1.0
for res in cumulative_port['sum'].values:
    balance *= 1 + res
print(balance)
