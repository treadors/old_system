import matplotlib
matplotlib.use('Qt5Agg', warn=False, force=True)
import matplotlib.pyplot as plt

from hello.utils import loadData
conf = loadData('confidences')
plt.plot(conf[0], conf[1])
plt.show()