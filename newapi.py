from hello.environment import *
from hello.system import *
from accounts import SergeyKeysBitmexTest, SergeyKeys, SergeyKeysBitmex
import logging
from hello.StndardSignals import *
env = Environment(API=SergeyKeys[0], secret=SergeyKeys[1], Test=False, length=1000, source='poloniex')
#portfolio = Portfolio(env)
env.getExcRates(test=False)
ts = TradingSystem(env=env, port=None)
env.cutCurrenciesByVolume('BTC', 0)
env.cutCurrenciesByVolume('USDT', 1)
now = timezone.now()
env.getAllData(now-timedelta(weeks=6), now, 1)
ts.addLayer(20, {'BTC': 0, 'USDT': 1}, 1.0)
ts.Data.glb_prepare_general_indicators()
prefs = ts.Layers[0].getPreferences()
ts.Data.preferences = prefs
ts.Data.glb_prepare_layer_indicators(prefs)
while True:
    env.getExcRates(save=True)
    print(ts.Data['BTC/USDT'][['timedate', 'Close', 'atr_e']].tail())